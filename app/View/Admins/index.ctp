<?php
$this -> start('main-content');
?>
<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
	        <div class="table-responsive">
	          <table class="table table-striped m-b-none" data-ride="datatables">
	            <thead>
	              <tr>
	              	<th>#</th>
                    <th>Name</th>
					<th>Username</th>
                    <th>User type</th>
                    <th>Status</th>
                    <th>Created</th>
                    <th>Modified</th>
                    <th>Action</th>
	              </tr>
	            </thead>
	            	
	            	<tbody>
	            	
	            <?php 
	            $inc=1;
	            foreach($users as $user) 
	            	{
	            	?>
		            
		            	<tr>
		            		<td><?php echo $inc; ?></td>
                            <td><?php echo $user['Admin']['name']; ?></td>
                            <td><?php echo $user['Admin']['username']; ?></td>
                            <td><?php echo $user['AdminType']['name']; ?></td>
                            <td><?php $a= $user['Admin']['status']; 
                            	if($a==0){
                            		echo "<p style='color:green'>Active</p>";
                            	}
                            	else if($a==1){
                            		echo "<p style='color:red'>Inactive</p>";
                            	}
                            ?></td>
                            <td><?php echo $user['Admin']['created']; ?></td>
                            <td><?php echo $user['Admin']['modified']; ?></td>
                            <td><?php  echo $this->Html->link('Edit',array('controller'=>'admins','action'=>'edit',$user['Admin']['id']),array('class'=>'btn btn-xs btn-info btn-info')); ?></td>
		                </tr>            
		            
	            	<?php
						$inc++;
					}
	            	?>
	            	
	            	</tbody>
	          </table>
	          <?php ?>
	        </div>
      </section>
    </div>
</div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#view" data-toggle="tab">Admin Listing</a></li>
  
	<li class="">
		<?php echo $this->Html->link('Add Admin',array('controller'=>'admins','action'=>'add')); ?>
	</li>
  </ul>
</header>
<?php
$this -> end('main-header');
?>