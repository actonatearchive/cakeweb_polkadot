<?php
$this -> start('main-content');
?>
    <div class="tab-content">
        <div class="tab-pane active" id="add">
            <?php echo $this -> Form -> create('EmailTemplate', array('controller' => 'email_templates', 'action' => 'edit', 'data-validate' => 'parsley', 'role' => 'form')); ?>
            <div class="row">
                <div class="col-sm-6">
                    <section class="panel">
                        <header class="panel-heading font-bold">1. Primary Details</header>
                        <!-- <header class="panel-heading font-bold">Add Item Category</header> -->
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Subject</label>
                                    <?php
                                    echo $this -> Form -> input('id', array('type'=>'hidden','default'=>$selectedEmailTemplate['EmailTemplate']['id'],'div' => false, 'label' => false, 'title' => 'Title', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'email-template-title'));
                                    echo $this -> Form -> input('title', array('default'=>$selectedEmailTemplate['EmailTemplate']['title'],'div' => false, 'label' => false, 'title' => 'Title', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'email-template-title'));
                                    ?>
                                    <label>Alias</label>
                                    <?php
                                    echo $this -> Form -> input('alias', array('default'=>$selectedEmailTemplate['EmailTemplate']['alias'],'div' => false, 'label' => false, 'title' => 'Alias', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'email-template-alias'));
                                    ?>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">
                        <header class="panel-heading font-bold">2. Email Template Content</header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="form-group col-sm-12">
                                    <?php
                                    echo $this -> Form -> input('content', array('default'=>html_entity_decode($selectedEmailTemplate['EmailTemplate']['content']),'type'=>'textarea','div' => false, 'label' => false, 'title' => 'Content', 'class' => 'form-control parsley-validated', 'id' => 'email-template-content'));
                                    ?>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">
                        <!-- <header class="panel-heading font-bold">Add Item Category</header> -->
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Submit', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'article-submit'));  ?>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            </form>
        </div>
    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="">
                <?php echo $this->Html->link('View Email Templates',array('controller'=>'email_templates','action'=>'index')); ?>
            </li>
            <li class="active"><a href="#add" data-toggle="tab">Add Email Template</a></li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>

<?php
    $this->start('script');
?>
<?php
    echo $this -> Html -> script('ckeditor/ckeditor');
    echo $this -> Html -> script('ckeditor/adapters/jquery');
?>
<script>
    $(document).ready(function(e){
        $( '#email-template-content' ).ckeditor({
            enterMode : CKEDITOR.ENTER_BR
        });
    })
</script>
<?php
    $this->end('script');
?>