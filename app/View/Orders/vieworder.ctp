<?php
$this -> start('main-content');
?>
    <div class="tab-content">
    <div class="tab-pane active" id="add">

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">1. Order Details</header>
                    <!-- <header class="panel-heading font-bold">Add Item</header> -->
                    <div class="panel-body">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Order Code</label>
                                <input type="hidden" name="data[OrderDispatch][order_id]" value="<?php echo $order['Order']['id'] ?>" />
                                <?php
                                //echo $this -> Form -> input('order_id', array('value'=> $order['Order']['id'],'disabled'=>'disabled', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'label' => false, 'div' => false));

                                echo $this -> Form -> input('temp', array('div' => false,'value'=> $order['Order']['code'],'disabled'=>'disabled', 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Username</label>
                                <?php
                                	echo $this -> Form -> input('temp', array('value'=> $order['User']['username'],'disabled'=>'disabled', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'label' => false, 'div' => false));
                                ?>
                            </div>
							<?php
							if ($order['Order']['status'] == 0) {
								$os = "Pending";
							} elseif ($order['Order']['status'] == 1) {
								$os = "Completed";
							}
							?>
                            <div class="form-group">
                                <label>Payment Status</label>
                                <?php
                                echo $this -> Form -> input('temp', array('value'=> $os,'disabled'=>'disabled', 'class' => 'form-control parsley-validated', 'label' => false, 'div' => false));
                                ?>
                            </div>
                            <!-- <div class="form-group">
                                <label>Discount Coupon</label>
                                <?php
                                echo $this -> Form -> input('temp', array('value'=> $coupon['Coupon']['code'],'disabled'=>'disabled','div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div> -->
                            <div class="form-group">
                                <label>Order Placed At</label>
                                <?php
                                $display_created = new DateTime($order['Order']['order_placed_at']);
                                $display_c = $display_created->format("D, F j, Y, g:i a");
                                echo $this -> Form -> input('temp', array('value'=> $display_c,'disabled'=>'disabled','div' => false, 'label' => false,  'class' => 'form-control parsley-validated'));
                                ?>
                            </div>

                            <!-- <div class="form-group">
                                <label>Referrer</label>
                                <?php

                                echo $this -> Form -> input('temp', array('value'=> $order['Order']['Referral']['name'],'disabled'=>'disabled','div' => false, 'label' => false,  'class' => 'form-control parsley-validated'));
                                ?>
                            </div> -->

                        </div>


                        <div class="col-sm-6">


                            <div class="form-group">
                                <label>Total Amount</label>
                                <?php
                                echo $this -> Form -> input('temp', array('value'=> $order['Order']['total_amount']+$order['Order']['eggless'],'disabled'=>'disabled','div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Shipping Charges</label>
                                <?php
                                echo $this -> Form -> input('temp', array('value'=> $order['Order']['additional_charges'],'disabled'=>'disabled','div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
							<div class="form-group">
                                <label>Discount Value</label>
                                <?php
                                echo $this -> Form -> input('temp', array('value'=> $order['Order']['discount_value'],'disabled'=>'disabled','div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
                            
                            <div class="form-group">
                                <label>Payment Mode</label>
                                <?php
                                echo $this -> Form -> input('temp', array('value'=>$order['Order']['payment_mode'],'disabled'=>'disabled','div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Modified</label>
                                <?php
                                $display_modified = new DateTime($order['Order']['modified']);
                                $display_m = $display_modified->format("D, F j, Y, g:i a");
                                echo $this -> Form -> input('temp', array('value'=> $display_m,'disabled'=>'disabled','div' => false, 'label' => false,  'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">2. Addresses</header>
                    <div class="panel-body">
						<div class="col-sm-6">
							<h3>Shipping Address :</h3>
							<?php
							echo "<b>".$order['ShippingAddress']['name']."</b><br />";
							echo $order['ShippingAddress']['line1']."<br />";
							echo $order['ShippingAddress']['line2']."<br />";
							echo $order['ShippingAddress']['landmark']."<br />";
							echo $order['ShippingAddress']['city_name']." - ".$order['ShippingAddress']['pin_code']."<br />";
							// echo $order['ShippingAddress']['state_name']."<br />";
							echo "Phone : ".$order['ShippingAddress']['mobile']."<br />";
							?>
						</div>
						<div class="col-sm-6">
							<h3>Billing Address :</h3>
							<?php
							echo "<b>".$order['User']['first_name']." ".$order['User']['last_name']."</b><br />";
							echo $order['User']['city']."<br />";
							echo $order['User']['username']."<br />";
							echo "Phone : ".$order['User']['mobile']."<br />";
							?>
						</div>
                    </div>
                </section>
            </div>

        </div>





        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">3. Order Items</header>
                    <div class="panel-body">
						<div class="col-sm-6">
							<?php
                            // pr($order_items);die();
							foreach($order_items as $key=>$value){

								//echo '<span class=""><h3 style="color:#c0493b;display:inline"><b> '.$value['name'].'</b></h3></span><br />';

								echo $this->Html->link($value['name']." - (".$value['variant_name'].")",array('controller'=>'items','action'=>'viewitem',$value['item_id']),array('class'=>'btn btn-lg btn-danger','target'=>'_blank'));

                                if($value['item_params'] != ""){
                                    echo '<h4><b>Delivery Instructions</b></h4>';
                                    echo '<p><b>Message on Cake - </b>'.$value['item_params']['delivery_message'];
                                    if(isset($value['item_params']['midnight']) && $value['item_params']['midnight'] == 1){
                                        $midnight_status = " - 8:00 PM to 12:00 AM (Midnight)";
                                        echo '<p><b>Delivery Date and Time - </b>'.$value['item_params']['delivery_date'].$midnight_status;
                                    }
                                    else{
                                        echo '<p><b>Delivery Date - </b>'.$value['item_params']['delivery_date'];
                                    }



                                    echo '</p>';
                                }

							}
							?>
						</div>
                    </div>
                </section>
            </div>

        </div>



		<!--<div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">4. Gift Details</header>
                    <div class="panel-body">
						<div class="col-sm-6">
							<?php
								//$display_ddate = new DateTime($order['Order']['delivery_date']);
                                //$display_deldate = $display_created->format("D, F j, Y");
								//echo "<b>Delivery Date : </b>".$display_deldate."<br />";
								//echo "<b>Gift To : </b>".$order['Order']['gift_to']."<br />";
								//echo "<b>Gift Message : </b>".$order['Order']['gift_message']."<br />";
							?>
						</div>
                    </div>
                </section>
            </div>

        </div>-->


    </div>
    </div>







<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
    <li class="">
        <?php echo $this -> Html -> link('Pending Dispatch Orders (Payment Complete)', array('controller' => 'orders', 'action' => 'index')); ?>
    </li>
    <li class="">
        <?php echo $this->Html->link('Pending Mark Delivered',array('controller'=>'orders','action'=>'pending_mark_delivered')); ?>
    </li>
    <li class="">
        <?php echo $this->Html->link('Today\'s Orders',array('controller'=>'orders','action'=>'todays')); ?>
    </li>
	<li class="">
        <?php echo $this->Html->link('Completed Orders (Payment+Dispatch)',array('controller'=>'orders','action'=>'completed')); ?>
    </li>
    <li class="">
        <?php echo $this->Html->link('Canceled Orders',array('controller'=>'orders','action'=>'canceled')); ?>
    </li>
	<li class="">
        <?php echo $this->Html->link('Incomplete Orders (Payment Incomplete)',array('controller'=>'orders','action'=>'payment_incomplete')); ?>
    </li>
	<li class="active"><a href="#view" data-toggle="tab">Order Details</a></li>
    </li>
  </ul>
</header>
<?php
$this -> end('main-header');
?>
