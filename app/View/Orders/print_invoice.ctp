<?php
$this -> start('main-content');
?>
<div class="wrapper">
    <div class="header">
    	<div class="header_left">
       		<img src="http://actonatepanel.com/cakeweb/img/small_logo.png" title="CakeWeb" alt="cakeweb"/>

        </div>
    	<div class="header_right">
        <h2>Invoice</h2>
        </div>
        <div class="clear"></div>
    </div>
    
    <div class="to_left">
    	<table>
    		<tr>
            	<td id="to">TO:</td>
                <td></td>
            </tr>
            <tr>
            	<td></td>
                <td id="to_name"><?php echo $order['User']['first_name']." ".$order['User']['last_name']; ?></td>
            </tr>    
        </table>
    </div>
    <div class="to_right">
    	<table id="invoice_no">
    		<tr>
            	<td>INVOICE NO</td>
                <td>: </td>
            </tr>
            <tr>
            	<td>DATE</td>
                <td>: </td>
            </tr>    
        </table>
    </div>
    <div class="clear"></div>
    
    <div>
    	<table id="jobtype">
        	<tr id="jobid_headtr">
            	<td>JOB TYPE</td>
                <td>DESCRIPTION</td>
            </tr>
            <tr id="jobid_datatr">
            	<td>Cake Delivery</td>
                <td>In accordance to order booked online with cakeweb.in<br />(Order code : <?php echo $order['Order']['code'] ?>)</td>
            </tr>
        </table>
    </div>
    
     <div>
    	<table class="pro_details">
        	<tr class="prodeteail_headhr">
            	<td class="first_td" >SR.NO.</td>
                <td class="second_td">PARTICULARS</td>
                <td class="first_td">TOTAL</td>
            </tr>
			<?php
			$counter = 1;
			foreach($order_items as $key=>$value){
				?>
					<tr>
						<td class="first_td"><?php echo $counter; ?></td>
						<td class="second_td"><?php echo $value['name']; ?></td>
						 <td class="first_td"><?php echo $value['price']; ?></td>
					</tr>
				<?php
				//echo $this->Html->link($value['name'],array('controller'=>'items','action'=>'viewitem',$value['item_id']),array('class'=>'btn btn-lg btn-danger','target'=>'_blank'));

			$counter++;
			}
			?>
            <tr id="total_amt">
            	<td class="first_td"></td>
                <td class="total_due">TOTAL DUE</td>
                <td class="total"><?php echo $order['Order']['total_amount']; ?></td>
            </tr>
        </table>
    </div>
    <div style="margin-top:15px;">
    	<p style="text-align:center;">All Prices are inclusive of taxes.</p>
         <hr />
    </div>
    <div class="footer">
    	<p style="text-align:center;color:#3b5e91;">
    		<span style="font-size:0.8em;">SB-3, Janmotri Appartment,
Opp. Panchratne Building,
Ellora Park, 
Vadodara-390023.<br /><br />
            THANK YOU FOR CHOOSING CAKE WEB &#9786;<br />
			WE LOOK FORWARD TO DO MORE BUSINESS WITH YOU!

    	</p>
  <br />
    </div>
   
</div>
      
<?php
$this -> end('main-content');
?>