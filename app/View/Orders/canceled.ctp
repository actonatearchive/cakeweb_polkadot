<?php
$this -> start('main-content');
?>

<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
	        <div class="table-responsive">
	          <table class="table table-striped m-b-none" data-ride="datatab">
	            <thead>
	              <tr>
					<th>Order Info</th>
                    <th>Code</th>
                    <th>User Name</th>
	                <th>Payment Status</th>
                    <th>Total Amount</th>
                    <th>Additional Charges</th>
                    <th>Payment Mode</th>
                    <th>Created</th>
                    <!-- <th>Referrer</th> -->
	              </tr>
	            </thead>
	            	
	            	<tbody>
	            	
	            <?php
	            $grand_total = 0;
	             foreach($orders as $order) 
	            	{
	            		if($order['Order']['status'] == 1 || $order['Order']['status'] == 2){
	            			$grand_total += $order['Order']['total_amount'] + $order['Order']['additional_charges'] - $order['Order']['discount_value'];	
	            		}
	            		
	            	?>
		            
		            	<tr>
							<td><?php
                                  echo $this->Html->link('View Order',array('controller'=>'orders','action'=>'vieworder',$order['Order']['id']),array('class'=>'btn btn-xs btn-default'));
                                ?></td>
                            <td><?php echo $order['Order']['code']; ?></td>
							<td><?php
                                  echo $this->Html->link($order['User']['username'],array('controller'=>'users','action'=>'viewuser',$order['User']['id']),array('class'=>'btn btn-xs btn-info'));
                                ?></td>
                            <!--<td><?php echo $order['User']['username']; ?></td>-->
							<td><?php
							if ($order['Order']['status'] == 0) {
							echo "Pending";
							} elseif ($order['Order']['status'] == 1) {
							echo "Completed";
							} elseif ($order['Order']['status'] == 2) {
							echo "Dispatched";
							}
							?></td>
                            <td><?php echo $order['Order']['total_amount']; ?></td>
                            <td><?php echo $order['Order']['additional_charges']; ?></td>
                            
                            <td>
                                                        <?= $order['Order']['payment_mode'] ?>

                            </td>
                            <td><?php echo $order['Order']['created']; ?></td>
                            <!-- <td><?php echo $order['Order']['Referral']['name']; ?></td> -->

		                </tr>            
		            
	            	<?php
					}
	            	?>
	            	
	            	</tbody>
	          </table>
	          
	        </div>

	        
      </section>
      <?php
                // pagination section
                echo "<div class='paging'>";
             
                    // the 'first' page button
                    echo $this->Paginator->first("First")."  ";
                     
                    // 'prev' page button, 
                    // we can check using the paginator hasPrev() method if there's a previous page
                    // save with the 'next' page button
                    if($this->Paginator->hasPrev()){
                        echo $this->Paginator->prev("Prev")."  ";
                    }
                     

                    // the 'number' page buttons
                    echo $this->Paginator->numbers(array('modulus' => 50))."  ";
                                       

                    // for the 'next' button
                    if($this->Paginator->hasNext()){
                        echo $this->Paginator->next("Next")."  ";
                    }              
                     
                    // the 'last' page button
                    echo $this->Paginator->last("Last");
                 
                echo "</div>";
               ?>

    </div>
</div>
<br />
<br />
<!-- <div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">              
			<h3>&nbsp;&nbsp;Today's Total (Completed) : <strong><i class="icon-inr"></i> <?php //echo $grand_total; ?></strong></h3>
      </section>

    </div>              
</div>   -->
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
    
    <li class="">
        <?php echo $this -> Html -> link('Pending Dispatch Orders (Payment Complete)', array('controller' => 'orders', 'action' => 'index')); ?>
    </li>
        <li class="">
        <?php echo $this->Html->link('Pending Mark Delivered',array('controller'=>'orders','action'=>'pending_mark_delivered')); ?>
    </li>
    
    <li class="">
        <?php echo $this->Html->link('Today\'s Orders',array('controller'=>'orders','action'=>'todays')); ?>
    </li>
		<li class="">
        <?php echo $this->Html->link('Completed Orders (Payment+Dispatch)',array('controller'=>'orders','action'=>'completed')); ?>
    </li>
    <li class="active"><a href="#view" data-toggle="tab">Canceled	 Orders</a></li>
	<li class="">
        <?php echo $this->Html->link('Incomplete Orders (Payment Incomplete)',array('controller'=>'orders','action'=>'payment_incomplete')); ?>
    </li>
  </ul>
</header>
<?php
$this -> end('main-header');
?>