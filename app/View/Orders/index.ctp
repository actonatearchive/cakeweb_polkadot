<?php
$this -> start('main-content');
?>
<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
	        <div class="table-responsive">
	          <table class="table table-striped m-b-none" data-ride="datatables">
	            <thead>
	              <tr>
	                  <th>Cancel / View<br />Order</th>
                    <th width="10%">Order Date</th>
                    <th>Order<br />Item</th>
                    <th width="50%">Special Remark</th>
					          <th>City</th>
                    <th>Payment <br /></th>
                    <th>Code</th>
                    <th>User Name</th>
                    <th>Order<br />Amount</th>
                    <th>Order Placed at</th>
                    <!-- <th>Review</th> -->
                    <!-- <th>Additional<br />Charges</th>
                    <th>Discount</th> -->
                    <!-- <th>Payment Mode</th>
                    <th>Created</th> -->
                    <!-- <th>Referrer</th> -->

	              </tr>
	            </thead>

	            	<tbody>

	            <?php
	            $grand_total = 0;
	            $todays = new DateTime();
	             foreach($orders as $order)
	            	{
                  if(sizeof($order['OrderItem']) > 0){
	            		$date = new DateTime($order['Order']['created']);

	            		//if($date->format('Y-m-d') == $todays->format('Y-m-d')){
	            			$grand_total += $order['Order']['total_amount'] + $order['Order']['additional_charges'] - $order['Order']['discount_value'] + $order['Order']['eggless'];
	            		//}

	            	?>

		            	<tr>
                            
							<td>
                    <?php
                            		if($order['Order']['status']==1) {
                                  		echo $this->Element('createDispatch',array('order' => $order));
                              		} else {
                              			?>
                              			<a href="https://cakeweb.in/cakeweb_polkadot/orderdispatches/mark_delivered/<?php echo $order['Order']['id']; ?>"><button class="btn btn-xs btn-primary" onclick="if(confirm('Press OK to mark delivered')) return true; else return false;">Mark Delivered</button></a>
                              			<?php
                              		}


                                ?><br><br>
                                <?php
                                  echo $this->Html->link('Print Invoice',array('controller'=>'orders','action'=>'print_invoice',$order['Order']['id']),array('class'=>'btn btn-xs btn-success btn-default','target'=>'_blank'));
                                ?><br><br>
              <?php
                                  echo $this->Html->link('View',array('controller'=>'orders','action'=>'vieworder',$order['Order']['id']),array('class'=>'btn btn-xs btn-default','target'=>'_blank'));
                                ?>
              <br><br>
                                
              <?php
                                  echo $this->Html->link('Cancel',array('controller'=>'orders','action'=>'cancel',$order['Order']['id']),array('class'=>'btn btn-xs btn-danger'));
                                ?>                  
                                </td>

                                <td>
                                  <?php
                                      echo date('d-m-Y',strtotime($order['Order']['created']));
                                  ?>
                                </td>

                                <td>
                                  <?php
                                    //echo $order['OrderItem']['item_name'];
                                    foreach ($order['OrderItem'] as $ord_item) {
                                     echo $this->Html->link($ord_item['Item']['name'],array('controller'=>'items','action'=>'viewitem',$ord_item['Item']['id']),array('class'=>'btn btn-xs btn-primary','target'=>'_blank'));
                                    echo "<br><br>";
                                    }
                                  ?>
                                </td>
                                <td>
                                  <?php
                                    foreach ($order['OrderItem'] as $item) {

                                      if($item['OrderItem']['item_params'] != '[]')
                                      {

                                        echo "<strong>".$item['Item']['name']."</strong><br>";

                                        $special_remark=json_decode($item['OrderItem']['item_params'],true);

                                        if(isset($special_remark['delivery_message'])){
                                          echo "Delivery Message: <b style='color:#FF5858;'>".$special_remark['delivery_message']."</b><br>";
                                        }
                                        if(isset($special_remark['delivery_date'])) {
                                            $de_date = str_replace("/","-",$special_remark['delivery_date']);
                                            echo "Delivery Date: <b style='color:#FF5858;'>".date("d-m-Y",strtotime($de_date))."</b><br>";
                                        }
                                        if(isset($special_remark['midnight']) && $special_remark['midnight'] == '1') {
                                            echo "Delivery Time: <b style='color:#FF5858;'>8:00 PM to 12:00 PM</b><br>";
                                        } else {
                                            echo "Delivery Time: <b style='color:#FF5858;'> - </b><br>";
                                        }

                                        if(isset($special_remark['eggless']) && $special_remark['eggless'] == '1') {
                                            echo "Eggless: <b style='color:#FF5858;'>Yes</b><br>";
                                        } else {
                                           echo "Eggless: <b style='color:#FF5858;'> - </b><br>";
                                        }
										echo "Extra Note: <b style='color: #FF5858'>".$special_remark['optional_message']."</b>";
                                        echo "<hr style='margin-top:5px;margin-bottom:5px;'>";
                                      }
                                      else
                                      {
                                        echo "";
                                      }

                                    }
                                    ?>
                                   </td>
                                <td>
                                
                                <?php
                                    echo $order['Order']['payment_mode'];

                                  ?>
                                </td>
                                <td>
                                  <?php
                                    echo $order['ShippingAddress']['city_name'];

                                  ?>
                                </td>
                            
                            <td><?php echo $order['Order']['code']; ?>
                                    <br>
                                    <!-- <a href="https://www.cakestudio.in/checkout/reconfirm_sms/<?php echo $order['Order']['id']; ?>"><button class="btn btn-xs btn-success" onclick="if(confirm('Press OK to send')) return true; else return false;">SMS</button></a>
                                <br><br>
                              <a href="https://www.cakestudio.in/checkout/reconfirm_email/<?php echo $order['Order']['id']; ?>"><button class="btn btn-xs btn-success" onclick="if(confirm('Press OK to send')) return true; else return false;">Email</button></a> -->

                            </td>
							               <td>
                            <?php
                                echo "Name: ".$order['ShippingAddress']['name']."<br>";
                                echo $order['ShippingAddress']['line1']."<br/>";
                                echo $order['ShippingAddress']['line2']."<br/>";
                                if($order['ShippingAddress']['landmark'] != "" && $order['ShippingAddress']['landmark'] != null){
                                echo "Landmark: ".$order['ShippingAddress']['landmark']."<br/>";
                                }
                                echo "Mobile: ".$order['ShippingAddress']['mobile'];


                                ?>
                              </td>
                            <!--<td><?php //echo $order['Order']['User']['username']; ?></td>-->

                            <td><?php echo $order['Order']['total_amount']+$order['Order']['additional_charges']+$order['Order']['eggless']-$order['Order']['discount_value']; ?></td>
                            <!-- <td><?php //echo $order['Order']['additional_charges']; ?></td>
                            <td><?php //echo $order['Order']['discount_value']; ?></td> -->

                            <!-- <td>
                            	<?php
								/*if ($order['Order']['payment_mode'] == 1) {
									echo "COD";
								} elseif ($order['Order']['payment_mode'] == 2) {
									echo "Debit Card";
								} elseif ($order['Order']['payment_mode'] == 3) {
									echo "Credit Card";
								} elseif ($order['Order']['payment_mode'] == 4) {
									echo "Net Banking";
								}*/
								 ?>
                            </td>
                            <td><?php //echo $order['Order']['created']; ?></td> -->
                            <td>
                            <?php
                                if ($order['Order']['status'] >= 1) {
                                    echo date('d-M-Y h:m a',strtotime($order['Order']['order_placed_at'])) ;
                                } else {
                                    echo date('d-M-Y h:m a',strtotime($order['Order']['created'])) ;
                                }
                            ?>
                            </td>
                            <!-- <td><?= $order['Order']['review'] ?></td> -->

		                </tr>

	            	<?php
					} }
	            	?>

	            	</tbody>
	          </table>
	          <?php ?>
	        </div>
      </section>
    </div>
</div>
<br />
<br />
<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
			<h3>&nbsp;&nbsp;Pending Dispatch Orders (Payment Complete) Total : <strong><i class="icon-inr"></i> <?php echo $grand_total; ?></strong></h3>
      </section>

    </div>
</div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#view" data-toggle="tab">Pending Dispatch Orders (Payment Complete)</a></li>
        <li class="">
        <?php echo $this->Html->link('Pending Mark Delivered',array('controller'=>'orders','action'=>'pending_mark_delivered')); ?>
    </li>
    <li class="">
        <?php echo $this->Html->link('Today\'s Orders',array('controller'=>'orders','action'=>'todays')); ?>
    </li>
	<li class="">
        <?php echo $this->Html->link('Completed Orders (Payment+Dispatch)',array('controller'=>'orders','action'=>'completed')); ?>
    </li>
    <li class="">
        <?php echo $this->Html->link('Canceled Orders',array('controller'=>'orders','action'=>'canceled')); ?>
    </li>
	<li class="">
        <?php echo $this->Html->link('Incomplete Orders (Payment Incomplete)',array('controller'=>'orders','action'=>'payment_incomplete')); ?>
    </li>
  </ul>
</header>
<?php
$this -> end('main-header');
?>
