<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
	<head>
		<?php echo $this -> Html -> charset(); ?>
		<title> <?php echo $page_title ?> | CakeWeb </title>
    <script>
        var baseUrl = "<?php echo $this->webroot; ?>";
    </script>
	<style>
		 body{margin:00px;font-family:Arial, Helvetica, sans-serif;}
		.wrapper{width:640px;margin:00px auto;}
		.header{background-image: -webkit-gradient(
			linear,
			left top,
			left bottom,
			color-stop(0.1, #CAD7E8),
			color-stop(0.77, #FFFFFF)
		);
		background-image: -o-linear-gradient(bottom, #CAD7E8 10%, #FFFFFF 77%);
		background-image: -moz-linear-gradient(bottom, #CAD7E8 10%, #FFFFFF 77%);
		background-image: -webkit-linear-gradient(bottom, #CAD7E8 10%, #FFFFFF 77%);
		background-image: -ms-linear-gradient(bottom, #CAD7E8 10%, #FFFFFF 77%);
		background-image: linear-gradient(to bottom, #CAD7E8 10%, #FFFFFF 77%);}
		.header_left{float:left;}
		.header_right{float:right;padding-right:40px;}
		.header_right h2{color:#3b5e91;text-transform:uppercase;font-family:Arial, Helvetica, sans-serif;}
		.clear{clear:both}
		#invoice_no td{font-size:0.9em;}
		#to{padding-left:20px;font-size:0.9em;}
		#to_name{font-size:1.3em;border-bottom:1px #3b5e91 solid;padding-right:100px;}
		.to_left{float:left;width:65%;}
		.to_right{float:right;width:35%;}
		#jobtype{border-spacing:00px;width:100%;margin-top:50px;font-size:0.9em;border-top:2px solid #3b5e91;border-right:1px solid #3b5e91;}
		#jobtype td{border-bottom:1px solid #3b5e91 !important;border-left:1px solid #3b5e91 !important;}
		#jobid_headtr{background:#cad7e9;}
		#jobid_datatr td{width:50%;text-align:center;padding:20px 00px;}

		.pro_details{border-spacing:00px;width:100%;margin-top:30px;font-size:0.9em;border-top:2px solid #3b5e91;border-right:1px solid #3b5e91;}
		.pro_details tr td{border-bottom:1px solid #3b5e91;border-left:1px solid #3b5e91;}
		.prodeteail_headhr{background:#cad7e9;}

		.first_td{width:20%;text-align:center;padding:10px 00px;}
		.second_td{width:60%;text-align:center;padding:10px 00px;}
		.total_due{width:60%;text-align:center;padding:10px 20px;border:none;color:#3b5e91;text-align:right;}
		.total{width:20%;text-align:center;padding:10px 00px;border-bottom:1px solid #3b5e91 !important;border-left:1px solid #3b5e91 !important;}
		#total_amt td{border:none;}

		#jobid_headtr td{width:50%;text-align:center;padding:10px 00px;}

		.footer{background-image: -webkit-gradient(
			linear,
			left top,
			left bottom,
			color-stop(0, #FFFFFF),
			color-stop(1, #CAD7E9)
		);
		background-image: -o-linear-gradient(bottom, #FFFFFF 0%, #CAD7E9 100%);
		background-image: -moz-linear-gradient(bottom, #FFFFFF 0%, #CAD7E9 100%);
		background-image: -webkit-linear-gradient(bottom, #FFFFFF 0%, #CAD7E9 100%);
		background-image: -ms-linear-gradient(bottom, #FFFFFF 0%, #CAD7E9 100%);
		background-image: linear-gradient(to bottom, #FFFFFF 0%, #CAD7E9 100%);}
	</style>	
	</head>
	<body onload="window.print()">
			<!-- .vbox -->
			<section id="content">
				<section class="vbox">
					<section class="scrollable wrapper">
						<?php echo $this -> fetch('main-content'); ?>

					</section>
				</section>
				<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
			</section>
			<!-- /.vbox -->
		</section>

	</body>
</html>
