<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$contentMangerMenu = array();

array_push($contentMangerMenu, array('icon' => 'icon-file', 'name' => 'Articles', 'controller' => 'articles', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));
array_push($contentMangerMenu, array('icon' => 'icon-envelope', 'name' => 'Email', 'controller' => 'email_templates', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));
array_push($contentMangerMenu, array('icon' => 'icon-mobile-phone', 'name' => 'SMS', 'controller' => 'sms_templates', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));
array_push($contentMangerMenu, array('icon' => 'icon-picture', 'name' => 'Banners', 'controller' => 'banners', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));
array_push($contentMangerMenu, array('icon' => 'icon-star', 'name' => 'Offers', 'controller' => 'offers', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));
array_push($contentMangerMenu, array('icon' => 'icon-star', 'name' => 'Promotion Email Photos', 'controller' => 'promotion_photos', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));

$itemsContextMenu = array();

array_push($itemsContextMenu, array('icon' => 'icon-file', 'name' => 'View Items', 'controller' => 'items', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));
array_push($itemsContextMenu, array('icon' => 'icon-envelope', 'name' => 'Add Item', 'controller' => 'items', 'action' => 'add', 'hover' => false, 'hoverOptions' => 'null'));
array_push($itemsContextMenu, array('icon' => 'icon-mobile-phone', 'name' => 'Add Gallery', 'controller' => 'items', 'action' => 'add_gallery', 'hover' => false, 'hoverOptions' => 'null'));
array_push($itemsContextMenu, array('icon' => 'icon-mobile-phone', 'name' => 'City Wise Items', 'controller' => 'items', 'action' => 'city', 'hover' => false, 'hoverOptions' => 'null'));

/* -====================================== */
// New Item Menu
$newitemsContextMenu = array();

array_push($newitemsContextMenu, array('icon' => 'icon-file', 'name' => 'View Items', 'controller' => 'new_items', 'action' => 'view', 'hover' => false, 'hoverOptions' => 'null'));
array_push($newitemsContextMenu, array('icon' => 'icon-envelope', 'name' => 'Add Item', 'controller' => 'new_items', 'action' => 'add', 'hover' => false, 'hoverOptions' => 'null'));
// array_push($newitemsContextMenu, array('icon' => 'icon-mobile-phone', 'name' => 'Add Gallery', 'controller' => 'items', 'action' => 'add_gallery', 'hover' => false, 'hoverOptions' => 'null'));
// array_push($newitemsContextMenu, array('icon' => 'icon-mobile-phone', 'name' => 'City Wise Items', 'controller' => 'items', 'action' => 'city', 'hover' => false, 'hoverOptions' => 'null'));

/* =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

$stocksContextMenu = array();

array_push($stocksContextMenu, array('icon' => 'icon-file', 'name' => 'View Stocks', 'controller' => 'item_stocks', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));
array_push($stocksContextMenu, array('icon' => 'icon-envelope', 'name' => 'Add Item', 'controller' => 'item_stocks', 'action' => 'add', 'hover' => false, 'hoverOptions' => 'null'));

$itemCategoriesContextMenu = array();

array_push($itemCategoriesContextMenu, array('icon' => 'icon-file', 'name' => 'View Categories', 'controller' => 'item_categories', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));
array_push($itemCategoriesContextMenu, array('icon' => 'icon-envelope', 'name' => 'Add Category', 'controller' => 'item_categories', 'action' => 'add', 'hover' => false, 'hoverOptions' => 'null'));

$citiesContextMenu = array();

array_push($citiesContextMenu, array('icon' => 'icon-file', 'name' => 'View Cities', 'controller' => 'cities', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));
array_push($citiesContextMenu, array('icon' => 'icon-envelope', 'name' => 'Add City', 'controller' => 'cities', 'action' => 'add', 'hover' => false, 'hoverOptions' => 'null'));

$ordersContextMenu = array();

array_push($ordersContextMenu, array('icon' => 'icon-file', 'name' => 'Pending Dispatch Orders (Payment Complete)', 'controller' => 'orders', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));
array_push($ordersContextMenu, array('icon' => 'icon-envelope', 'name' => 'Today\'s Orders', 'controller' => 'orders', 'action' => 'todays', 'hover' => false, 'hoverOptions' => 'null'));
array_push($ordersContextMenu, array('icon' => 'icon-envelope', 'name' => 'Completed Orders (Payment+Dispatch)', 'controller' => 'orders', 'action' => 'completed', 'hover' => false, 'hoverOptions' => 'null'));
array_push($ordersContextMenu, array('icon' => 'icon-envelope', 'name' => 'Incomplete Orders (Payment Incomplete)', 'controller' => 'orders', 'action' => 'payment_incomplete', 'hover' => false, 'hoverOptions' => 'null'));

$couriersContextMenu = array();

array_push($couriersContextMenu, array('icon' => 'icon-file', 'name' => 'View Couriers', 'controller' => 'couriers', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));
array_push($couriersContextMenu, array('icon' => 'icon-envelope', 'name' => 'Add Courier', 'controller' => 'couriers', 'action' => 'add', 'hover' => false, 'hoverOptions' => 'null'));

$systemMailsContextMenu = array();

array_push($systemMailsContextMenu, array('icon' => 'icon-file', 'name' => 'All Users', 'controller' => 'users', 'action' => 'email_all', 'hover' => false, 'hoverOptions' => 'null'));
array_push($systemMailsContextMenu, array('icon' => 'icon-envelope', 'name' => 'Users With City', 'controller' => 'users', 'action' => 'email_city_listed', 'hover' => false, 'hoverOptions' => 'null'));
array_push($systemMailsContextMenu, array('icon' => 'icon-envelope', 'name' => 'To a CSV list', 'controller' => 'users', 'action' => 'email_csv', 'hover' => false, 'hoverOptions' => 'null'));

$usersContextMenu = array();

array_push($usersContextMenu, array('icon' => 'icon-file', 'name' => 'User Listing', 'controller' => 'users', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));
array_push($usersContextMenu, array('icon' => 'icon-envelope', 'name' => 'Export CSV', 'controller' => 'users', 'action' => 'export_csv', 'hover' => false, 'hoverOptions' => 'null'));

$couponsContextMenu = array();

array_push($couponsContextMenu, array('icon' => 'icon-file', 'name' => 'View Coupons', 'controller' => 'coupons', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));
array_push($couponsContextMenu, array('icon' => 'icon-envelope', 'name' => 'Add Coupon', 'controller' => 'coupons', 'action' => 'add', 'hover' => false, 'hoverOptions' => 'null'));



//if($activeUser['User']['AdminType']['name']=="Admin") {

	$sideBarOptions = array();
	array_push($sideBarOptions, array('icon' => 'icon-table', 'name' => 'Dashboard', 'controller' => 'home', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));
	// array_push($sideBarOptions, array('icon' => 'icon-list', 'name' => 'Items', 'controller' => 'items', 'action' => 'index', 'hover' => true, 'hoverOptions' => $itemsContextMenu));

    /** -- NEW ITEM MENU -- **/
    array_push($sideBarOptions, array('icon' => 'icon-list', 'name' => 'New Items', 'controller' => 'items', 'action' => 'index', 'hover' => true, 'hoverOptions' => $newitemsContextMenu));
    /* -============================================= */

	array_push($sideBarOptions, array('icon' => 'icon-list', 'name' => 'Stocks', 'controller' => 'item_stocks', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));
	array_push($sideBarOptions, array('icon' => 'icon-book', 'name' => 'Categories', 'controller' => 'item_categories', 'action' => 'index', 'hover' => true, 'hoverOptions' => $itemCategoriesContextMenu ));
	array_push($sideBarOptions, array('icon' => 'icon-map-marker', 'name' => 'Cities', 'controller' => 'cities', 'action' => 'index', 'hover' => true, 'hoverOptions' => $citiesContextMenu));
	array_push($sideBarOptions, array('icon' => 'icon-file', 'name' => 'Site Content', 'controller' => 'articles', 'action' => 'index', 'hover' => true, 'hoverOptions' => $contentMangerMenu));
	array_push($sideBarOptions, array('icon' => 'icon-check', 'name' => 'Orders', 'controller' => 'orders', 'action' => 'index', 'hover' => true, 'hoverOptions' => $ordersContextMenu));
	array_push($sideBarOptions, array('icon' => 'icon-pencil', 'name' => 'Dispatch', 'controller' => 'orderdispatches', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));
	array_push($sideBarOptions, array('icon' => 'icon-suitcase', 'name' => 'Couriers', 'controller' => 'couriers', 'action' => 'index', 'hover' => true, 'hoverOptions' => $couriersContextMenu));
	array_push($sideBarOptions, array('icon' => 'icon-user', 'name' => 'Users', 'controller' => 'users', 'action' => 'index', 'hover' => true, 'hoverOptions' => $usersContextMenu));
	array_push($sideBarOptions, array('icon' => 'icon-envelope', 'name' => 'System Mails', 'controller' => 'users', 'action' => 'email_all', 'hover' => true, 'hoverOptions' => $systemMailsContextMenu));
	array_push($sideBarOptions, array('icon' => 'icon-tags', 'name' => 'Coupons', 'controller' => 'coupons', 'action' => 'index', 'hover' => true, 'hoverOptions' => $couponsContextMenu));

//} else {
	//$sideBarOptions = array();
	//array_push($sideBarOptions, array('icon' => 'icon-table', 'name' => 'Dashboard', 'controller' => 'home', 'action' => 'index', 'hover' => false, 'hoverOptions' => 'null'));
//}


?>
<!DOCTYPE html>
<html>
	<head>
		<?php echo $this -> Html -> charset(); ?>
		<title> <?php echo $page_title ?> | CakeWeb - Powered by Polkadot!</title>
		<?php
		echo $this -> Html -> css('bootstrap');
		echo $this -> Html -> css('animate');
		//echo $this -> Html -> css('datatables');
		echo $this -> Html -> css('font-awesome.min');
		echo $this -> Html -> css('font');
		echo $this -> Html -> css('colorpicker');
		echo $this -> Html -> css('app');
		echo $this -> Html -> css('plugin');
        echo $this -> Html -> css('polka');
        echo $this -> Html -> css('/js/datepicker/datepicker');
        echo $this -> Html -> css('spectrum');

	?>
    <script>
        var baseUrl = "<?php echo $this->webroot; ?>";
    </script>
	</head>
	<body>
        <div class="main-loader">
            <div id="circleG">
                <div id="circleG_1" class="circleG">
                </div>
                <div id="circleG_2" class="circleG">
                </div>
                <div id="circleG_3" class="circleG">
                </div>
            </div>
        </div>
		<section class="hbox stretch">
			<!-- .aside -->
			<aside class="bg-primary aside-sm" id="nav">
				<section class="vbox">
					<header class="dker nav-bar nav-bar-fixed-top">
						<a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav"> <i class="icon-reorder"></i> </a>
                        <?php echo $this->Html->link($this->Html->image('cakeweb_Logo.png'),array('controller'=>'home','action'=>'index'),array('escape'=>false,'data-toggle'=>'fullscreen','class'=>'nav-brand')); ?>

						<a class="btn btn-link visible-xs" data-toggle="class:show" data-target=".nav-user"> <i class="icon-comment-alt"></i> </a>
					</header>
					<section>
						<!-- user -->
						<div class="bg-primary-color nav-user hidden-xs pos-rlt">
							<div class="nav-avatar pos-rlt">
								<a href="#" class="thumb-sm avatar animated rollIn" data-toggle="dropdown"> <?php echo $this->Html->image('avatar.jpg')?> <span class="caret caret-white"></span> </a>
								<ul class="dropdown-menu m-t-sm animated fadeInLeft">
									<span class="arrow top"></span>
									<!--<li>
										<a href="#">Settings</a>
									</li>
									<li>
										<a href="#"> <span class="badge bg-danger pull-right">3</span> Notifications </a>
									</li>-->
									<!-- <li><?php //echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-plus')) . " View admin",array('controller' => 'admins', 'action' => 'index'),array('escape' => false)); ?></li>
									<li><?php //echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-user')) . " Add admin",array('controller' => 'admins', 'action' => 'add'),array('escape' => false)); ?></li> -->
									<li class="divider"></li>
									<li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-power-off')) . " Logout",array('controller' => 'admins', 'action' => 'logout'),array('escape' => false)); ?></li>
								</ul>
								<div class="visible-xs m-t m-b">
									<a href="#" class="h3">John.Smith</a>
									<p>
										<i class="icon-map-marker"></i> London, UK
									</p>
								</div>
							</div>
							<!--  <div class="nav-msg">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<b class="badge badge-white count-n">2</b>
							</a>
							<section class="dropdown-menu m-l-sm pull-left animated fadeInRight">
							<div class="arrow left"></div>
							<section class="panel bg-white">
							<header class="panel-heading">
							<strong>You have <span class="count-n">2</span> notifications</strong>
							</header>
							<div class="list-group">
							<a href="#" class="media list-group-item">
							<span class="pull-left thumb-sm">
							<img src="images/avatar.jpg" alt="John said" class="img-circle">
							</span>
							<span class="media-body block m-b-none">
							Use awesome animate.css<br>
							<small class="text-muted">28 Aug 13</small>
							</span>
							</a>
							<a href="#" class="media list-group-item">
							<span class="media-body block m-b-none">
							1.0 initial released<br>
							<small class="text-muted">27 Aug 13</small>
							</span>
							</a>
							</div>
							<footer class="panel-footer text-sm">
							<a href="#" class="pull-right"><i class="icon-cog"></i></a>
							<a href="#">See all the notifications</a>
							</footer>
							</section>
							</section>
							</div> -->
						</div>
						<!-- / user -->
						<!-- nav -->
						<nav class="nav-primary hidden-xs">
							<ul class="nav">
								<?php
								foreach ($sideBarOptions as $option) {
									if ($page_title == $option['name']) {
										if ($option['hover'] == true)
											echo '<li class="active dropdown-submenu">';
										else
											echo '<li class="active">';
									} else {
										if ($option['hover'] == true)
											echo '<li class="dropdown-submenu">';
										else
											echo '<li>';
									}
									echo $this -> Html -> link('
<i class="' . $option['icon'] . '"></i>
<span>' . $option['name'] . '</span>
', array('controller' => $option['controller'], 'action' => $option['action']), array('escape' => false));
									if ($option['hover'] == true) {
										echo '<ul class="dropdown-menu">';
										foreach ($option['hoverOptions'] as $hover) {
											echo '<li>';
											echo $this -> Html -> link($hover['name'], array('controller' => $hover['controller'], 'action' => $hover['action']), array('escape' => false));
											echo '</li>';
										}
										echo '</ul>';
									}
									echo '</li>';
								}
								?>
								<!--  <li class="dropdown-submenu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="icon-check-empty"></i>
								<span>Stocks</span>
								</a> -->
								<!-- <ul class="dropdown-menu">
								<li>
								<a href="buttons.html">Content</a>
								</li>
								<li>
								<a href="icons.html">
								<b class="badge pull-right">302</b>Orders
								</a>
								</li>
								<li>
								<a href="grid.html">Grid</a>
								</li>
								<li>
								<a href="widgets.html">
								<b class="badge bg-primary pull-right">8</b>Widgets
								</a>
								</li>
								<li>
								<a href="components.html">
								<b class="badge pull-right">18</b>Components
								</a>
								</li>
								<li>
								<a href="list.html">List groups</a>
								</li>
								<li>
								<a href="table.html">Table</a>
								</li>
								<li>
								<a href="form.html">Form</a>
								</li>
								<li>
								<a href="chart.html">Chart</a>
								</li>
								<li>
								<a href="calendar.html">Fullcalendar</a>
								</li>
								<li>
								<a href="portlet.html">Portlet</a>
								</li>
								</ul> -->
								<!-- </li>
								<li class="dropdown-submenu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="icon-suitcase"></i>
								<span>Content</span>
								</a> -->
								<!-- <ul class="dropdown-menu">
								<li>
								<a href="dashboard.html">Dashboard</a>
								</li>
								<li>
								<a href="dashboard-1.html">Dashboard one</a>
								</li>
								<li>
								<a href="dashboard-2.html">Dashboard layout</a>
								</li>
								<li>
								<a href="analysis.html">Analysis</a>
								</li>
								<li>
								<a href="gallery.html">Gallery</a>
								</li>
								<li>
								<a href="profile.html">Profile</a>
								</li>
								<li>
								<a href="blog.html">Blog</a>
								</li>
								<li>
								<a href="invoice.html">Invoice</a>
								</li>
								<li>
								<a href="signin.html">Signin page</a>
								</li>
								<li>
								<a href="signup.html">Signup page</a>
								</li>
								<li>
								<a href="404.html">404 page</a>
								</li>
								</ul> -->
								<!-- </li>
								<li>
								<a href="mail.html"> -->
								<!-- <b class="badge bg-primary pull-right">3</b> -->
								<!--   <i class="icon-envelope"></i>
								<span>Orders</span>
								</a>
								</li>
								<li>
								<a href="tasks.html">
								<i class="icon-user"></i>
								<span>Customers</span>
								</a>
								</li> -->
							</ul>
						</nav>
						<!-- / nav -->
						<!-- note -->
						<!-- <div class="bg-danger wrapper hidden-vertical animated fadeInUp text-sm">
						<a href="#" data-dismiss="alert" class="pull-right m-r-n-sm m-t-n-sm"><i class="icon-close icon-remove "></i></a>
						Hi, welcome to todo,  you can start here.
						</div> -->
						<!-- / note -->
					</section>
					<footer class="footer bg-gradient hidden-xs">
						<?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-off ')) . "",array('controller' => 'admins', 'action' => 'logout'),array('class'=>'btn btn-sm btn-link m-l-n-sm pull-right','escape'=>false)); ?>
						<a href="#nav" data-toggle="class:nav-vertical" class="btn btn-sm btn-link m-l-n-sm"> <i class="icon-reorder"></i> </a>
					</footer>
				</section>
			</aside>
			<!-- /.aside -->
			<!-- .vbox -->
			<section id="content">
				<section class="vbox">
                    <?php
                        echo $this->Session->flash('error');
                        echo $this->Session->flash('success');
                        echo $this->Session->flash('notice');
                    ?>
                   <!-- <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
                        <i class="icon-ban-circle"></i><?php echo $this->Session->flash('error'); ?>
                    </div>
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
                        <i class="icon-ok-sign"></i><?php echo $this->Session->flash('success'); ?>
                    </div>
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
                        <i class="icon-info-sign"></i><?php echo $this->Session->flash('notice'); ?>
                    </div>  -->
					<?php echo $this -> fetch('main-header'); ?>
					<section class="scrollable wrapper">
						<?php echo $this -> fetch('main-content'); ?>

					</section>
				</section>
				<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
			</section>
			<!-- /.vbox -->
		</section>
		<?php
		echo $this -> Html -> script('jquery.min');
		//echo $this -> Html -> script('datatables/jquery.dataTables.min');
		echo $this -> Html -> script('bootstrap');
		echo $this -> Html -> script('jquery.sparkline.min');
		echo $this -> Html -> script('app');
		echo $this -> Html -> script('main');
		echo $this -> Html -> script('bootstrap-colorpicker');
		echo $this -> Html -> script('app.plugin');
		echo  $this -> Html -> script('app.data');
		echo $this -> Html -> script('parsley/parsley.min');
		echo $this -> Html -> script('spectrum');


        //WSYIWYG
        echo $this->Html->script('wysiwyg/jquery.hotkeys');
        echo $this->Html->script('wysiwyg/bootstrap-wysiwyg');

        //File Input
        echo $this->Html->script('file-input/bootstrap.file-input');

        //File Input
        echo $this->Html->script('datepicker/bootstrap-datepicker');

        //File Input


        echo $this->fetch('script');

	?>
    <script type="text/javascript">
        $(document).ready(function(){
            setTimeout(function() {
           //     $('.alert').fadeOut(800);
            }, 3000);
            $('.alert').click(function(){
                $(this).fadeOut(800);
            });

        });

    </script>

	<?php
	 if (class_exists('JsHelper') && method_exists($this->Js, 'writeBuffer')) echo $this->Js->writeBuffer();
	?>

	</body>
</html>
