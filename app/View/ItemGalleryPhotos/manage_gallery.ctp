<?php
$this -> start('main-content');
?>
    <div class="tab-content">
    <div class="tab-pane active" id="add">

        <div class="row">
            <div class="col-sm-12">
				
                <section class="panel">
                    <header class="panel-heading font-bold">1. Primary Photo</header>
                    <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <?php echo $this->Html->image('/files/item/primary_photo/'.$selectedItem['Item']['id']."/".$selectedItem['Item']['primary_photo'],array('height'=>'200px')); ?>
                                </div>

                            </div>
                    </div>
                </section>				

            </div>
        </div>
		
		<div class="row">
            <div class="col-sm-12">
				
                <section class="panel">
                    <header class="panel-heading font-bold">2. Gallery Photo/s </header>
                    <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-12">
								
                                    
									<?php 
									
									foreach($itemGalleryPhotos as $itemgp)
									{
									
										echo $this->Html->image('/files/item_gallery_photo/file_name/'.$itemgp['ItemGalleryPhoto']['file_dir']."/".$itemgp['ItemGalleryPhoto']['file_name'],array('height'=>'200px'));

                                        echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-remove')),array('controller'=>'item_gallery_photos','action'=>'delete',$itemgp['ItemGalleryPhoto']['id']),array('escape' => false));
										?>
                                        
										&nbsp;&nbsp;&nbsp;
										<?php
									
									}
									?>
									
                                </div>

                            </div>
                    </div>
                </section>				

            </div>
        </div>		

		<div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">3. Add Gallery Photo/s</header>
                    <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">PNG, JPG or GIF<br/><br/>(Press crtl/shift key to select multiple files -or- select multiple files via mouse)</label>
								<div class="col-sm-6">
									<?php
                                    echo $this->Form->create('ItemGalleryPhoto', array('type' => 'file', 'multiple','controller' => 'item_gallery_photos', 'action' => 'manage_gallery', 'data-validate' => 'parsley', 'role' => 'form'));
									
									echo $this->Form->input('file_names.', array('type' => 'file', 'multiple'));
									
									
									echo $this->Form->input('item_id', array('type' => 'hidden','value'=>$selectedItem['Item']['id']));
									
									echo "<br />";
									
									
									echo $this->Form->end('Upload');
									?>
                                </div>
                            </div>
                    </div>
                </section>
            </div>

        </div>
    </div>
    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="">
                <?php echo $this->Html->link('View Items',array('controller'=>'items','action'=>'index')); ?>
            </li>
			 <li class="">
				<?php echo $this->Html->link('Add Item',array('controller'=>'items','action'=>'add')); ?>
			</li>
			<li class="">
                <?php echo $this->Html->link('Add Gallery',array('controller'=>'items','action'=>'add_gallery')); ?>
            </li>
			<li class="active"><a href="#view" data-toggle="tab">Manage Gallery (<?php echo $selectedItem['Item']['name']; ?>)</a></li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>
