<?php
$this -> start('main-content');
?>
<section class="content-header">
     <h1>
       View Items
       <small>Items</small>
     </h1>
     <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
       <li class="active">Items</li>
     </ol>
</section>

<div class="tab-content">
    <section class="panel">
        <header class="panel-heading font-bold">Search Item</header>
        <?php echo $this -> Form -> create('Item', array('data-validate' => 'parsley')); ?>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6">
                    <?php
                    echo $this -> Form -> input('search_term', array('div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'form-control parsley-validated','id' => 'item-name','required'=>'required','placeholder'=>'Enter Item Name...'));
                    ?>
                </div>
                <div class="col-sm-2">
                    <?php   echo $this -> Form -> input('Search', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'btn btn-s-md btn-info', 'data-required' => 'true', 'id' => 'item-submit'));  ?>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </section>






    <section class="panel">

        <div class="table-responsive">
            <table class="table table-striped m-b-none">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Alias</th>
                    <th>Variants</th>
                    <th>Category</th>
                    <th></th>
                  </tr>
                </thead>

                <tbody>
                    <?php foreach ($items as $data): ?>
                        <tr>
                            <td>
                                <?=     $this->Html->image('/files/item/primary_photo/'.$data['Item']['primary_photo_directory']."/small_".$data['Item']['primary_photo'],array('style'=>'height: 100px;width: 100px;'));
                                ?>
                            </td>
                            <td>
                                <?= $data['Item']['name'] ?>
                            </td>
                            <td>
                                <?= $data['Item']['alias'] ?>
                            </td>
                            <td>

                                <!-- <h4>
                                    <span class="label label-success"><?= $data['Item']['variant_name']?> - <?= $data['ItemCity'][0]['discount_price'] ?></span>
                                </h4> -->

                                <?php if (sizeof($data['ChildItems']) > 0): ?>
                                    <h4>
                                    <?php foreach ($data['ChildItems'] as $val): ?>
                                        <span class="label label-info">
                                            <?= $val['variant_name']?> - <?= $val['ItemCity'][0]['discount_price'] ?>
                                        </span>
                                        &nbsp;
                                    <?php endforeach; ?>
                                    </h4>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?= $data['ItemCategory']['name']?>
                            </td>

                            <td>
                                <?= $this->Html->link('Edit',array('controller'=>'new_items','action'=>'edit',$data['Item']['id']),array('class'=>'btn btn-xs btn-warning')); ?>

                                <?php if ($data['Item']['featured'] == 0): ?>
                                    <?= $this->Html->link('Set Featured',array('controller'=>'new_items','action'=>'set_featured',$data['Item']['id'],1),array('class'=>'btn btn-xs btn-success')); ?>
                                <?php else: ?>
                                    <?= $this->Html->link('Unset Featured',array('controller'=>'new_items','action'=>'set_featured',$data['Item']['id'],0),array('class'=>'btn btn-xs btn-danger')); ?>
                                <?php endif; ?>

                                <?php if ($data['Item']['special'] == 0): ?>
                                    <?= $this->Html->link('Set special',array('controller'=>'items','action'=>'set_featured',$data['Item']['id'],'special'),array('class'=>'btn btn-xs btn-success')); ?>
                                <?php else: ?>
                                    <?= $this->Html->link('Unset special',array('controller'=>'items','action'=>'unset_featured',$data['Item']['id'],'special'),array('class'=>'btn btn-xs btn-danger')); ?>
                                <?php endif; ?>
                                <br><br>
                                <?php if ($data['Item']['best_seller'] == 0): ?>
                                    <?= $this->Html->link('Set best seller',array('controller'=>'items','action'=>'set_featured',$data['Item']['id'],'best_seller'),array('class'=>'btn btn-xs btn-success')); ?>
                                <?php else: ?>
                                    <?= $this->Html->link('Unset best seller',array('controller'=>'items','action'=>'unset_featured',$data['Item']['id'],'best_seller'),array('class'=>'btn btn-xs btn-danger')); ?>
                                <?php endif; ?>


                                <?php if ($data['Item']['disabled'] == 0): ?>
                                    <?= $this->Html->link('Disable',array('controller'=>'new_items','action'=>'set_disabled',$data['Item']['id'],1),array('class'=>'btn btn-xs btn-danger')); ?>
                                <?php else: ?>
                                    <?= $this->Html->link('Enable',array('controller'=>'new_items','action'=>'set_disabled',$data['Item']['id'],0),array('class'=>'btn btn-xs btn-success')); ?>
                                <?php endif; ?>

                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <ul class="pagination" style="float: right;">
            <?php
                echo $this->Paginator->prev(__('Previous'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('Next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
        <?php
            echo $this->Paginator->counter(array('format' => 'range'));
        ?>
    </section>
</div>

<?php
$this -> end('main-content');
?>
