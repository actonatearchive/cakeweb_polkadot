<?php
$this -> start('main-content');
?>
<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
	        <div class="table-responsive">
	          <table class="table table-striped m-b-none">
	            <thead>
	              <tr>
	                <th>id</th>
	                <th>name</th>
	                <th>created</th>
	                <th>modified</th>
	                <th></th>
	              </tr>
	            </thead>
	            	
	            	<tbody>
	            	
	            <?php foreach($item_types as $item) 
	            	{
	            	?>
		            
		            	<tr>
			                <td><?php echo $item['ItemType']['id']; ?></td>
			                <td><?php echo $item['ItemType']['name']; ?></td>
			                <td><?php echo $item['ItemType']['created']; ?></td>
			                <td><?php echo $item['ItemType']['modified']; ?></td>
			                <td><?php echo $this->Html->link('edit',array('controller'=>'ItemCategories','action'=>'edit',$item['ItemType']['id'])); ?></td>
			                
		                </tr>            
		            
	            	<?php
					}
	            	?>
	            	
	            	</tbody>
	          </table>
	          <?php ?>
	        </div>
      </section>
    </div>
    <div class="tab-pane" id="add">
    	<div class="col-sm-12">
          <section class="panel">
            <!-- <header class="panel-heading font-bold">Add Item Category</header> -->
            <div class="panel-body">
            <?php echo $this -> Form -> create('ItemType', array('controller' => 'itemtypes', 'action' => 'add', 'data-validate' => 'parsley', 'role' => 'form')); ?>
	              <div class="col-sm-6">	                
	                <div class="form-group">
	                  <label>Item Type Name</label>
	                  <?php
					echo $this -> Form -> input('name', array('div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'item-name'));
						?>
	                </div>
						<?php
						echo $this -> Form -> input('Submit', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'btn btn-sm btn-default', 'data-required' => 'true', 'id' => 'item-p-color'));
						?>	  
		            </div>
                    
	              </div>
              </form>
            </div>
          </section>
        </div>
    </div>
</div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#view" data-toggle="tab">View Item Types</a></li>
    <li class=""><a href="#add" data-toggle="tab">Add Item Type</a></li>
  </ul>
</header>
<?php
$this -> end('main-header');
?>