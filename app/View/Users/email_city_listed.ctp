<?php
$this -> start('main-content');
?>
<script>
	function getMsg(){		
		var container=document.getElementById('email-desc');
		var msg=document.getElementById('editor').innerHTML;		
		container.value=msg;
	}
</script>
    <div class="tab-content">
        <div class="tab-pane active" id="add">
            <?php echo $this -> Form -> create('User', array('controller' => 'users', 'action' => 'email_city_listed', 'data-validate' => 'parsley', 'role' => 'form','onsubmit'=>'getMsg()')); ?>
			
		<div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">1. Email Subject</header>
                    <!-- <header class="panel-heading font-bold">Subject</header> -->
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Subject</label>
                                <?php
                                echo $this -> Form -> input('subject', array('div' => false, 'label' => false, 'title' => 'Subject', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'email-subject'));
                                ?>
                            </div>
						</div>
                    </div>
                </section>

            </div>
        </div>							
			
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">2. Email Content</header>
                    <div class="panel-body">
                            <div class="form-group">
                                
                                <div class="col-sm-10">
                                    
									
                                    <?php echo $this -> Form -> input('message_body', array('type'=>'textarea','div' => false, 'label' => false, 'title' => 'Email Description', 'class' => 'hide', 'id' => 'email-desc')); ?>
									
                                </div>
                            </div>
                    </div>
                </section>
            </div>
            

            <div class="col-sm-12">
                <section class="panel">
                    
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Submit', 'class' => 'btn btn-s-md btn-success', 'id' => 'item-submit'));  ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
			
            </form>
        </div>
    </div>
<?php echo $this->Form->end(); ?>	
	
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
			<li class="">
				<?php echo $this->Html->link('All users',array('controller'=>'users','action'=>'email_all')); ?>
			</li>
            <li class="active">
                <a href="#add" data-toggle="tab">Users with city</a>
            </li>
            <li class="">
                <?php echo $this->Html->link('To a csv list',array('controller'=>'users','action'=>'email_csv')); ?>
            </li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>

<?php
    $this->start('script');
?>
<?php
    echo $this -> Html -> script('ckeditor/ckeditor');
    echo $this -> Html -> script('ckeditor/adapters/jquery');
?>
<script>
    $(document).ready(function(e){
        $( '#email-desc' ).ckeditor();
    })
</script>
<?php
    $this->end('script');
?>