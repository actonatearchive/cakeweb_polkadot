<?php
$this -> start('main-content');
?>
    <div class="tab-content">
    <div class="tab-pane active" id="add">
        
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">1. User Details</header>
                    <!-- <header class="panel-heading font-bold">Add Item</header> -->
                    <div class="panel-body">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>First Name</label>
                                <?php
                                //echo $this -> Form -> input('order_id', array('value'=> $order['Order']['id'],'disabled'=>'disabled', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'label' => false, 'div' => false));
								
                                echo $this -> Form -> input('temp', array('div' => false,'value'=> $user['User']['first_name'],'disabled'=>'disabled', 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
							<div class="form-group">
                                <label>Last Name</label>
                                <?php
                                //echo $this -> Form -> input('order_id', array('value'=> $order['Order']['id'],'disabled'=>'disabled', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'label' => false, 'div' => false));
								
                                echo $this -> Form -> input('temp', array('div' => false,'value'=> $user['User']['last_name'],'disabled'=>'disabled', 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Username</label>
                                <?php
                                	echo $this -> Form -> input('temp', array('value'=> $user['User']['username'],'disabled'=>'disabled', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'label' => false, 'div' => false));
                                ?>
                            </div>                        
                            <div class="form-group">
                                <label>Mobile</label>
                                <?php
                                echo $this -> Form -> input('temp', array('value'=> $user['User']['mobile'],'disabled'=>'disabled', 'class' => 'form-control parsley-validated', 'label' => false, 'div' => false));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Fb Token</label>
                                <?php
                                echo $this -> Form -> input('temp', array('value'=> $user['User']['fb_token'],'disabled'=>'disabled','div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
							<div class="form-group">
                                <label>Google Token</label>
                                <?php
                                echo $this -> Form -> input('temp', array('value'=> $user['User']['goog_token'],'disabled'=>'disabled','div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
                            					


                        </div>


                        <div class="col-sm-6">
						
							
                            <div class="form-group">
                                <label>Emails ?</label>
                                <?php
								
								if ($user['User']['recv_email'] == 0) {
									$remail = "Off";
								} elseif ($user['User']['recv_email'] == 1) {
									$remail = "On";
								}
								
                                echo $this -> Form -> input('temp', array('value'=> $remail,'disabled'=>'disabled','div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>News ?</label>
                                <?php
								
								if ($user['User']['recv_email'] == 0) {
									$rnews = "Off";
								} elseif ($user['User']['recv_email'] == 1) {
									$rnews = "On";
								}
								
                                echo $this -> Form -> input('temp', array('value'=> $rnews,'disabled'=>'disabled','div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>SMS ?</label>
                                <?php
								
								if ($user['User']['recv_email'] == 0) {
									$rsms = "Off";
								} elseif ($user['User']['recv_email'] == 1) {
									$rsms = "On";
								}
								
                                echo $this -> Form -> input('temp', array('value'=> $rsms,'disabled'=>'disabled','div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>							
                            <div class="form-group">
                                <label>Created</label>
                                <?php
                                $display_created = new DateTime($user['User']['created']);
                                $display_c = $display_created->format("D, F j, Y, g:i a");                                
                                echo $this -> Form -> input('temp', array('value'=> $display_c,'disabled'=>'disabled','div' => false, 'label' => false,  'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
							<div class="form-group">
                                <label>Modified</label>
                                <?php
                                $display_modified = new DateTime($user['User']['modified']);
                                $display_m = $display_modified->format("D, F j, Y, g:i a");                                
                                echo $this -> Form -> input('temp', array('value'=> $display_m,'disabled'=>'disabled','div' => false, 'label' => false,  'class' => 'form-control parsley-validated'));
                                ?>
                            </div>                            
                        </div>
                    </div>
                </section>
            </div>
        </div>



        






<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
    <li class="">
        <?php echo $this -> Html -> link('User Listing', array('controller' => 'users', 'action' => 'index')); ?>
    </li>  
    <li class="active"><a href="#view" data-toggle="tab">View User</a></li>
  </ul>
</header>
<?php
$this -> end('main-header');
?>