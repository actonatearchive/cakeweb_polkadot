<?php
$this -> start('main-content');
?>
    <div class="tab-content">
        <div class="tab-pane active" id="add">
            <?php echo $this -> Form -> create('Article', array('controller' => 'articles', 'action' => 'add', 'data-validate' => 'parsley', 'role' => 'form')); ?>
            <div class="row">
                <div class="col-sm-6">
                    <section class="panel">
                        <header class="panel-heading font-bold">1. Primary Details</header>
                        <!-- <header class="panel-heading font-bold">Add Item Category</header> -->
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Title</label>
                                    <?php
                                    echo $this -> Form -> input('title', array('div' => false, 'label' => false, 'title' => 'Title', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'article-title'));
                                    ?>
                                </div>
                                <div class="form-group">
                                    <label>Keywords</label>
                                    <?php
                                    echo $this -> Form -> input('keywords', array('div' => false, 'label' => false, 'title' => 'Keywords', 'class' => 'form-control parsley-validated', 'id' => 'article-keywords'));
                                    ?>
                                </div>
                                <div class="form-group">
                                    <label>Meta Description</label>
                                    <?php
                                    echo $this -> Form -> input('meta_description', array('type'=>'textarea','div' => false, 'label' => false, 'title' => 'Meta Description', 'class' => 'form-control parsley-validated', 'id' => 'article-meta'));
                                    ?>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">
                        <header class="panel-heading font-bold">2. Article Content</header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="form-group col-sm-12">
                                    <?php
                                    echo $this -> Form -> input('content', array('type'=>'textarea','div' => false, 'label' => false, 'title' => 'Content', 'class' => 'form-control parsley-validated', 'id' => 'article-content'));
                                    ?>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">
                        <!-- <header class="panel-heading font-bold">Add Item Category</header> -->
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Submit', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'article-submit'));  ?>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            </form>
        </div>
    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="">
                <?php echo $this->Html->link('Article Index',array('controller'=>'articles','action'=>'index')); ?>
            </li>
            <li class="active"><a href="#add" data-toggle="tab">Add Article</a></li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>

<?php
    $this->start('script');
?>
<?php
    echo $this -> Html -> script('ckeditor/ckeditor');
    echo $this -> Html -> script('ckeditor/adapters/jquery');
?>
<script>
    $(document).ready(function(e){
        $( '#article-content' ).ckeditor();
    })
</script>
<?php
    $this->end('script');
?>