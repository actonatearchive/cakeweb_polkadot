<?php
$this -> start('main-content');

    // // Page-Level Plugin CSS - Dashboard
    // echo $this -> Html -> css('plugins/morris/morris-0.4.3.min');
    //
    // // Page-Level Plugin Scripts - Dashboard
    // echo $this -> Html -> script('plugins/morris/raphael-2.1.0.min');
    // echo $this -> Html -> script('plugins/morris/morris');
    // echo $this->fetch('script');
    //
	// $this->Html->scriptStart(array('inline' => false));
?>

<?php
	$this->Html->scriptEnd();
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Dashboard</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-user fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><h2><?= $total_user?></h2></div>
                        <div>Users refered!</div>
                    </div>
                </div>
            </div>
            <a href="<?= $this->webroot.'userWallets/searchUser'?>">
                <div class="panel-footer">
                    <span class="pull-left">search users wallets</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-tasks fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><h2><?= $total_count?></h2></div>
                        <div>No. of transactions!</div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>



<?php
$this -> end('main-content');
?>
