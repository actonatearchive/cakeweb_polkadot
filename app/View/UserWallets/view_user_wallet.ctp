<?php
$this -> start('main-content');

$this->Html->scriptStart(array('inline' => false));
?>


$(document).ready(function(){

  var rowCount = $('#dataTables tr').length;

  $('#entryto').html(rowCount-1);



});

<?php
    $this->Html->scriptEnd();
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $user['User']['first_name'].' '.$user['User']['last_name'] ?>'s Wallet details</h1>
        <div class="well">
            <h4>User Name : <b><?= $user['User']['first_name'].' '.$user['User']['last_name'] ?></b></h4>
            <h4>User email : <b><?= $user['User']['username'] ?></b></h4>
            <h4>User mobile : <b><?= $user['User']['mobile'] ?></b></h4>

        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                View


            </div>

            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables">
                        <thead>
                          <tr>
                            <th>LS Money</th>

                            <th>details</th>

                            <th>recived</th>
                            <th>expires</th>

                          </tr>
                        </thead>
                        <tbody>

                            <?php foreach($wallets as $wallet)
                            {
                            ?>

                            <tr>
                                <td>
                                    <b><?php echo $wallet['UserWallet']['points'] ?></b>
                                </td>
                                <td>

                                    <?php echo $wallet['UserWallet']['reason'] ?>

                                </td>
                                <td>
                                    <?php echo $wallet['UserWallet']['created'] ?>

                                </td>
                                <td>
                                    <?php echo $wallet['UserWallet']['expires'] ?>

                                </td>




                            </tr>
                            <?php
                            }
                            ?>
                       </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

</div>
<!-- /.row -->
<?php
$this -> end('main-content');
?>
