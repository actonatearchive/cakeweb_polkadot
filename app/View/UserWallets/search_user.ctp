<?php
$this -> start('main-content');

$this->Html->scriptStart(array('inline' => false));
?>

$(document).ready(function(){

  var rowCount = $('#dataTables tr').length;

  $('#entryto').html(rowCount-1);



});

<?php
    $this->Html->scriptEnd();
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">LS Money</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                Search User's LS Money
            </div>

            <!-- /.panel-heading -->
            <div class="panel-body">

            <?php echo $this -> Form -> create('UserWallet', array('url'=>['controller' => 'UserWallets', 'action' => 'searchUser'], 'role' => 'form')); ?>

                <div class="form-group">
                    <label>Search User by Name or Email or mobile</label>
                    <?php
                    echo $this -> Form -> input('search_user', array('div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'form-control', 'required' => 'required', 'id' => 'item-name'));
                    ?>
                </div>

                <?php   echo $this -> Form -> input('Search', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'btn btn-s-md btn-info', 'required' => 'required', 'id' => 'item-submit'));  ?>

            <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<?php if ($finalSearchItems != null): ?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                View


            </div>

                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables">
                            <thead>
                              <tr>
                                <th>Name</th>
                                <th>email</th>

                                <th>mobile</th>
                                <th style="width:20%">LS Money</th>
                                <th>view details</th>

                              </tr>
                            </thead>
                            <tbody>

                                <?php foreach($finalSearchItems as $user)
                                {
                                ?>

                                <tr>
                                    <td>
                                        <?php echo $user['User']['first_name'].' '.$user['User']['last_name']; ?>
                                    </td>
                                    <td>

                                        <b><?php echo $user['User']['username']; ?>
                                        </b>

                                    </td>
                                    <td>
                                        <?php echo $user['User']['mobile']; ?>
                                    </td>
                                    <td>
                                        <?php echo $user[0]['points']; ?>
                                    </td>
                                    <td>
                                        <?php
                                          echo $this->Html->link('View',array('controller'=>'UserWallets','action'=>'viewUserWallet',$user['User']['id']),array('class'=>'btn btn-info', 'escape' => false));
                                        ?>
                                    </td>



                                </tr>
                                <?php
                                }
                                ?>
                           </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->






                </div>
                <!-- /.panel-body -->


        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php endif; ?>
<!-- /.row -->

</div>
<!-- /.row -->
<?php
$this -> end('main-content');
?>
