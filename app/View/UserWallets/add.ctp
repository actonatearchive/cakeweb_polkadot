<?php
$this -> start('main-content');
?>
<style>
.address_block:hover {
    border-style: solid;
    border-color: orange;
    border-width: 1px;
    border-radius: 1px;
    cursor: pointer;

}
.address_block {
    min-height: 180px;
    max-height: 180px;
}
.item_block:hover {
    border-style: solid;
    border-color: orange;
    border-width: 1px;
    border-radius: 1px;
    cursor: pointer;
}
.item_block {
    /*min-width: 155px;*/
    /*max-width: 155px;*/
    min-height: 222px;
}
.angucomplete-holder {
    position: relative;
}
.angucomplete-dropdown {
    border-color: #ececec;
    border-width: 1px;
    border-style: solid;
    border-radius: 2px;
    width: 300px;
    max-height: 200px;
    overflow-y: auto;
    padding: 6px;
    cursor: pointer;
    z-index: 9999;
    position: absolute;
    /*top: 32px;
    left: 0px;
    */
    margin-top: -6px;
    background-color: #ffffff;
}

.angucomplete-searching {
    color: #acacac;
    font-size: 14px;
}

.angucomplete-description {
    font-size: 14px;
}

.angucomplete-row {
    padding: 5px;
    color: #000000;
    margin-bottom: 4px;
    clear: both;
}

.angucomplete-selected-row {
    background-color: lightblue;
    color: #ffffff;
}

.angucomplete-image-holder {
    padding-top: 2px;
    float: left;
    margin-right: 10px;
    margin-left: 5px;
}

.angucomplete-image {
    height: 34px;
    width: 34px;
    border-radius: 50%;
    border-color: #ececec;
    border-style: solid;
    border-width: 1px;
}

.angucomplete-image-default {
    /* Add your own default image here
     background-image: url('/assets/default.png');
    */
    background-position: center;
    background-size: contain;
    height: 34px;
    width: 34px;
}

</style>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add CW Money</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row" ng-controller="AddLSMoney">
    <div ng-if="steps == 0">
	<div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-plus fa-fw"></i>Details
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="form-group">
                       <span class="radio" style="display: inline-block !important;">
                           <label>
                               <input type="radio" ng-model="obj.cust_type" name="optionsRadios" id="optionsRadios1" value="0"> Exisiting Customer
                           </label>

                       </span>
   <!--                     <span class="radio" style="margin-left: 5%; display: inline-block !important;">
                           <label>
                               <input type="radio" ng-model="obj.cust_type" name="optionsRadios" id="optionsRadios2" value="1" ng-change="openNewCustomer()"> New Customer
                           </label>
                       </span> -->
                </div>
                <div class="form-group">
                <div ng-if="obj.cust_type == 0">
                <angucomplete-alt id="customer"
                      placeholder="Enter Customer Email/Mobile no."
                      pause="400"
                      selected-object="selectedName"
                      remote-url="{{userUrl}}"
                      remote-url-data-field="data"
                      title-field="name"
                      input-class="form-control"
                      description-field="desc"
                      field-required="true"
                      />
                </div>
                </div>
                <div ng-show="showAddMoneyDetails">
                    <form name="addLSMoneyForm">
                        <div class="form-group">
                            <label>Amount</label><font color='red'>*</font>
                            <input class="form-control" placeholder="Enter amount in Rs." ng-model="obj.amount" required>
                        </div>
                        <div class="form-group">
                            <label>Select Order</label>
                            <angucomplete-alt id="order"  placeholder="Enter Order Code." pause="400"
                              selected-object="selectedOrder"
                              remote-url="{{orderUrl}}"
                              remote-url-data-field="data"
                              title-field="order_code"
                              input-class="form-control"
                              description-field="desc"
                              field-required="true">
                        </div>
                        <div class="form-group">
                            <label>Reason</label><font color='red'>*</font>
                            <textarea class="form-control" placeholder="Enter reason for giving LS Money" ng-model="obj.reason" required></textarea>
                        </div>
                        <div class="form-group">
                            <label>Expiry Date</label><font color='red'>*</font>
                            <input type="date" class="form-control" placeholder="Select an expiry date" ng-model="obj.expires_at" required>
                        </div>
                <button type="button" class="btn btn-primary" ng-click="addLSMoney($event)" ng-disabled="addLSMoneyForm.$invalid">Submit</button>
                    </form>
                </div>
            </div>
            <!--                      clear-selected="false" /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>

    <!-- ############################ -->
    <!-- New Customer Modal -->

    <!-- Modal -->
    <div class="modal fade" id="customerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add New Customer</h4>
            </div>
            <div class="modal-body">
            <form name="custAdd">
                <div class="form-group">
                    <label>First Name</label><font color='red'>*</font>
                    <input class="form-control" placeholder="Enter First Name..." ng-model="new_customer.first_name" required>
                </div>

                <div class="form-group">
                    <label>Last Name</label><font color='red'>*</font>
                    <input class="form-control" placeholder="Enter Last Name..." ng-model="new_customer.last_name" required>
                </div>

                <div class="form-group">
                    <label>Email ID</label><font color='red'>*</font>
                    <input class="form-control" placeholder="Enter Email Id..." ng-model="new_customer.email" required>
                </div>

                <!-- <div class="form-group">
                    <label>Password</label><font color='red'>*</font>
                    <input type="password" class="form-control" placeholder="Enter Password..." ng-model="new_customer.password" required>
                </div> -->


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" ng-click="addNewCustomer($event)" ng-disabled="custAdd.$invalid">Add Customer</button>
            </div>
        </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<?php
$this -> end('main-content');
?>
