<?php
$this -> start('main-content');
?>
    <div class="tab-content">
    <div class="tab-pane active" id="add">
        <?php echo $this -> Form -> create('Item', array('type'=>'file','controller' => 'items', 'action' => 'edit', 'data-validate' => 'parsley', 'role' => 'form')); ?>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">1. Basic Details</header>
                    <!-- <header class="panel-heading font-bold">Add Item</header> -->
                    <div class="panel-body">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Item Name</label>
                                <?php
                                echo $this -> Form -> input('id', array('type'=>'hidden','default'=>$selectedItem['Item']['id'],'div' => false, 'label' => false, 'title' => 'Item Price', 'class' => 'form-control parsley-validated', 'id' => 'item-id'));
                                echo $this -> Form -> input('name', array('default'=>$selectedItem['Item']['name'],'div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'item-name'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Category</label>
                                <?php
                                echo $this -> Form -> input('item_category_id', array('default'=>$selectedItem['Item']['item_category_id'],'options' => $item_categories,'empty'=>'Choose a category', 'class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'item-category', 'label' => false, 'div' => false));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Parent Item</label>
                                <?php
                                    echo $this -> Form -> input('item_parents', array('default'=>$selectedItem['Item']['item_parents'],'empty' => 'Choose Parent Item', 'class' => 'form-control m-b parsley-validated parent-item-list','option' => $item_parents, 'label' => false, 'div' => false,'autocomplete' => 'on'));
                                    echo $this -> Form -> input('item_id', array('default'=>$selectedItem['Item']['item_id'],'type'=>'hidden','div' => false, 'label' => false, 'id'=>'item-parent'));  
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Variant Name</label>
                                <?php
                                echo $this -> Form -> input('variant_name', array('default'=>$selectedItem['Item']['variant_name'],'div' => false, 'label' => false, 'title' => 'Variant Name', 'class' => 'form-control parsley-validated', 'id' => 'item-variant-name'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Short Description</label>
                                <?php
                                echo $this -> Form -> input('short_desc', array('default'=>$selectedItem['Item']['short_desc'],'div' => false, 'label' => false, 'title' => 'Short Description', 'class' => 'form-control parsley-validated', 'id' => 'item-short-desp'));
                                ?>
                            </div>
                        </div>


                        <div class="col-sm-6">

                            <div class="form-group">
                                <label>Keywords</label>
                                <?php
                                echo $this -> Form -> input('keyword', array('default'=>$selectedItem['Item']['keyword'],'div' => false, 'label' => false, 'title' => 'Keywords', 'class' => 'form-control parsley-validated', 'id' => 'item-keyword'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Alias</label>
                                <?php
                                echo $this -> Form -> input('alias', array('default'=>$selectedItem['Item']['alias'],'div' => false, 'label' => false, 'title' => 'Alias', 'class' => 'form-control parsley-validated', 'id' => 'item-alias'));
                                ?>
                            </div>

                            <div class="form-group">
                                <label>Shipping Charges</label>
                                <?php
                                if($selectedItem['Item']['shipping_charges'] != -1)
                                {
                                    echo $this -> Form -> input('shipping_charges', array('div' => false,'default'=>$selectedItem['Item']['shipping_charges'],'type'=>'number', 'label' => false, 'title' => 'Shipping Charges', 'class' => 'form-control parsley-validated', 'id' => 'item-shipping-charges'));
                                }
                                else{
                                    echo $this -> Form -> input('shipping_charges', array('div' => false,'default'=>"",'type'=>'number', 'label' => false, 'title' => 'Shipping Charges', 'class' => 'form-control parsley-validated', 'id' => 'item-shipping-charges'));
                                }

                                ?>
                            </div>

                            <?php
                                if($selectedItem['Item']['stock_type']==1 && isset($selectedItem['ItemStock'][0]))
                                {
                                    echo $this -> Form -> input('stock_type', array('type'=>'hidden','default'=>$selectedItem['Item']['stock_type'],'div' => false, 'label' => false, 'title' => 'Item Price', 'class' => 'form-control parsley-validated', 'id' => 'item-stock_type'));
                            ?>
                                    <div class="form-group">
                                        <label class="label-price">Price</label>
                                        <?php

                                        echo $this -> Form -> input('price', array('default'=>$selectedItem['ItemStock'][0]['price'],'div' => false, 'label' => false, 'title' => 'Item Price', 'class' => 'form-control parsley-validated', 'id' => 'item-price'));
                                        ?>
                                    </div>

                                    <div class="form-group">
                                        <label class="label-discount">Discounted Price</label>
                                        <?php
                                        echo $this -> Form -> input('discount_price', array('default'=>$selectedItem['ItemStock'][0]['discount_price'],'div' => false, 'label' => false, 'title' => 'Discounted Price', 'class' => 'form-control parsley-validated', 'id' => 'item-discount_price'));
                                        ?>
                                    </div>
                            <?php
                                }
                                else if($selectedItem['Item']['stock_type']==1 && !isset($selectedItem['ItemStock'][0])){
                                    echo $this -> Form -> input('stock_type', array('type'=>'hidden','default'=>$selectedItem['Item']['stock_type'],'div' => false, 'label' => false, 'title' => 'Item Price', 'class' => 'form-control parsley-validated', 'id' => 'item-stock_type'));
                            ?>
                                    <div class="form-group">
                                        <label class="label-price">Price</label>
                                        <?php
                                        echo $this -> Form -> input('price', array('div' => false, 'label' => false, 'title' => 'Item Price', 'class' => 'form-control parsley-validated', 'id' => 'item-price'));
                                        ?>
                                    </div>

                                    <div class="form-group">
                                        <label class="label-discount">Discounted Price</label>
                                        <?php
                                        echo $this -> Form -> input('discount_price', array('div' => false, 'label' => false, 'title' => 'Discounted Price', 'class' => 'form-control parsley-validated', 'id' => 'item-discount_price'));
                                        ?>
                                    </div>
                            <?php
                                }
                                else{
                                    echo $this -> Form -> input('stock_type', array('type'=>'hidden','default'=>$selectedItem['Item']['stock_type'],'div' => false, 'label' => false, 'title' => 'Item Price', 'class' => 'form-control parsley-validated', 'id' => 'item-stock_type'));
                                }

                            ?>



                            <div class="checkbox">
                                <label>
                                    <?php echo $this -> Form -> checkbox('featured', array('hiddenField' => false)); ?> Set as Featured
                                </label>
                            </div>
                            <?php
                            if($selectedItem['Item']['is_midnight'] == 1){
                            ?>
                            	<div class="checkbox">
                                <label>
                                    <?php echo $this -> Form -> checkbox('is_midnight', array('type' => checkbox,'checked' => 'checked')); ?> Set as Midnight
                                </label>
                            </div>
                            <?php
                            }
                            else{
                            ?>
                            	<div class="checkbox">
                                <label>
                                    <?php echo $this -> Form -> checkbox('is_midnight', array('type' => checkbox)); ?> Set as Midnight
                                </label>
                            </div>
                            <?php
                            }
                            ?>
                            
                            
                            
                        </div>
                    </div>
                </section>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">2. Set Description</header>
                    <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Item Description</label>
                                <div class="col-sm-10">
                                    <div class="btn-toolbar m-b-sm btn-editor" data-role="editor-toolbar" data-target="#editor">
                                        <div class="btn-group">
                                            <a class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font"><i class="icon-font"></i><b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li><a data-edit="fontName Serif" style="font-family:'Serif'">Serif</a></li><li><a data-edit="fontName Sans" style="font-family:'Sans'">Sans</a></li><li><a data-edit="fontName Arial" style="font-family:'Arial'">Arial</a></li><li><a data-edit="fontName Arial Black" style="font-family:'Arial Black'">Arial Black</a></li><li><a data-edit="fontName Courier" style="font-family:'Courier'">Courier</a></li><li><a data-edit="fontName Courier New" style="font-family:'Courier New'">Courier New</a></li><li><a data-edit="fontName Comic Sans MS" style="font-family:'Comic Sans MS'">Comic Sans MS</a></li><li><a data-edit="fontName Helvetica" style="font-family:'Helvetica'">Helvetica</a></li><li><a data-edit="fontName Impact" style="font-family:'Impact'">Impact</a></li><li><a data-edit="fontName Lucida Grande" style="font-family:'Lucida Grande'">Lucida Grande</a></li><li><a data-edit="fontName Lucida Sans" style="font-family:'Lucida Sans'">Lucida Sans</a></li><li><a data-edit="fontName Tahoma" style="font-family:'Tahoma'">Tahoma</a></li><li><a data-edit="fontName Times" style="font-family:'Times'">Times</a></li><li><a data-edit="fontName Times New Roman" style="font-family:'Times New Roman'">Times New Roman</a></li><li><a data-edit="fontName Verdana" style="font-family:'Verdana'">Verdana</a></li></ul>
                                        </div>
                                        <div class="btn-group">
                                            <a class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
                                                <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
                                                <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
                                            </ul>
                                        </div>
                                        <div class="btn-group">
                                            <a class="btn btn-white btn-sm" data-edit="bold" title="" data-original-title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="italic" title="" data-original-title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="strikethrough" title="" data-original-title="Strikethrough"><i class="icon-strikethrough"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="underline" title="" data-original-title="Underline (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
                                        </div>
                                        <div class="btn-group">
                                            <a class="btn btn-white btn-sm" data-edit="insertunorderedlist" title="" data-original-title="Bullet list"><i class="icon-list-ul"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="insertorderedlist" title="" data-original-title="Number list"><i class="icon-list-ol"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="outdent" title="" data-original-title="Reduce indent (Shift+Tab)"><i class="icon-indent-left"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="indent" title="" data-original-title="Indent (Tab)"><i class="icon-indent-right"></i></a>
                                        </div>
                                        <div class="btn-group">
                                            <a class="btn btn-white btn-sm btn-info" data-edit="justifyleft" title="" data-original-title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="justifycenter" title="" data-original-title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="justifyright" title="" data-original-title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="justifyfull" title="" data-original-title="Justify (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a>
                                        </div>
                                        <div class="btn-group">
                                            <a class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Hyperlink"><i class="icon-link"></i></a>
                                            <div class="dropdown-menu">
                                                <div class="input-group m-l-xs m-r-xs">
                                                    <input class="form-control input-sm" placeholder="URL" type="text" data-edit="createLink">
                                                    <div class="input-group-btn">
                                                        <button class="btn btn-white btn-sm" type="button">Add</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <a class="btn btn-white btn-sm" data-edit="unlink" title="" data-original-title="Remove Hyperlink"><i class="icon-cut"></i></a>
                                        </div>

                                        <div class="btn-group">
                                            <a class="btn btn-white btn-sm" title="" id="pictureBtn" data-original-title="Insert picture (or just drag &amp; drop)"><i class="icon-picture"></i></a>
                                            <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" style="opacity: 0; position: absolute; top: 0px; left: 0px; width: 39px; height: 29px;">
                                        </div>
                                        <div class="btn-group">
                                            <a class="btn btn-white btn-sm" data-edit="undo" title="" data-original-title="Undo (Ctrl/Cmd+Z)"><i class="icon-undo"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="redo" title="" data-original-title="Redo (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
                                        </div>
                                        <input type="text" class="form-control-trans pull-left" data-edit="inserttext" id="voiceBtn" x-webkit-speech="" style="width:25px;height:28px;">
                                    </div>
                                    <div id="editor" class="form-control" style="overflow:scroll;height:150px;max-height:150px" contenteditable="true">
                                        <?php
                                            echo html_entity_decode($selectedItem['Item']['long_desc']);
                                        ?>
                                    </div>
                                    <?php echo $this -> Form -> input('long_desc', array('default'=>html_entity_decode($selectedItem['Item']['long_desc']),'type'=>'textarea','div' => false, 'label' => false, 'title' => 'Item Description', 'class' => 'hide', 'id' => 'item-long_desc')); ?>
                                </div>
                            </div>
                    </div>
                </section>
            </div>
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">4. Choose Cities</header>
                    <div class="panel-body">
                        <div class="row">
                        <div class="form-group col-sm-4">
                            <label class="sr-only" for="exampleInputEmail2">Search City</label>
                            <input type="search" class="form-control " id="item-city-search" placeholder="Search City">
                        </div>
                        </div>
                        <div class="row">
                        <div class="form-group m-l cities-list col-sm-4">

                        <?php

                        //pr($selectedCitiesList);die();
                        echo $this->Form->input('city_id', array(
                            'label' => false,
                            'type' => 'select',
                            'multiple' => 'checkbox',
                            'options' => $cities,
                            'selected'=>$selectedCitiesList,
                        ));
                        ?>

                        </div>
                        </div>


                    </div>
                </section>
            </div>
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">4. Upload Primary Photo</header>
                    <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">PNG, JPG or GIF</label>
                                <div class="col-sm-7">
                                    <?php echo $this -> Form -> input('primary_photo', array('type'=>'file','div' => false, 'label' => false, 'title' => 'Primary Photo', 'id' => 'item-primary-photo')); ?>
                              <!--      <div class="dropfile visible-lg">
                                        <small>Drag and Drop file here</small>
                                    </div>-->
                                </div>
                                <div class="col-sm-5">
                                    <?php echo $this->Html->image('/files/item/primary_photo/'.$selectedItem['Item']['id']."/".$selectedItem['Item']['primary_photo'],array('height'=>'200px')); ?>
                                </div>
                            </div>
                    </div>
                </section>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body">
                        <?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'item-submit'));  ?>
                    </div>
                </section>
            </div>

        </div>
        <?php echo $this->Form->end(); ?>
    </div>
    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="">
                <?php echo $this->Html->link('View Items',array('controller'=>'items','action'=>'index')); ?>
            </li>
			 <li class="">
				<?php echo $this->Html->link('Add Item',array('controller'=>'items','action'=>'add')); ?>
			</li>
			<li class="">
                <?php echo $this->Html->link('Add Gallery',array('controller'=>'items','action'=>'add_gallery')); ?>
            </li>
            <li class="">
                <?php echo $this->Html->link('City Wise Items',array('controller'=>'items','action'=>'city')); ?>
            </li>            
			<li class="active"><a href="#add" data-toggle="tab">Edit Item (<?php echo $selectedItem['Item']['name']; ?>)</a></li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>


<?php
  $this->start('script');
?>
<script type="text/javascript">
    $(function(){
        function initToolbarBootstrapBindings() {
            var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                    'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                    'Times New Roman', 'Verdana'],
                fontTarget = $('[title=Font]').siblings('.dropdown-menu');
            $.each(fonts, function (idx, fontName) {
                fontTarget.append($('<li><a data-edit="fontName ' + fontName +'" style="font-family:\''+ fontName +'\'">'+fontName + '</a></li>'));
            });
            $('a[title]').tooltip({container:'body'});
            $('.dropdown-menu input').click(function() {return false;})
                .change(function () {$(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');})
                .keydown('esc', function () {this.value='';$(this).change();});

            $('[data-role=magic-overlay]').each(function () {
                var overlay = $(this), target = $(overlay.data('target'));
                overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
            });
            if ("onwebkitspeechchange" in document.createElement("input")) {
                var editorOffset = $('#editor').offset();
                // $('#voiceBtn').css('position','absolute').offset({top: editorOffset.top, left: editorOffset.left+$('#editor').innerWidth()-35});
            } else {
                $('#voiceBtn').hide();
            }
        };
        function showErrorAlert (reason, detail) {
            var msg='';
            if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
            else {
                console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+
                '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
        };
        initToolbarBootstrapBindings();
        $('#editor').wysiwyg({ fileUploadError: showErrorAlert} );

    });

    $(document).ready(function(){
       $("#item-submit").click(function(e){
          $("#item-long_desc").val($("#editor").html());
       })

        $("#item-stock_type").change(function(){

            if($(this).val()==0){
                $("#item-price").hide();
                $("#item-discount_price").hide();
                $(".label-discount").hide();
                $(".label-price").hide();
            }
            else{
                $(".label-discount").show();
                $(".label-price").show();
                $("#item-price").show();
                $("#item-discount_price").show();
            }
        })

        $("#item-city-search").keyup(function(){

          var search = $(this).val().toLowerCase();

          if(search==""){
              $(".cities-list .checkbox label").css("background",'none');
              $(".cities-list .checkbox label").css("color","#717171");
              return;
          }

          $(".cities-list label").each(function(){
             var tag = $(this).text().toLowerCase();
             if(tag.indexOf(search) != -1){
                 $(this).css("background",'#7dc63c');
                 $(this).css("color","#FFF");
             }
              else{
                 $(this).css("background",'none');
                 $(this).css("color","#717171");
             }

          })

        })

        //This is for autoccomplete of parent item list
        $( ".parent-item-list" ).autocomplete({
          source: "../find_parent",
          minLength: 3,
          delay: 1,
            focus: function( event, ui ) {
                $( ".parent-item-list" ).val( ui.item.name );
                return false;
            },
            select: function( event, ui ) {
                $( ".parent-item-list" ).val( ui.item.name );
                $("#item-parent").val(ui.item.id);
                return false;
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li>" )
            .append( "<a>" + item.name + "</a>" )
            .appendTo( ul );
        } 




    });
</script>
<?php
  $this->end('script');
?>