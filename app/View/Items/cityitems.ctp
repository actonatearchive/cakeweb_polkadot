<?php
$this -> start('main-content');
?>
<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">

	        <div class="table-responsive">
	          <table class="table table-striped m-b-none" data-ride="datatables">
	            <thead>
	              <tr>
                    <th>Name</th>
                    <th>Alias</th>
                    <!-- <th>Photo</th>   -->
	                <th>Variant Name</th>
	                <th>Category</th>
                    <th>Price</th>
                    <th>Discount<br />Price</th>
                    <th>Save<br />Price</th>
                    <th>Add Stock</th>
                    <th></th>
	              </tr>
	            </thead>

	            	<tbody>

	            <?php foreach($items as $item)
	            	{
	            	?>

		            	<tr>
                            <td><?php echo $item['Item']['name']; ?></td>
                            <td><?php echo $item['Item']['alias']; ?></td>
			                <td><?php echo $item['Item']['variant_name']; ?></td>
                            <td><?php
                                if(isset($item['Item']['item_category_id'])){
                                    echo $item['ItemCategory']['name'];
                                }
                                 ?>
                            </td>
                            <td>
                                <?php
                                    if($item['Item']['stock_type'] == 1)
                                    {                                
                                        //echo $this -> Form -> create('ItemStock', array('controller' => 'item_stocks', 'action' => 'save_price', 'data-validate' => 'parsley', 'role' => 'form'));

                                        //echo $this -> Form -> input('id', array('type'=>'hidden','default'=>$item['ItemStock'][0]['id'],'div' => false, 'label' => false, 'id' => 'item-id-'));

                                    
                                        echo $this -> Form -> input('price', array('default'=>$item['ItemStock'][0]['price'],'div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'price-'.$item['ItemStock'][0]['id']));
                                    }
                                ?>
                            </td>
                            <td>
                                <?php
                                    if($item['Item']['stock_type'] == 1)
                                    {
                                    echo $this -> Form -> input('discount_price', array('default'=>$item['ItemStock'][0]['discount_price'],'div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'discount-price-'.$item['ItemStock'][0]['id']));
                                    }
                                ?>
                            </td>
                            <td>
                                <?php
                                    if($item['Item']['stock_type'] == 1)
                                    {
                                        ?>                                            
                                            <input type="button" class="btn btn-xs btn-default ajax-save" id="<?php echo $item['ItemStock'][0]['id']; ?>" value="Save Price"  />
                                        <?php

                                        //echo $this->Form->end();
                                    }
                                    else{
                                        echo "n/a";
                                    }
                                    

                                    

                                ?>

                            </td>
                            <td>
                                <?php
                                if($item['Item']['stock_type'] != 1)
                                {
                                    echo $this->Html->link('Add Stock',array('controller'=>'item_stocks','action'=>'add',$item['Item']['id']),array('class'=>'btn btn-xs btn-default'));
                                }
                                else{
                                    echo "n/a";
                                }
                                ?>

                            </td>
                            <td>
                                <a href="<?php echo $hostedSiteUrl.strtolower(str_replace(" ","-",$item['Item']['name'])); ?>/<?php echo $item['Item']['id']; ?>" class="btn btn-xs btn-info" target="_blank">View</a>

                                <?php
                                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";  

                                echo $this->Html->link('Edit',array('controller'=>'items','action'=>'edit',$item['Item']['id']),array('class'=>'btn btn-xs btn-default'));

                                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";                                

                                echo $this->Html->link('Delete',array('controller'=>'items','action'=>'delete',$item['Item']['id']),array('class'=>'btn btn-xs btn-danger delete-confirm btn-default'));

                                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                                if($item['Item']['featured']==0)
                                {
                                    echo $this->Html->link('Set Featured',array('controller'=>'items','action'=>'set_featured',$item['Item']['id']),array('class'=>'btn btn-xs btn-success'));
                                }
                                else{
                                    echo $this->Html->link('Unset Featured',array('controller'=>'items','action'=>'unset_featured',$item['Item']['id']),array('class'=>'btn btn-xs btn-danger'));
                                }                                
                                ?>

                            </td>

		                </tr>

	            	<?php
					}
	            	?>

	            	</tbody>
	          </table>

	        </div>

      </section>
      <?php
                // pagination section
                echo "<div class='paging'>";
             
                    // the 'first' page button
                    echo $this->Paginator->first("First")."  ";
                     
                    // 'prev' page button, 
                    // we can check using the paginator hasPrev() method if there's a previous page
                    // save with the 'next' page button
                    if($this->Paginator->hasPrev()){
                        echo $this->Paginator->prev("Prev")."  ";
                    }
                     

                    // the 'number' page buttons
                    echo $this->Paginator->numbers(array('modulus' => 50))."  ";
                                       

                    // for the 'next' button
                    if($this->Paginator->hasNext()){
                        echo $this->Paginator->next("Next")."  ";
                    }              
                     
                    // the 'last' page button
                    echo $this->Paginator->last("Last");
                 
                echo "</div>";
               ?>


    </div>
</div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
    <li class="">
        <?php echo $this->Html->link('View Items',array('controller'=>'items','action'=>'index')); ?>
    </li>
    <li class="">
        <?php echo $this->Html->link('Add Item',array('controller'=>'items','action'=>'add')); ?>
    </li>
	<li class="">
        <?php echo $this->Html->link('Add Gallery',array('controller'=>'items','action'=>'add_gallery')); ?>
    </li>
    <li class="active"><a href="#add" data-toggle="tab">City Wise Items (<?php echo $city['City']['name']; ?>)</a></li>       
  </ul> 
</header>
<?php
$this -> end('main-header');
?>

<?php
  $this->start('script');
?>
    <script>
    $( ".ajax-save" ).on("click",function(){
        id = $(this).attr('id');

        $(".ajax-save").attr('disabled','disabled');
        $("#price-"+id).attr('disabled','disabled');
        $("#discount-price-"+id).attr('disabled','disabled');


        ajax_price = $("#price-"+id).val();
        ajax_discount_price = $("#discount-price-"+id).val();
        var url = baseUrl+"item_stocks/save_price_ajax/"+id+"/"+ajax_price+"/"+ajax_discount_price;

        $.ajax({
          type: "GET",
          url: url,
          success : function(data){
                $(".ajax-save").removeAttr('disabled','disabled');
                $("#price-"+id).removeAttr('disabled','disabled');
                $("#discount-price-"+id).removeAttr('disabled','disabled');            
            }
        })

    });
    </script>
<?php
  $this->end('script');
?>