<?php
$this -> start('main-content');
?>
    <div class="tab-content">
        <div class="tab-pane active" id="view">
            <section class="panel">
                <div class="table-responsive">
                    <table class="table table-striped m-b-none" data-ride="datatables">
                        <thead>
                        <tr>

                            <th>Title</th>
                            <th>Alias</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>

                        <tbody>

                        <?php foreach($sms_templates as $sms_template)
                        {
                            ?>

                            <tr>
                                <td><?php echo $sms_template['SmsTemplate']['title']; ?></td>
                                <td><?php echo $sms_template['SmsTemplate']['alias']; ?></td>
                                <td><?php
                                    echo $this->Html->link('Edit',array('controller'=>'sms_templates','action'=>'edit',$sms_template['SmsTemplate']['id']),array('class'=>'btn btn-xs btn-default'));
                                    ?>
                                </td>
                                <td><?php
                                    echo $this->Html->link('Delete',array('controller'=>'sms_templates','action'=>'delete',$sms_template['SmsTemplate']['id']),array('class'=>'btn btn-xs btn-danger btn-default delete-confirm'));
                                    ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>

                        </tbody>
                    </table>
                    <?php ?>
                </div>
            </section>
        </div>

    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#view" data-toggle="tab">Sms Templates</a></li>
            <li class="">
                <?php echo $this->Html->link('Add Sms Template',array('controller'=>'sms_templates','action'=>'add')); ?>
            </li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>