<?php $this->Html->scriptStart(array('inline' => false));  ?>

$("#custom").spectrum({
    color: "#333",
	showInput: true
});
    
<?php $this->Html->scriptEnd(); ?>

<?php
$this -> start('main-content');
?>
    <div class="tab-content">
    <div class="tab-pane active" id="add">
	
        <?php echo $this -> Form -> create('Banner', array('type'=>'file','controller' => 'banners', 'action' => 'add', 'data-validate' => 'parsley', 'role' => 'form')); ?>
		
        <div class="row">
            <div class="col-sm-6">
                <section class="panel">
                    <header class="panel-heading font-bold">1. Banner Details</header>
                    <!-- <header class="panel-heading font-bold">Add Item</header> -->
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Caption</label>
                                <?php
                                echo $this -> Form -> input('caption', array('div' => false,'placeholder'=>'Caption', 'label' => false, 'title' => 'Banner Name', 'class' => 'form-control parsley-validated', 'id' => 'banner-caption'));
                                ?>
                            </div>
							
							<div class="form-group">
                                <label>Caption Color</label><br />
                                <?php
                                echo $this -> Form -> input('caption_color', array('class' => 'form-control m-b parsley-validated', 'label' => false,'id'=>'custom', 'div' => false));
                                ?>
						
                            </div>							
                            <div class="form-group">
                                <label>Description</label>
                                <?php
                                    echo $this -> Form -> input('description', array('type'=>'textarea', 'class' => 'form-control m-b parsley-validated', 'label' => false,'id'=>'banner-description', 'div' => false));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Link</label>
                                <?php
                                echo $this -> Form -> input('link', array('class' => 'form-control m-b parsley-validated', 'label' => false,'id'=>'banner-link', 'div' => false));
                                ?>
                            </div>
							<div class="form-group">
                                <label>Position</label>
                                <?php
                                echo $this -> Form -> input('position', array('options' => $position,'class' => 'form-control m-b parsley-validated', 'label' => false,'id'=>'banner-position', 'div' => false,'data-required' => 'true'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 control-label">Banner (720X390)</label>
                                <div class="col-sm-12">
                                    <?php echo $this -> Form -> input('banner_filename', array('type'=>'file','div' => false, 'label' => false, 'title' => 'Banner', 'id' => 'banner-banner_filename')); ?>
                                </div>
                            </div>
							<div class="form-group">
                                <label>Image Alt</label>
                                <?php
                                    echo $this -> Form -> input('image_alt', array('type'=>'textarea', 'class' => 'form-control m-b parsley-validated', 'label' => false,'id'=>'banner-image-alt', 'div' => false));
                                ?>
                            </div>							
                        </div>

                    </div>
					
                </section>

            </div>
            <div class="col-sm-6">
                <section class="panel">
                    <header class="panel-heading font-bold">2. Choose Cities</header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group col-sm-8">
                                <label class="sr-only" for="exampleInputEmail2">Search City</label>
                                <input type="search" class="form-control " id="item-city-search" placeholder="Search City">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group m-l cities-list col-sm-12">

                                <?php


                                echo $this->Form->input('city_id', array(
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => 'checkbox',
                                    'options' => $cities,
                                    'selected'=>array(),
                                ));
                                ?>

                            </div>
                        </div>


                    </div>
                </section>
            </div>			
        </div>
		
		
		
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body">
                        <?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'item-submit'));  ?>
                    </div>
                </section>
            </div>

        </div>
        <?php echo $this->Form->end(); ?>
    </div>

    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="">
                <?php echo $this->Html->link('View Banners',array('controller'=>'banners','action'=>'index')); ?>
            </li>
            <li class="active"><a href="#add" data-toggle="tab">Add Banner</a></li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>


<?php
  $this->start('script');
?>
<script type="text/javascript">

    $(document).ready(function(){

    });
</script>
<?php
  $this->end('script');
?>