<?php $this->Html->scriptStart(array('inline' => false));  ?>

$("#custom").spectrum({
    color: "#333",
	showInput: true
});
    
<?php $this->Html->scriptEnd(); ?>

<?php
$this -> start('main-content');
?>
    <div class="tab-content">
    <div class="tab-pane active" id="add">
	
        <?php echo $this -> Form -> create('PromotionPhoto', array('type'=>'file','controller' => 'promotion_photos', 'action' => 'add', 'data-validate' => 'parsley', 'role' => 'form')); ?>
		
        <div class="row">
            <div class="col-sm-10">
                <section class="panel">
                    <header class="panel-heading font-bold">Promotion Photo Details</header>
                    <!-- <header class="panel-heading font-bold">Add Item</header> -->
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Name</label>
                                <?php
                                echo $this -> Form -> input('name', array('div' => false,'placeholder'=>'Enter name', 'label' => false, 'title' => 'Name', 'class' => 'form-control parsley-validated', 'id' => 'banner-caption','data-required' => 'true'));
                                ?>
                            </div>
							
												
                            
                            <div class="form-group">
                                <label>Link</label>
                                <?php
                                echo $this -> Form -> input('link', array('class' => 'form-control m-b parsley-validated', 'label' => false,'id'=>'banner-link', 'div' => false));
                                ?>
                            </div>
							
                            <div class="form-group">
                            <label class="col-sm-12 control-label">Image</label>
                                <div class="col-sm-12">
                                    <?php echo $this -> Form -> input('img', array('type'=>'file','div' => false, 'label' => false, 'title' => 'Image', 'id' => 'img')); ?>
                                </div>
                            </div>
										
                        </div>
                        <?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'item-submit'));  ?>
                    </div>
					
                </section>

            </div>
            			
        </div>
		
		
		
      

        </div>
        <?php echo $this->Form->end(); ?>
    </div>

    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="">
                <?php echo $this->Html->link('View Promotion Photos',array('controller'=>'promotion_photos','action'=>'index')); ?>
            </li>
            <li class="active"><a href="#add" data-toggle="tab">Add Promotion Photos</a></li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>


<?php
  $this->start('script');
?>
<script type="text/javascript">

    $(document).ready(function(){

    });
</script>
<?php
  $this->end('script');
?>