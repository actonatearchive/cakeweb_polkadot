
<?php
$this -> start('main-content');
?>
    <div class="tab-content">
    <div class="tab-pane active" id="add">
        <?php echo $this -> Form -> create('Coupon', array('controller' => 'coupons', 'action' => 'add', 'data-validate' => 'parsley', 'role' => 'form')); ?>

        <div class="row">
            <div class="col-sm-6">

                <section class="panel">

                <header class="panel-heading font-bold">Coupon Details</header>

                <div class="panel-body">

                    <div class="form-group">
                        <label>Coupon Code</label>
                        <?php
                        echo $this -> Form -> input('code', array('div' => false, 'label' => false, 'title' => 'Coupon Code', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'coupon-code'));
                        ?>
                    </div>

                    <div class="form-group">
                        <label>Coupon Type</label>
                        <?php
                        echo $this -> Form -> input('type', array('options' => $coupon_type, 'class' => 'form-control m-b parsley-validated','id'=>'coupon-type','data-required' => 'true', 'label' => false,'default'=>0, 'div' => false));
                        ?>
                    </div>

                    <div class="form-group">
                        <label>Percent</label>
                        <?php
                        echo $this -> Form -> input('percent', array('div' => false, 'label' => false, 'title' => 'Percent', 'class' => 'form-control parsley-validated', 'id' => 'coupon-percent'));
                        ?>
                    </div>

                    <div class="form-group">
                        <label>Value</label>
                        <?php
                        echo $this -> Form -> input('value', array('div' => false, 'label' => false, 'title' => 'Value', 'class' => 'form-control parsley-validated', 'id' => 'coupon-value','readonly'=>'readonly'));
                        ?>
                    </div>

                    <div class="form-group">
                        <label>Item Category</label>
                        <?php
                        echo $this -> Form -> input('item_category_id', array('options' => $item_categories,'empty'=>'Choose an item category', 'class' => 'form-control m-b parsley-validated','id'=>'coupon-item-category', 'label' => false, 'div' => false));
                        ?>
                    </div>

                    <div class="form-group">
                        <label>Item</label>
                        <?php
                            echo $this -> Form -> input('item_id', array('options' => $items, 'empty' => 'Choose item', 'class' => 'form-control m-b parsley-validated', 'label' => false,'id'=>'coupon-item', 'div' => false));
                        ?>
                    </div>

                    <div class="form-group">
                        <label>Max Count</label>
                        <?php
                        echo $this -> Form -> input('max_count', array('div' => false, 'label' => false, 'title' => 'Max Count', 'class' => 'form-control parsley-validated', 'id' => 'coupon-max-count'));
                        ?>
                    </div>
					<div class="form-group">
                        <label>Min Value</label>
                        <?php
                        echo $this -> Form -> input('min_value', array('div' => false, 'label' => false, 'title' => 'Min Value', 'class' => 'form-control parsley-validated', 'id' => 'coupon-min-value'));
                        ?>
                    </div>  					
                    <div class="form-group">
                        <label>Max Value</label>
                        <?php
                        echo $this -> Form -> input('max_value', array('div' => false, 'label' => false, 'title' => 'Max Value', 'class' => 'form-control parsley-validated', 'id' => 'coupon-max-value'));
                        ?>
                    </div>                                                                                    

                    <div class="form-group">
                        <label>Expiry Date</label><br />
                        <?php
                            $todays = new DateTime();
                            $the_datetime = $todays->format('d-m-Y H:i:s');
                            echo $this->Form->input('expiry_date',array('div' => false, 'label' => false, 'class' => 'combodate', 'data-required' => 'true', 'data-format'=>'DD-MM-YYYY HH:mm:ss','data-template'=>'DD  MM  YYYY  -  HH : mm : ss','value'=> $the_datetime));
                        ?>
                    </div>    

                    <div class="checkbox">
                        <label>
                            <?php echo $this -> Form -> checkbox('shipping_free', array('hiddenField' => false)); ?> Free Shipping
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <?php echo $this -> Form -> checkbox('active', array('hiddenField' => false)); ?> Set as Active
                        </label>
                    </div>

                </div>

                </section>

            </div>


                        <div class="col-sm-6">
                            <section class="panel">
                                <header class="panel-heading font-bold">Choose Cities</header>
                                <div class="panel-body">
                                    <div class="row">
                                    <div class="form-group col-sm-4">
                                        <label class="sr-only" for="exampleInputEmail2">Search City</label>
                                        <input type="search" class="form-control " id="item-city-search" placeholder="Search City">
                                    </div>
                                    </div>
                                    <div class="row">
                                    <div class="form-group m-l cities-list col-sm-4">

                                    <?php


                                    echo $this->Form->input('city_id', array(
                                        'label' => false,
                                        'type' => 'select',
                                        'multiple' => 'checkbox',
                                        'options' => $cities,
                                        'selected'=>array(),
                                    ));
                                    ?>

                                    </div>
                                    </div>


                                </div>
                            </section>
                        </div>

        </div>

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body">
                        <?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'item-submit'));  ?>
                    </div>
                </section>
            </div>

        </div>
        <?php echo $this->Form->end(); ?>
    </div>
    </div>


<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="">
                <?php echo $this->Html->link('View Coupons',array('controller'=>'coupons','action'=>'index')); ?>
            </li>
            <li class="active"><a href="#add" data-toggle="tab">Add Coupon</a></li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>



<?php $this->Html->scriptStart(array('inline' => false));  ?>

    $('select').select2();

  $("#coupon-type").change(function(){
                if($("#coupon-type").val()==0){
                    $("#coupon-value").attr('readonly','readonly');
                    $("#coupon-percent").removeAttr('readonly');
                }
                else if($("#coupon-type").val()==1){
                    $("#coupon-percent").attr('readonly','readonly');
                    $("#coupon-value").removeAttr('readonly');
                }

    });
<?php $this->Html->scriptEnd(); ?>
