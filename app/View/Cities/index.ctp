<?php
$this -> start('main-content');
$this->Html->scriptStart(array('inline' => false));
?>

$(function() {

    // This is for ajax saving of item price and discount price
    $( ".ajax-save" ).on("click",function(){
        id = $(this).attr('id');
        field = $(this).attr('field');
		

        // Disable ajax buttons and inputs
        $(".ajax-save").attr('disabled','disabled');

        // Fetch values
        ajax_price = $("#sequence-"+id).val();


        ajax_discount_price = $("#discount-price-"+id).val();
        var url = baseUrl+"cities/update/"+id+"/"+field+"/"+ajax_price;

        // Enable ajax buttons and inputs
        $.ajax({
          type: "GET",
          url: url,
          success : function(data){
                $(".ajax-save").removeAttr('disabled','disabled');
            }
        })

    });
});
<?= $this->Html->scriptEnd() ?>

<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
	        <div class="table-responsive">
	          <table class="table table-striped m-b-none" data-ride="datatables">
	            <thead>
	              <tr>
	                <th>Name</th>
	                <th>Code</th>
	                <th>State</th>
	                <th>Zip Prefix</th>
                    <th>Shipping &amp; Handling</th>
					<th>Sequence</th>
                    <th>Disable?</th>
                    <th>Edit</th>
                    <th>Delete</th>
	              </tr>
	            </thead>
	            	
	            	<tbody>
	            	
	            <?php foreach($cities as $item)
	            	{
	            	?>
		            
		            	<tr>
			                <td><?php echo $item['City']['name']; ?></td>
                            <td><?php echo $item['City']['code']; ?></td>
                            <td><?php echo $item['State']['name']; ?></td>
                            <td><?php echo $item['City']['zip_prefix']; ?></td>
                            <td><?php echo $item['City']['shipping_handling_charges']; ?></td>
                            <td>
								<?php 
								echo $this->Form->input('sequence',
								[
									'value'=>$item['City']['sequence'],
									'label'=>false,
									'style'=>'width:30%;float:left',
									'id'=>'sequence-'.$item['City']['id']
								]); ?>
								<?php 
								echo $this->Form->button('save',
								[
									'id'=>$item['City']['id'],
									'field'=>'sequence',
									'class'=>'ajax-save',
									'type'=>'button',
									'style'=>'width:30%'
								]); ?>
								
								
							</td>
							
                            <td>
								<?php
								if($item['City']['disabled']==0){
                                    echo $this->Html->link('Disable',array('controller'=>'cities','action'=>'toggle_disable',$item['City']['id']),array('class'=>'btn btn-xs btn-danger btn-default'));
								} else if ($item['City']['disabled'] == 1) {
									echo $this->Html->link('Enable',array('controller'=>'cities','action'=>'toggle_disable',$item['City']['id']),array('class'=>'btn btn-xs btn-success btn-default'));
								}
                                ?>
                            </td>
							
							<td><?php
                                    echo $this->Html->link('Edit',array('controller'=>'cities','action'=>'edit',$item['City']['id']),array('class'=>'btn btn-xs btn-default'));
                                ?>
                            </td>
                            <td><?php
                                echo $this->Html->link('Delete',array('controller'=>'cities','action'=>'delete',$item['City']['id']),array('class'=>'btn btn-xs btn-danger btn-default delete-confirm'));
                                ?>
                            </td>
                        </tr>
		            
	            	<?php
					}
	            	?>
	            	
	            	</tbody>
	          </table>
	          <?php ?>
	        </div>
      </section>
    </div>>
</div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#view" data-toggle="tab">View Cities</a></li>
        <li class="">
            <?php echo $this->Html->link('Add City',array('controller'=>'cities','action'=>'add')); ?>
        </li>
    </ul>
</header><?php
$this -> end('main-header');
?>