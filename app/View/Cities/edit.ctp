<?php
$this -> start('main-content');
?>
    <div class="tab-content">
    <div class="tab-pane active" id="add">
        <?php echo $this -> Form -> create('City', array('controller' => 'cities', 'action' => 'edit', 'data-validate' => 'parsley', 'role' => 'form')); ?>
        <div class="row">
            <div class="col-sm-6">
                <section class="panel">
                    <header class="panel-heading font-bold">1. City Details</header>
                    <!-- <header class="panel-heading font-bold">Add Item</header> -->
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <?php
                            echo $this -> Form -> input('id', array('type'=>'hidden','div' => false,'default'=>$selectedCity['City']['id'],'placeholder'=>'City ID', 'label' => false, 'title' => 'City Name', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'city-id'));
                            ?>
                            <div class="form-group">
                                <label>Name</label>
                                <?php
                                echo $this -> Form -> input('name', array('div' => false,'default'=>$selectedCity['City']['name'],'placeholder'=>'City Name', 'label' => false, 'title' => 'City Name', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'city-name'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <?php
                                    echo $this -> Form -> input('state_id', array('options' => $states,'default'=>$selectedCity['City']['state_id'],'empty'=>'Choose State', 'class' => 'form-control m-b parsley-validated', 'data-required' => 'true', 'label' => false,'id'=>'city-zip-prefix', 'div' => false));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Zip Prefix (Comma separated)</label>
                                <?php
                                echo $this -> Form -> input('zip_prefix', array('div' => false,'default'=>$selectedCity['City']['zip_prefix'],'placeholder'=>'4000,4006', 'label' => false, 'title' => 'Zip Prefix', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'city-zip-prefix'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Shipping &amp; Handling Charges</label>
                                <?php
                                echo $this -> Form -> input('shipping_handling_charges', array('default'=>$selectedCity['City']['shipping_handling_charges'],'type'=>'number','div' => false,'placeholder'=>'0.00', 'label' => false, 'title' => 'Shipping &amp; Handling Charges', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'city-shipping-handling'));
                                ?>
                            </div>
                            <div class="form-group">
                                <?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Submit', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'item-submit'));  ?>
                            </div>
                        </div>

                    </div>
                </section>

            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="">
                <?php echo $this->Html->link('View Cities',array('controller'=>'cities','action'=>'index')); ?>
            </li>
            <li class="active"><a href="#add" data-toggle="tab">Edit City (<?php echo $selectedCity['City']['name']; ?>)</a></li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>


<?php
  $this->start('script');
?>
<script type="text/javascript">

    $(document).ready(function(){

    });
</script>
<?php
  $this->end('script');
?>