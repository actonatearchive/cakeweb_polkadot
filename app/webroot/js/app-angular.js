

(function () {
	var app = angular.module("cakeweb", ['angucomplete-alt', 'angular-loading-bar', 'ui.bootstrap', 'angular-growl']);
	console.log("AppJS Loaded.");
	app.config(['growlProvider', function (growlProvider) {
		growlProvider.globalTimeToLive({ success: 5000, error: 2000, warning: 3000, info: 4000 });
		growlProvider.onlyUniqueMessages(false);
	}]);
	//,'daterangepicker'
})();
