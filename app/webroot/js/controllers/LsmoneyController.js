(function() {

	var app = angular.module("cakeweb");

    app.controller("AddLSMoney", ['$scope','$http','$timeout', function($scope,$http,$timeout) {
        console.log("AddLSMoney");

        $scope.obj = {
            cust_type: 0,
			user_id: '',
			selectedUser: '',
			order_id: '',
			amount: 0,
			reason: '',
			expires_at: ''
        };
		$scope.new_customer = {
			first_name: '',
			last_name: '',
			email: '',
			// password: ''
		};
		$scope.steps = 0;
		$scope.showAddMoneyDetails = false;

		// $scope.myAtt = "";
        $scope.userUrl = baseUrl + "customOrder/getUsersList/";
		$scope.orderUrl = baseUrl + "userWallets/getOrdersList/";
		$scope.openNewCustomer = function()
		{
			$scope.obj.address = [];
			$scope.obj.fname = '';
			$scope.obj.lname = '';
			$scope.obj.user_email = '';
			angular.element('#customerModal').modal('show');

		};

		$scope.addNewCustomer = function($event)
		{
			console.log("addNewCustomer()");
			var req = {
				 method: 'POST',
				 url: baseUrl + 'customOrder/addNewCustomer',
				 data:
				 {
					user: $scope.new_customer
				 }
			};
			$event.currentTarget.disabled = true;

			$http(req)
			.then(function successCallback(response)
			{
				//response
				if(response.data.status == 'success')
				{
					console.log("Response: ",response.data);
					$scope.showAddMoneyDetails = true;
					$scope.obj.user_id = response.data.id;
					$scope.new_customer = {}; //make all fields empty
					// $scope.obj.address = response.data.data;
					$event.currentTarget.disabled = false;
					angular.element('#customerModal').modal('hide');

				}
				else {
					console.log("Error:",response);
					alert(response.data.message);
					$event.currentTarget.disabled = false;
				}
			},function errorCallback(response) {
			  console.log(response);
			});
			//onElse part user already exists.
		};

		$scope.selectedName = function(name) {
		  console.log("Title: ", name.title); // or description, or image - from your angucomplete attribute configuration
		  console.log("Actual Object: ", name.originalObject);
		  $scope.obj.user_id = name.originalObject.user_id;
		  $scope.showAddMoneyDetails = true;
		//   $item.originalObject // the actual object which was selected
		//   this.$parent // the control which caused the change, contains useful things like $index for use in ng-repeat.

		};

		$scope.selectedOrder = function(order) {
		  $scope.obj.order_id = order.originalObject.order_code;
		};		

		$scope.addLSMoney = function($event)
		{
			console.log("addLSMoney()");
			var req = {
				 method: 'POST',
				 url: baseUrl + 'userWallets/submitAddLSMoney',
				 data:
				 {
					user_id: $scope.obj.user_id,
					order_id: $scope.obj.order_id,
					amount: $scope.obj.amount,
					reason: $scope.obj.reason,
					expires_at: $scope.obj.expires_at
				 }
			};
			$event.currentTarget.disabled = true;

			$http(req)
			.then(function successCallback(response)
			{
				//response
				if(response.data.status == 'success')
				{
					alert("LS Money Added Successfully");
					try {
						window.location.href = baseUrl + 'userWallets/viewUserWallet/' + response.data.user_id;
					}
					catch(err)
					{
						console.log("Error: ",err);
					}
					console.log("Response: ",response.data);

				}
				else {
					console.log("Error:",response);
					alert(response.data.message);
					$event.currentTarget.disabled = false;
				}
			},function errorCallback(response) {
			  console.log(response);
			});
			//onElse part user already exists.
		};
    }]);
}());
