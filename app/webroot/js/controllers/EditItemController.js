(function() {

    var app = angular.module("cakeweb");

    app.controller("EditItemController", ['$scope', '$http', function($scope, $http) {
        console.log("EditItemController");


        //---------------------------------------------------------------------------
        $scope.obj = {
            variants: [],
            idx: 0
        };
        var oidx = parseInt(document.getElementById('child_counts').value) + 1 ;
        $scope.org_idx = oidx;
        console.log("Child_Counts:", oidx);
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

        $scope.addVariant = function()
        {
            $scope.obj.variants.push({id: 'variant_'+oidx});

            console.log("AddVariant Called. - variant_", oidx);
            oidx++;
            setTimeout(function() {
                //Initialize
                $('select').select2();
            },500);

        };

        $scope.removeVariant = function(var_id)
        {
            console.log("IDX: ", var_id);
            $scope.findAndRemove(var_id);

        };

        $scope.findAndRemove = function(var_id)
        {
            try {
                console.log("Obj.VARIANTS: ",$scope.obj.variants);
                var _idx = 0;
                $scope.obj.variants.forEach(function(row) {
                    if (var_id === row.id) {
                        console.log("VARIANT:- ", var_id);
                        console.log("Index: ", _idx);
                        $scope.obj.variants.splice(_idx, 1);
                        throw 'STOP_IT';
                    }
                    _idx ++;
                });

            } catch(err) {
                console.log("IGNORE EXCEPTION: ", err);
            }
        };

        $scope.selectAllCities = function(id)
        {
            $('#'+id).select2('destroy').find('option').prop('selected', 'selected').end().select2();
            console.log("selectAllCities ID: ", id);
        };

        $scope.clearAllCities = function(id)
        {
            $('#'+id).select2('destroy').find('option').prop('selected', false).end().select2();
            console.log("selectAllCities ID: ", id);

        };

        //Initialize
        $('select').select2();
    }]);


})();
//onchange
// function updateURLSlag()
// {
//     var item_name = $('#item-name').val();
//     console.log("Item: ", item_name);
//     item_name = (item_name.replace(' ','-')).toLowerCase();
//     $('#url_slag').val(item_name);
// }
