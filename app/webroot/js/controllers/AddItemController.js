(function() {

    var app = angular.module("cakeweb");

    app.controller("AddItemController", ['$scope', '$http', function($scope, $http) {
        console.log("AddItemController");


        //---------------------------------------------------------------------------
        $scope.obj = {
            variants: [],
            idx: 0
        };
        var oidx = 0 ;
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
        $scope.obj.variants.push({id: 'variant_'+oidx});
        oidx++;

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
        $scope.addVariant = function()
        {
            $scope.obj.variants.push({id: 'variant_'+oidx});

            oidx++;

            setTimeout(function() {
                //Initialize
                $('select').select2();
            },500);

        };

        $scope.removeVariant = function(var_id)
        {
            console.log("IDX: ", var_id);
            $scope.findAndRemove(var_id);

        };

        $scope.findAndRemove = function(var_id)
        {
            try {
                var _idx = 0;
                $scope.obj.variants.forEach(function(row) {
                    if (var_id === row.id) {
                        $scope.obj.variants.splice(_idx, 1);
                        throw 'STOP_IT';
                    }
                    _idx ++;
                });

            } catch(err) {
                console.log("IGNORE EXCEPTION: ", err);
            }
        };

        $scope.selectAllCities = function(id)
        {
            $('#'+id).select2('destroy').find('option').prop('selected', 'selected').end().select2();
            console.log("selectAllCities ID: ", id);
        };

        $scope.clearAllCities = function(id)
        {
            $('#'+id).select2('destroy').find('option').prop('selected', false).end().select2();
            console.log("selectAllCities ID: ", id);

        };

        setTimeout(function() {
            //Initialize
            $('select').select2();
        },300);
    }]);



})();
//onchange
function updateURLSlag()
{
    var item_name = $('#item-name').val();
    console.log("Item: ", item_name);
    var tmp = item_name.split(" ").join("-");
    // var tmp = item_name.replace(/\s/g,"-");
    item_name = (tmp).toLowerCase();
    $('#url_slag').val(item_name);
}


$(document).on("keypress",".select2-input",function(event){
    if (event.ctrlKey || event.metaKey) {
        var id =$(this).parents("div[class*='select2-container']").attr("id").replace("s2id_","");
        var element =$("#"+id);
        if (event.which == 97){
            var selected = [];
            element.find("option").each(function(i,e){
                selected[selected.length]=$(e).attr("value");
            });
            element.select2("val", selected);
        } else if (event.which == 100){
            element.select2("val", "");
        }
    }
});
