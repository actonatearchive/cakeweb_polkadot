(function() {

	var app = angular.module("cakeweb");

    app.controller("AddCustomOrder", ['$scope','$http','$timeout', function($scope,$http,$timeout) {
        console.log("AddCustomOrder");

        $scope.obj = {
            cust_type: 0,
            name: '',
			fname: '',
			lname: '',
			address: [],
			user_id: '',
			user_email: '',
			selectedUser: '',
			usrObj: '',
			selectedAddress: [],
			item_name: '',
			selectedItems: [],
			add_charge: 0,
			ship_charge: 0,
			discount: 0,
			courier_id: '',
			total: 0,
			grand_total: 0,
            cod_available: false
        };
		$scope.new_customer = {
			first_name: '',
			last_name: '',
			email: '',
			// password: ''
		};
		$scope.steps = 0;
		$scope.showAddresses = false;
		$scope.showItems = false;
		$scope.items = [];
		var tmp_idx = 0;
		$scope.new_address = {
			name: '',
			line1: '',
			line2: '',
			landmark: '',
			pin_code: '',
			city_name: '',
			state_name: '',
			mobile: '',
			is_active: 1
		};

		$scope.edit_address = {
			id: '',
			name: '',
			line1: '',
			line2: '',
			landmark: '',
			pin_code: '',
			city_name: '',
			state_name: '',
			mobile: '',
			is_active: 1
		};

		// $scope.myAtt = "";
        $scope.userUrl = baseUrl + "customOrder/getUsersList/";
		$scope.openNewCustomer = function()
		{
			$scope.obj.address = [];
			$scope.obj.fname = '';
			$scope.obj.lname = '';
			$scope.obj.user_email = '';
			angular.element('#customerModal').modal('show');

		};

		$scope.addNewCustomer = function($event)
		{
			console.log("addNewCustomer()");
			var req = {
				 method: 'POST',
				 url: baseUrl + 'customOrder/addNewCustomer',
				 data:
				 {
					user: $scope.new_customer
				 }
			};
			$event.currentTarget.disabled = true;

			$http(req)
			.then(function successCallback(response)
			{
				//response
				if(response.data.status == 'success')
				{
					console.log("Response: ",response.data);
					$scope.showAddresses = true;
					$scope.obj.user_id = response.data.id;
					$scope.obj.fname = response.data.data.User.first_name;
					$scope.obj.lname = response.data.data.User.last_name;
					$scope.obj.user_email = response.data.data.User.username;
					alert($scope.obj.fname + " "+ $scope.obj.lname + " succesfully registered!");
					$scope.obj.usrObj = response.data.data.User;
					// $scope.obj.address.push(response.data.data);
					$scope.new_customer = {}; //make all fields empty
					// $scope.obj.address = response.data.data;
					$event.currentTarget.disabled = false;
					angular.element('#customerModal').modal('hide');

				}
				else {
					console.log("Error:",response);
					alert(response.data.message);
					$event.currentTarget.disabled = false;
				}
			},function errorCallback(response) {
			  console.log(response);
			});
			//onElse part user already exists.
		};

		$scope.selectedName = function(name) {
		  console.log("Title: ", name.title); // or description, or image - from your angucomplete attribute configuration
		  console.log("Actual Object: ", name.originalObject);
		  $scope.obj.user_id = name.originalObject.user_id;
		  $scope.obj.selectedUser = name.originalObject;
		  $scope.obj.user_email = name.originalObject.email;
		  $scope.obj.fname = name.originalObject.first_name;
		  $scope.obj.lname = name.originalObject.last_name;

		  $scope.showAddresses = true;
		//   $item.originalObject // the actual object which was selected
		//   this.$parent // the control which caused the change, contains useful things like $index for use in ng-repeat.

			var req = {
				 method: 'POST',
				 url: baseUrl + 'customOrder/getUserAddresses',
				 data: { id: name.originalObject.user_id }
		 	};
			$scope.obj.address = [];
			$http(req)
			.then(function successCallback(response)
			{
				//response
				if(response.data.status == 'success')
				{
					console.log("Address Response: ",response.data);
					if(response.data.data.length > 0)
					{
						$scope.obj.usrObj = response.data.data[0].User;
						console.log("Selected User: ", $scope.obj.usrObj);
					}
					$scope.obj.address = response.data.data;
				}
				else {
					console.log("Error:",response);
				}
			},function errorCallback(response) {
			  console.log(response);
			});

		};
		$scope.selectAddress = function(address,idx)
		{
			$scope.obj.selectedAddress = address;

            $scope.checkPincode(address.pin_code);
			console.log("Index: ", idx);
			var ele;
			for(var i = 0 ; i < $scope.obj.address.length; i ++)
			{
				if(idx == i)
				{
					// console.log("HEY I AM EXECUTED!");
					ele = document.getElementById("add_"+idx);
					ele.style.background = "orange";
				}
				else {
					// console.log("NOPE NOPE");
					ele = document.getElementById("add_"+i);
					ele.style.background = "#f8f8f8";
				}
			}
			$scope.showItems = true;
			console.log("SelectedAddress: ", $scope.obj.selectedAddress);
		};

        $scope.checkPincode = function(_pincode)
        {
            var req = {
				 method: 'POST',
				 url: baseUrl + 'customOrder/pincodeCheck',
				 data:
				 {
					pincode: _pincode
				 }
			};

            $http(req)
			.then(function successCallback(response)
			{
				//response
				if(response.data.status == 'success')
				{
					console.log("Response: ",response.data);
                    $scope.obj.cod_available = true;
				}
				else {
					console.log("Error:",response);
                    $scope.obj.cod_available = false;
				}
			},function errorCallback(response) {
			  console.log(response);
			});

        };

		$scope.addNewAddress = function($event)
		{
			console.log("addNewAddress()");
			var req = {
				 method: 'POST',
				 url: baseUrl + 'customOrder/addNewAddress',
				 data:
				 {
					id: $scope.obj.user_id,
					address: $scope.new_address
				 }
			};
			$event.currentTarget.disabled = true;

			$http(req)
			.then(function successCallback(response)
			{
				//response
				if(response.data.status == 'success')
				{
					console.log("Response: ",response.data);
					$scope.obj.address.push(response.data.data);
					$scope.new_address = {};
					// $scope.obj.address = response.data.data;
					$event.currentTarget.disabled = false;
					angular.element('#addressModal').modal('hide');

				}
				else {
					console.log("Error:",response);
					$event.currentTarget.disabled = false;
				}
			},function errorCallback(response) {
			  console.log(response);
			});


			//To Hide the Modal
		};

//---------------------------------
	$scope.checkPrice = function(itm_price,idx)
	{
		if(itm_price < 0)
		{
			$scope.obj.selectedItems[idx].itm_price = 0;
			alert("Sorry! Price must be in positive.");
		}
	};
		//-----------------------

		$scope.getDetailsForEditAddress = function(usr,idx)
		{
			console.log("User Details: ",usr);

			$scope.edit_address = usr.UserAddress;
			tmp_idx = idx;
			angular.element('#editAddressModal').modal('show');

		};

		$scope.editAddress = function()
		{
			var req = {
				 method: 'POST',
				 url: baseUrl + 'customOrder/editAddress',
				 data:
				 {
					address: $scope.edit_address
				 }
			};

			console.log("EditAddressObj: ", $scope.edit_address);
			$http(req)
			.then(function successCallback(response)
			{
				//response
				if(response.data.status == 'success')
				{
					console.log("Response: ",response.data.data);
					$scope.obj.address[tmp_idx] = response.data.data;
					// $scope.obj.address = response.data.data;
					angular.element('#editAddressModal').modal('hide');

				}
				else {
					console.log("Error:",response);
				}
			},function errorCallback(response) {
			  console.log(response);
			});

		};

		$scope.getItems = function()
		{
			if($scope.obj.item_name.length <= 3)
			{
				console.log("NOPE");
				return false;
			}


			console.log("TIMEOUT");
			var req = {
				method: 'POST',
				url: baseUrl + 'customOrder/getProductItems',
				data:{
					item_name: $scope.obj.item_name
				}
			};

			$http(req)
			.then(function successCallback(response)
			{
				//response
				if(response.data.status == 'success')
				{
					console.log("Response: ",response.data);
					$scope.items = response.data.data;
				}
				else {
					console.log("Error:",response);
				}
			},function errorCallback(response) {
			  console.log(response);
			});


		};

		//-----------------------------------
		$scope.addItemsToSelected = function(itm,idx)
		{
			var photo_url = "https://ls-cdn.letsshave.com/products/item/primary_photo/" + itm.Item.primary_photo_directory + "/" + "small_" + itm.Item.primary_photo;

			$scope.obj.selectedItems.push({itm_id: itm.Item.id,variant_name: itm.Item.variant_name,itm_photo: photo_url, itm_name: itm.Item.name, itm_price: itm.Item.discount_price, itm_qty: 1, one_line3: itm.Item.one_line3});
			console.log("Item: ", itm);
			$scope.items.splice(idx, 1);
		};

		$scope.nextBtn = function()
		{
			//Increase Steps
			$scope.steps += 1;
			console.log("Steps: ", $scope.steps);
			window.scrollTo(0, 0);
			if($scope.steps == 1)
			{
				$scope.getItemsTotal();
			}
		};

		$scope.backBtn = function()
		{
			$scope.steps -= 1;
			console.log("Steps: ", $scope.steps);
			window.scrollTo(0, 0);
		};

		$scope.removeSelectedItem = function(idx)
		{
			$scope.obj.selectedItems.splice(idx,1);
		};
		//-----------------------------------
		$scope.getItemsTotal = function()
		{
			$scope.obj.total = 0;
			$scope.obj.selectedItems.forEach(function(row) {
				// console.log("Row: ", row.itm_price * row.itm_qty); //
				$scope.obj.total +=  parseFloat(row.itm_price) * parseFloat(row.itm_qty) ;
			});

			$scope.obj.grand_total = $scope.obj.total;
		};

		$scope.addCharges = function()
		{
			if($scope.obj.ship_charge < 0)
			{
				$scope.obj.ship_charge = 0;

				alert("Sorry! Price must be in positive.");
			}
			else if($scope.obj.add_charge < 0)
			{
				$scope.obj.add_charge = 0;
				alert("Sorry! Price must be in positive.");
			}

			$scope.obj.grand_total = parseFloat($scope.obj.total) + parseFloat($scope.obj.ship_charge) + parseFloat($scope.obj.add_charge)-parseFloat($scope.obj.discount);
		};

		$scope.addDiscounts = function()
		{
			if($scope.obj.discount < 0)
			{
				$scope.obj.discount = 0;alert("Sorry! Price must be in positive.");
			}
			$scope.obj.grand_total = parseFloat($scope.obj.total) + parseFloat($scope.obj.ship_charge) + parseFloat($scope.obj.add_charge)-parseFloat($scope.obj.discount);

		};
		//----------------------------------------------------------------------
		//	Place COD Order
		//----------------------------------------------------------------------
		$scope.placeCODOrder = function($event,mode)
		{
			var _order_type = null;
			if (mode == 'cod') {
				mode = 'COD';
			} else if(mode == 'prepaid'){
				mode = 'PREPAID';
			} else if(mode === 'foc'){
				mode = 'FOC';
				_order_type= 'FOC';
			}


			var req = {
				method: 'POST',
				url: baseUrl + 'customOrder/placeCODOrder',
				data:{
					items: $scope.obj.selectedItems,
					ship_charge: $scope.obj.ship_charge,
					add_charge: $scope.obj.add_charge,
					discount: $scope.obj.discount,
					address_id: $scope.obj.selectedAddress.id,
					payment_mode: mode,
					payment_through: mode,
					user_id: $scope.obj.user_id,
					order_type: _order_type,
					courier_id: $scope.obj.courier_id
				}
			};
			$event.currentTarget.disabled = true;

			// console.log("RequestCOD:" ,req);
			$http(req)
			.then(function successCallback(response)
			{
				$event.currentTarget.disabled = false;
				//response
				if(response.data.status == 'success')
				{
					alert("Order Placed Successfully");
					try {
						// window.scrollTo(0, 0);
						window.location.href = baseUrl + 'customOrder/cod_success/' + response.data.order_id + '/' + response.data.data.Order.code;

					}
					catch(err)
					{
						console.log("Error: ",err);
					}
					console.log("Response: ",response.data);
				}
				else {
					console.log("Error:",response);
				}
			},function errorCallback(response) {
				$event.currentTarget.disabled = false;
			  	console.log(response);
			});

		};

		//----------------------------------------------------------------------
		//Payment Online
		//----------------------------------------------------------------------
		$scope.placeOnlinePaymentOrder = function($event)
		{
			var req = {
				method: 'POST',
				url: baseUrl + 'customOrder/placeOnlinePaymentOrder',
				data:{
					items: $scope.obj.selectedItems,
					ship_charge: $scope.obj.ship_charge,
					add_charge: $scope.obj.add_charge,
					discount: $scope.obj.discount,
					address_id: $scope.obj.selectedAddress.id,
					payment_mode: 1,
					user_id: $scope.obj.user_id,
					courier_id: $scope.obj.courier_id
				}
			};
			$event.currentTarget.disabled = true;

			console.log("RequestPAYOnline:" ,req);
			$http(req)
			.then(function successCallback(response)
			{
				$event.currentTarget.disabled = false;
				//response
				if(response.data.status == 'success')
				{
					alert("Order Placed Successfully");
					try {
						// window.scrollTo(0, 0);
						window.location.href = baseUrl + 'customOrder/online_success/' + response.data.order_id + '/' + response.data.data.Order.code;
					}
					catch(err)
					{
						console.log("Error: ",err);
					}
					console.log("Response: ",response.data);
				}
				else {
					alert("Oops! Something went Wrong! Please try again later.");
					console.log("Error:",response);
				}
			},function errorCallback(response) {
				$event.currentTarget.disabled = false;
				console.log(response);
			});

		};


    }]);
}());
