<?php
/**
 * Created by IntelliJ IDEA.
 * User: Shoaib Merchant
 * Date: 10/4/13
 * Time: 2:37 AM
 * To change this template use File | Settings | File Templates.
 */

class ItemCategoriesController extends AppController {

	public $name = 'ItemCategories';

	public $uses = array('ItemCategory', 'ItemType','City','ItemCategoryCity');

	public function index() {
		$itemCats = $this -> ItemCategory -> find('all');
		$this -> set('item_categories', $itemCats);


		$this -> set('page_title', 'Item Categories');
		$this -> layout = 'polka_shell';

	}

	public function add() {
		if ($this -> request -> is('post')) {
			$itemCategory = $this -> request -> data;

            if (isset($itemCategory['ItemCategory']['is_festival'])) {
                $itemCategory['ItemCategory']['is_festival'] = 1; //checkbox checked
            } else {
                $itemCategory['ItemCategory']['is_festival'] = 0; //not checked
            }

            if($itemCategory['ItemCategory']['item_category_id'] == ""){
                $itemCategory['ItemCategory']['item_category_id'] = null;
            }
            if($itemCategory['ItemCategory']['item_type_id'] == ""){
                $itemCategory['ItemCategory']['item_type_id'] = null;
            }
            $itemCategory['ItemCategory']['url_slag'] = str_replace(' ','-',$itemCategory['ItemCategory']['name']);

            if ($this -> ItemCategory -> save($itemCategory)) {
				//This flash message has to be set in the view properly

                $last_id = $this->ItemCategory->getInsertID();

                //Set Cities
                $itemCatCities = array();
                foreach($itemCategory['ItemCategory']['city_id'] as $key=>$value){
                    $newItemCategoryCity = array();
                    $newItemCategoryCity['ItemCategoryCity']['city_id'] = $value;
                    $newItemCategoryCity['ItemCategoryCity']['item_category_id'] = $last_id;

                    array_push($itemCatCities, $newItemCategoryCity);
                }

                if(sizeof($itemCatCities)>0)
                {
                    $this->ItemCategoryCity->saveAll($itemCatCities);
                }

				$this -> Session -> setFlash("New item category added.");
				$this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
			} else {
				//This flash message has to be set in the view properly
				$this -> Session -> setFlash("Sorry. an error occurred.");
				$this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
			}
		}
        else {

            $itemTypes = $this -> ItemType -> find('list', array('order' => 'ItemType.name ASC'));
            $this -> set('item_types', $itemTypes);

            $itemCatParents = $this -> ItemCategory -> find('list', array('order' => 'ItemCategory.name ASC'));
            $this -> set('item_category_parents', $itemCatParents);

            //Get All Cities
            $cities = $this->City->find('list',array('order'=>array('City.name')));
            $this->set('cities',$cities);

            $this -> set('page_title', 'Add Item Category');
            $this -> layout = 'polka_shell';
		}
	}



    public function delete($id=null) {

        if($id == null){
            $this->Session->setFlash('Please choose a category.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
        }

        $selectedItemCategory = $this->ItemCategory->findById($id);

        if($selectedItemCategory == null){
            $this->Session->setFlash('Please choose a category.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
        }

        if($this->ItemCategory->delete($selectedItemCategory['ItemCategory']['id'])){
            $this->Session->setFlash('Category deleted.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
        }

    }

    public function edit($id=null) {

        if ($this -> request -> is('post')) {
            $itemCategory = $this -> request -> data;

            if (isset($itemCategory['ItemCategory']['is_festival'])) {
                $itemCategory['ItemCategory']['is_festival'] = 1; //checkbox checked
            } else {
                $itemCategory['ItemCategory']['is_festival'] = 0; //not checked
            }

            if($itemCategory['ItemCategory']['item_category_id'] == ""){
                $itemCategory['ItemCategory']['item_category_id'] = null;
            }
            if($itemCategory['ItemCategory']['item_type_id'] == ""){
                $itemCategory['ItemCategory']['item_type_id'] = null;
            }
            $itemCategory['ItemCategory']['url_slag'] = str_replace(' ','-',$itemCategory['ItemCategory']['name']);
            
            if ($this -> ItemCategory -> save($itemCategory)) {

                //Remove Existing Cities
                $this->ItemCategoryCity->deleteAll(array('ItemCategoryCity.item_category_id'=>$itemCategory['ItemCategory']['id']));

                //Set Cities
                $itemCatCities = array();
                foreach($itemCategory['ItemCategory']['city_id'] as $key=>$value){
                    $newItemCategoryCity = array();
                    $newItemCategoryCity['ItemCategoryCity']['city_id'] = $value;
                    $newItemCategoryCity['ItemCategoryCity']['item_category_id'] = $itemCategory['ItemCategory']['id'];

                    array_push($itemCatCities, $newItemCategoryCity);
                }

                if(sizeof($itemCatCities)>0)
                {
                    $this->ItemCategoryCity->saveAll($itemCatCities);
                }

                //This flash message has to be set in the view properly
                $this->Session->setFlash('Selected category edited.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
            } else {
                //This flash message has to be set in the view properly
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
            }
        }

        else{

            if($id == null){
                $this->Session->setFlash('Please choose a category.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
            }

            $selectedCat = $this->ItemCategory->findById($id);

            if($selectedCat == null){
                $this->Session->setFlash('Please choose a category.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
            }

            //Get Item Category Cities
            $itemCatCities = $this->ItemCategoryCity->find('all',array('conditions'=>array('ItemCategoryCity.item_category_id'=>$selectedCat['ItemCategory']['id'])));

            $itemCatCitiesList = array();
            foreach($itemCatCities as $itemCatCity){
                array_push($itemCatCitiesList, $itemCatCity['ItemCategoryCity']['city_id']);
            }

            $this->set('selectedCitiesList',$itemCatCitiesList);
            //pr($itemCatCities);die();

            $itemTypes = $this -> ItemType -> find('list', array('order' => 'ItemType.name ASC'));
            $this -> set('item_types', $itemTypes);

            $itemCatParents = $this -> ItemCategory -> find('list', array('order' => 'ItemCategory.name ASC','conditions'=>array('not'=>array('ItemCategory.id'=>$selectedCat['ItemCategory']['id']))));
            $this -> set('item_category_parents', $itemCatParents);


            //Get All Cities
            $cities = $this->City->find('list',array('order'=>array('City.name')));
            $this->set('cities',$cities);


            $this->set('selectedCat',$selectedCat);
            $this -> set('page_title', 'Edit Category');
            $this -> layout = 'polka_shell';

        }
    }

    public function disable($id=null) {
        if($id == null){
            $this->Session->setFlash('Please choose an item category.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
        }

        $selectedItemCat = $this->ItemCategory->findById($id);

        if($selectedItemCat == null){
            $this->Session->setFlash('Please choose an item category.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
        }


        $selectedItemCat['ItemCategory']['disabled']=1;

        if($this->ItemCategory->save($selectedItemCat)){
            $this->Session->setFlash('Item category disabled.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
        }

    }

    public function enable($id=null) {
        if($id == null){
            $this->Session->setFlash('Please choose an item category.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
        }

        $selectedItemCat = $this->ItemCategory->findById($id);

        if($selectedItemCat == null){
            $this->Session->setFlash('Please choose an item category.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
        }


        $selectedItemCat['ItemCategory']['disabled']=0;

        if($this->ItemCategory->save($selectedItemCat)){
            $this->Session->setFlash('Item category enabled.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'item_categories', 'action' => 'index'));
        }

    }




}
