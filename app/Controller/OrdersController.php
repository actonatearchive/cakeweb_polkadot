<?php
/**
 * Created by IntelliJ IDEA.
 * User: Shoaib Merchant
 * Date: 10/4/13
 * Time: 2:37 AM
 * To change this template use File | Settings | File Templates.
 */

class OrdersController extends AppController {

	public $name = 'Orders';

	public $uses = array('Order', 'OrderItem','User', 'Courier', 'OrderDispatch','City','State','Coupon','Referral','Item','UserAddresses','index_old');

    var $helpers = array('Html', 'Form','Csv');

    public function export() {

        $threshold =  new DateTime("2014-09-11 17:00:00");
        $threshold = $threshold->format('Y-m-d');

        $this->OrderItem->Behaviors->load('Containable');

        $completed_order = $this->OrderItem->find('all',array('contain' => array('Order','Order.User','Item','Order.ShippingAddress'),'conditions' => array('OR'=>array('AND'=>array('Order.status' => 2,'Order.created <'=>$threshold),'Order.status' => 3)),'group' => 'Order.id','order' => array('Order.created' => 'desc')));

        //pr($completed_order);die();
        $order_arr = array();
        foreach ($completed_order as $key => $order) {

            $temp = array();
            $temp['Order']['Date'] = date('d-m-Y',strtotime($order['Order']['created']));
            $temp['Order']['Code'] = $order['Order']['code'];
            $temp['Order']['Username'] = $order['Order']['ShippingAddress']['name'];
            $temp['Order']['Email'] = $order['Order']['User']['username'];
            $temp['Order']['Mobile'] = $order['Order']['ShippingAddress']['mobile'];
            $temp['Order']['Addressline1'] = $order['Order']['ShippingAddress']['line1'];
            $temp['Order']['Addressline2'] = $order['Order']['ShippingAddress']['line2'];
            $temp['Order']['Landmark'] = $order['Order']['ShippingAddress']['landmark'];
            $temp['Order']['pin_code'] = $order['Order']['ShippingAddress']['pin_code'];


            $temp['Order']['OrderItem'] = $order['Item']['name'];
            $temp['Order']['OrderItemQty'] = $order['OrderItem']['quantity'];
            $temp['Order']['Sku'] = $order['Item']['sku_code'];
            $temp['Order']['Variant'] = $order['Item']['variant_name'];

            $temp['Order']['amount'] = $order['Order']['total_amount'];
            $temp['Order']['OtherCharges'] = $order['Order']['additional_charges'];

            array_push($order_arr, $temp);


        }




     $this->set('orders', $order_arr);

     $this->layout = null;
     $this->autoLayout = false;
     Configure::write('debug','0');

    }

	public function index_old() {

		$this->OrderItem->Behaviors->load('Containable');

		$orders = $this -> OrderItem -> find('all',array(
		'conditions' => array('OrderItem.order_dispatch_id'=>null,'Order.status' => 1),
		'group' => 'Order.id',
		'contain' => array('Order','Order.User','Order.Referral')
		));

		$final_orders = array();
		foreach($orders as $order){

			$the_item = $this->Item->findById($order['OrderItem']['item_id']);

			if($the_item['Item']['variant_name'] != "" && $the_item['Item']['variant_name'] != null){
				$order['OrderItem']['item_name'] = $the_item['Item']['name']." (".$the_item['Item']['variant_name'].")";
			}
			else{
				$order['OrderItem']['item_name'] = $the_item['Item']['name'];
			}


			$the_shipping_address = $this->UserAddresses->findById($order['Order']['shipping_address_id']);

			$the_city = $this->City->findById($the_shipping_address['UserAddresses']['city_id']);

			$order['OrderItem']['city_name'] = $the_city['City']['name'];

			array_push($final_orders, $order);
		}


		$this -> set('orders', $final_orders);

		$this -> set('page_title', 'View Orders');
		$this -> layout = 'polka_shell';
	}
    public function index() {

		//$this->OrderItem->Behaviors->load('Containable');

		$orders = $this -> Order -> find('all',array(
				'conditions' => array('Order.status' => 1)
			)
		);

		$final_orders = array();

		foreach($orders as $order){

			$this->OrderItem->create();
			$ord_item = $this->OrderItem->find('all',array('conditions'=>array('OrderItem.order_id'=>$order['Order']['id'],'OrderItem.order_dispatch_id'=>null)));

			if($order['ShippingAddress']['city_id'] != null)
			{
				$the_city = $this->City->findById($order['ShippingAddress']['city_id']);
				$order['ShippingAddress']['city_name'] = $the_city['City']['name'];
			}

			$order['OrderItem'] = $ord_item;
			array_push($final_orders, $order);
		}
		// pr($final_orders);die();
		$couriers = $this->Courier->find('list');

		$this -> set('couriers', $couriers);
		$this -> set('orders', $final_orders);

		$this -> set('page_title', 'View Orders');
		$this -> layout = 'polka_shell';
	}
	public function pending_mark_delivered() {

		$this->OrderItem->Behaviors->load('Containable');

		
		$orders = $this -> Order -> find('all',array(
		'conditions' => array('Order.status' => 2),
		'group' => 'Order.id')
		);

		$final_orders = array();

		foreach($orders as $order){

			$this->OrderItem->create();
			$ord_item = $this->OrderItem->find('all',
			array('conditions'=>array('OrderItem.order_id'=>$order['Order']['id'],
			'OrderItem.order_dispatch_id'=>null)));

			if($order['ShippingAddress']['city_id'] != null)
			{
				$the_city = $this->City->findById($order['ShippingAddress']['city_id']);
				$order['ShippingAddress']['city_name'] = $the_city['City']['name'];
			}

			$order['OrderItem'] = $ord_item;
			array_push($final_orders, $order);
		}

		$this -> set('orders', $orders);

		// pr($orders);die();
		$this -> set('page_title', 'View Orders');
		$this -> layout = 'polka_shell';
	}

	public function payment_incomplete() {

		$this->OrderItem->Behaviors->load('Containable');

/*		$orders = $this -> OrderItem -> find('all',array(
		'conditions' => array('Order.status' => 0,array('OrderItem.order_dispatch_id'=>null),array('NOT'=>array('Order.user_id'=>null))),
		'contain' => array('Order','Order.User')
		));*/

        // we prepare our query, the cakephp way!
        $this->paginate = array(
			'conditions' => array('Order.status' => 0,array('OrderItem.order_dispatch_id'=>null),array('NOT'=>array('Order.user_id'=>null))),
			'contain' => array('Order','Order.User'),
            'limit' => 50,
            'group' => 'Order.id',
            'order' => array('created' => 'desc')
        );

        // we are using the 'User' model
        $orders = $this->paginate('OrderItem');

		$this -> set('orders', $orders);

		//pr($orders);

		$this -> set('page_title', 'Payment Incomplete');
		$this -> layout = 'polka_shell';
	}


	public function completed() {


		$this->Order->Behaviors->load('Containable');

/*		$orders = $this -> OrderItem -> find('all',array(
		'conditions' => array('Order.status' => 2),
		'contain' => array('Order','Order.User')
		));*/

        // we prepare our query, the cakephp way!
        $this->paginate = array(
			'conditions' => array('OR'=>array('AND'=>array('Order.status' => 2,'Order.status' => 3))),
			'contain' => array('User'),
            'limit' => 50,
            'group' => 'Order.id',
            'order' => array('created' => 'desc')
        );

        // we are using the 'User' model
        $orders = $this->paginate('Order');

		$this -> set('orders', $orders);

		//print_r($orders);die();

		$this -> set('page_title', 'View Orders');
		$this -> layout = 'polka_shell';
	}

	public function print_invoice($id) {

		if ($this -> request -> is('post')) {

		} else {

			$order = $this -> Order -> findById($id);
			$this -> set('order', $order);

			//pr($order);

			//$shipping = $this -> City -> findById($order['ShippingAddress']['city_id']);
			//$this -> set('shipping', $shipping);

			//$billing = $this -> City -> findById($order['BillingAddress']['city_id']);
			//$this -> set('billing', $billing);

			//$coupon = $this -> Coupon -> findById($order['Order']['coupon_id']);
			//$this -> set('coupon', $coupon);

			//pr($order);

			//$order_items = $this -> OrderItem -> find('all', array('conditions' => array('OrderItem.order_id' => $id), 'fields' => array('OrderItem.id', 'Item.name','OrderItem.quantity')));
			//$order_items_list = array();

			$order_items = $this -> OrderItem -> find('all', array('conditions' => array('OrderItem.order_id' => $id), 'fields' => array('OrderItem.id', 'Item.name', 'Item.id','OrderItem.quantity','OrderItem.price')));
			$order_items_list = array();

			//pr($order_items);

			foreach ($order_items as $oitem) {
                $order_items_list[$oitem['OrderItem']['id']] = array();
                if (isset($oitem['Item']['variant_name'])) {
					$order_items_list[$oitem['OrderItem']['id']]['name'] = $oitem['Item']['name'] . " (" . $oitem['Item']['variant_name'] . ") x ". $oitem['OrderItem']['quantity'];
				} else {
					$order_items_list[$oitem['OrderItem']['id']]['name'] = $oitem['Item']['name']. " x ". $oitem['OrderItem']['quantity'];
				}

                //if($oitem['OrderItem']['item_params'] != ""){
                //    $order_items_list[$oitem['OrderItem']['id']]['item_params'] = json_decode($oitem['OrderItem']['item_params'],true);
                //}
                //else{
                //    $order_items_list[$oitem['OrderItem']['id']]['item_params'] = "";
                //}

                if($oitem['OrderItem']['price'] != ""){
                    $order_items_list[$oitem['OrderItem']['id']]['price'] = $oitem['OrderItem']['price'];
                }
                else{
                    $order_items_list[$oitem['OrderItem']['id']]['price'] = "";
                }
				$order_items_list[$oitem['OrderItem']['id']]['item_id'] = $oitem['Item']['id'];
			}

			$this -> set('order_items', $order_items_list);

			$couriers = $this -> Courier -> find('list');
			$this -> set('couriers', $couriers);



			//pr($order_items_list);die();

			$page_title = "Invoice for Order Code : ".$order['Order']['code'];

			$this -> set('page_title', $page_title);
			$this -> layout = 'invoice_layout_blank';

		}
	}

	public function vieworder($id) {

		if ($this -> request -> is('post')) {

		} else {

			$order = $this -> Order -> findById($id);
			$this -> set('order', $order);

            // pr($order);
			$shipping = $this -> City -> findById($order['ShippingAddress']['city_id']);
			$this -> set('shipping', $shipping);

			$billing = $this -> City -> findById($order['BillingAddress']['city_id']);
			$this -> set('billing', $billing);

			$coupon = $this -> Coupon -> findById($order['Order']['coupon_id']);
			$this -> set('coupon', $coupon);

			//pr($order);

			//$order_items = $this -> OrderItem -> find('all', array('conditions' => array('OrderItem.order_id' => $id), 'fields' => array('OrderItem.id', 'Item.name','OrderItem.quantity')));
			//$order_items_list = array();

			$order_items = $this -> OrderItem -> find('all', array('conditions' => array('OrderItem.order_id' => $id), 'fields' => array('OrderItem.id', 'Item.name', 'Item.variant_name', 'Item.id','OrderItem.quantity','OrderItem.item_params')));
			$order_items_list = array();

			//pr($order_items);

			foreach ($order_items as $oitem) {
                $order_items_list[$oitem['OrderItem']['id']] = array();
                if (isset($oitem['Item']['variant_name'])) {
					$order_items_list[$oitem['OrderItem']['id']]['name'] = $oitem['Item']['name'] . " (" . $oitem['Item']['variant_name'] . ") x ". $oitem['OrderItem']['quantity'];
				} else {
					$order_items_list[$oitem['OrderItem']['id']]['name'] = $oitem['Item']['name']. " x ". $oitem['OrderItem']['quantity'];
				}

                if($oitem['OrderItem']['item_params'] != ""){
                    $order_items_list[$oitem['OrderItem']['id']]['item_params'] = json_decode($oitem['OrderItem']['item_params'],true);
                }
                else{
                    $order_items_list[$oitem['OrderItem']['id']]['item_params'] = "";
                }
				$order_items_list[$oitem['OrderItem']['id']]['item_id'] = $oitem['Item']['id'];
				$order_items_list[$oitem['OrderItem']['id']]['variant_name'] = $oitem['Item']['variant_name'];
			}

			$this -> set('order_items', $order_items_list);

			$couriers = $this -> Courier -> find('list');
			$this -> set('couriers', $couriers);



			//pr($order_items_list);die();

			$this -> set('page_title', 'View Orders');
			$this -> layout = 'polka_shell';

		}
	}

	public function todays() {

		$today = new DateTime();
		$tdate = $today->format('Y-m-d');

		$orders = $this -> Order -> find('all',array('conditions' => array('DATE(Order.created)' => $tdate)));
		$this -> set('orders', $orders);

		//pr($orders);

		$this -> set('page_title', 'View Orders');
		$this -> layout = 'polka_shell';
	}

	//last edit - Ishan
	public function cancel($id = null) {

		if ($this -> request -> is('post')) {

			$order = $this -> request -> data;

			//pr($order);

			//$order['Order']['status'] = -1;

			if ($this -> Order -> save($order)) {

				//This flash message has to be set in the view properly
				$this -> Session -> setFlash('Order canceled.', 'default', array('class' => 'alert alert-success'), 'success');
				$this -> redirect(array('controller' => 'orders', 'action' => 'index'));
			} else {
				//This flash message has to be set in the view properly
				$this -> Session -> setFlash("Sorry. an error occurred.");
				$this -> redirect(array('controller' => 'orders', 'action' => 'index'));
			}
		} else {

			if ($id == null) {
				//This flash message has to be set in the view properly
				$this->Session->setFlash('You must select an order.', 'default', array('class' => 'alert alert-danger') , 'error');
				$this -> redirect(array('controller' => 'orders', 'action' => 'index'));
			} else {
				//fetch and display the courier details

				$order = $this -> Order -> findById($id);
				$this -> set('order', $order);

				//pr($order);

				//display the page for adding couriers
				$this -> set('page_title', 'Cancel Order');
				$this -> layout = 'polka_shell';
			}
		}
	}

    public function mark_complete($id=null) {
        if($id == null){
            $this->Session->setFlash('Please choose an order.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'orders', 'action' => 'payment_incomplete'));
        }

        $selectedOrder = $this->Order->findById($id);

        if($selectedOrder == null){
            $this->Session->setFlash('Please choose an order.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'orders', 'action' => 'payment_incomplete'));
        }


        $selectedOrder['Order']['status']=1;

        if($this->Order->save($selectedOrder)){
            $this->Session->setFlash('Order marked as complete.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect(array('controller' => 'orders', 'action' => 'payment_incomplete'));
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'orders', 'action' => 'payment_incomplete'));
        }

    }


	public function canceled() {

		// Load containable behavior on order item model to allow fetching of other models
		$this->Order->Behaviors->load('Containable');

		// Fetch the orders with finished payment and dispatch
        // we prepare our query, the cakephp way!
        $this->paginate = array(
			'conditions' => array('Order.status' => -1),
			'contain' => array('User'),
            'limit' => 10,
            'group' => 'Order.id',
            'order' => array('created' => 'desc')
        );


        // we are using the 'OrderItem' model
        $orders = $this->paginate('Order');

        //pr($orders);

		// Set the view variables to controller variable values and layout for the view
		$this -> set('orders', $orders);
		$this -> set('page_title', 'View Canceled Orders');
		$this -> layout = 'polka_shell';
	}


}
