<?php
/**
 * Created by IntelliJ IDEA.
 * User: Shoaib Merchant
 * Date: 10/4/13
 * Time: 2:37 AM
 * To change this template use File | Settings | File Templates.
 */

class EmailTemplatesController extends AppController {

    public $name = 'EmailTemplates';

    public $uses = array('EmailTemplate');

    public function index() {
        $emails = $this -> EmailTemplate-> find('all',array('order'=>array('EmailTemplate.title')));
        $this -> set('email_templates', $emails);

        $this -> set('page_title', 'Email Templates');
        $this -> layout = 'polka_shell';
    }

    public function add() {

        if ($this -> request -> is('post')) {
            $email = $this -> request -> data;

            $email['EmailTemplate']['content'] = htmlentities($email['EmailTemplate']['content']);



            if ($this -> EmailTemplate -> save($email)) {
                $this->Session->setFlash('New email added.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'email_templates', 'action' => 'index'));
            } else {
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'email_templates', 'action' => 'index'));
            }
        }
        else{

        }
        $this -> set('page_title', 'Add Email Template');
        $this -> layout = 'polka_shell';
    }

    public function edit($id) {

        if ($this -> request -> is('post')) {
            $email = $this -> request -> data;

            $email['EmailTemplate']['content'] = htmlentities($email['EmailTemplate']['content']);


            if ($this -> EmailTemplate -> save($email)) {
                $this->Session->setFlash('Email Template has been edited.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'email_templates', 'action' => 'index'));
            } else {
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'email_templates', 'action' => 'index'));
            }
        }
        else{

            if($id == null)
            {
                $this->Session->setFlash('Email Template does not exist.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'email_templates', 'action' => 'index'));
            }

            $selectedEmailTemplate = $this->EmailTemplate->findById($id);

            if($selectedEmailTemplate == null)
            {
                $this->Session->setFlash('Email Template does not exist.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'email_templates', 'action' => 'index'));
            }

            $this->set('selectedEmailTemplate',$selectedEmailTemplate);

        }
        $this -> set('page_title', 'Edit Email Template');
        $this -> layout = 'polka_shell';
    }


    public function delete($id=null) {

        if($id == null){
            $this->Session->setFlash('Please choose an email.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'email_templates', 'action' => 'index'));
        }

        $selectedEmailTemplate = $this->EmailTemplate->findById($id);

        if($selectedEmailTemplate == null){
            $this->Session->setFlash('Please choose an email.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'email_templates', 'action' => 'index'));
        }

        if($this->EmailTemplate->delete($selectedEmailTemplate['EmailTemplate']['id'])){
            $this->Session->setFlash('EmailTemplate deleted.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect(array('controller' => 'email_templates', 'action' => 'index'));
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'email_templates', 'action' => 'index'));
        }

        $this -> set('page_title', 'Delete Email Template');
        $this -> layout = 'polka_shell';
    }


}
