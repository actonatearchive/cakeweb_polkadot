<?php
/**
 * Created by IntelliJ IDEA.
 * User: Shoaib Merchant
 * Date: 10/4/13
 * Time: 2:37 AM
 * To change this template use File | Settings | File Templates.
 */

class SmsTemplatesController extends AppController {

    public $name = 'SmsTemplates';

    public $uses = array('SmsTemplate');

    public function index() {
        $sms_templates = $this -> SmsTemplate-> find('all',array('order'=>array('SmsTemplate.title')));
        $this -> set('sms_templates', $sms_templates);

        $this -> set('page_title', 'Sms Templates');
        $this -> layout = 'polka_shell';
    }

    public function add() {

        if ($this -> request -> is('post')) {
            $sms_template = $this -> request -> data;

            $sms_template['SmsTemplate']['content'] = htmlentities($sms_template['SmsTemplate']['content']);

            $sms_template['SmsTemplate']['alias'] = str_replace(" ","_",strtolower($sms_template['SmsTemplate']['title']));

            if ($this -> SmsTemplate -> save($sms_template)) {
                $this->Session->setFlash('New SMS Template added.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'sms_templates', 'action' => 'index'));
            } else {
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'sms_templates', 'action' => 'index'));
            }
        }
        else{

        }
        $this -> set('page_title', 'Add Sms Template');
        $this -> layout = 'polka_shell';
    }

    public function edit($id) {

        if ($this -> request -> is('post')) {
            $sms_template = $this -> request -> data;

            $sms_template['SmsTemplate']['content'] = htmlentities($sms_template['SmsTemplate']['content']);
            $sms_template['SmsTemplate']['alias'] = str_replace(" ","_",strtolower($sms_template['SmsTemplate']['title']));

            if ($this -> SmsTemplate -> save($sms_template)) {
                $this->Session->setFlash('Sms Template has been edited.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'sms_templates', 'action' => 'index'));
            } else {
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'sms_templates', 'action' => 'index'));
            }
        }
        else{

            if($id == null)
            {
                $this->Session->setFlash('Sms Template does not exist.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'sms_templates', 'action' => 'index'));
            }

            $selectedSmsTemplate = $this->SmsTemplate->findById($id);

            if($selectedSmsTemplate == null)
            {
                $this->Session->setFlash('Sms Template does not exist.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'sms_templates', 'action' => 'index'));
            }

            $this->set('selectedSmsTemplate',$selectedSmsTemplate);

        }
        $this -> set('page_title', 'Edit Sms Template');
        $this -> layout = 'polka_shell';
    }


    public function delete($id=null) {

        if($id == null){
            $this->Session->setFlash('Please choose an sms template.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'sms_templates', 'action' => 'index'));
        }

        $selectedSmsTemplate = $this->SmsTemplate->findById($id);

        if($selectedSmsTemplate == null){
            $this->Session->setFlash('Please choose an sms template.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'sms_templates', 'action' => 'index'));
        }

        if($this->SmsTemplate->delete($selectedSmsTemplate['SmsTemplate']['id'])){
            $this->Session->setFlash('SmsTemplate deleted.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect(array('controller' => 'sms_templates', 'action' => 'index'));
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'sms_templates', 'action' => 'index'));
        }

        $this -> set('page_title', 'Delete Sms Template');
        $this -> layout = 'polka_shell';
    }


}
