<?php
/**
 * Created by IntelliJ IDEA.
 * User: Shoaib Merchant
 * Date: 10/4/13
 * Time: 2:37 AM
 * To change this template use File | Settings | File Templates.
 */
App::uses('CakeEmail', 'Network/Email');
class OrderDispatchesController extends AppController {

	public $name = 'Order Dispatches';

	public $uses = array('Order', 'OrderItem','UserWallet', 'Courier', 'OrderDispatch','City','EmailTemplate','State','Coupon');
    public $components = array(
        'Email','SMS'
    );
	public function index() {
		$order_dispatches = $this -> OrderDispatch -> find('all',array('order'=> 'OrderDispatch.created DESC'));
		$this -> set('order_dispatches', $order_dispatches);

		$this -> set('page_title', 'View Order Dispatches');
		$this -> layout = 'polka_shell';
	}

	public function add($id=null) {

		if ($this -> request -> is('post')) {

			$odispatch = $this -> request -> data;
			//pr($odispatch);

            if($odispatch['OrderDispatch']['courier_id']==""){
                $odispatch['OrderDispatch']['courier_id'] = null;
            }

            if(sizeof($odispatch['OrderDispatch']['order_item_ids'])==0){
                $this -> Session -> setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger'), 'error');
                $this -> redirect($this->referer());
            }
          //pr($odispatch);die();
			if ($this -> OrderDispatch -> save($odispatch)) {

				$disp_id = $this -> OrderDispatch -> getInsertId();

				foreach ($odispatch['OrderDispatch']['order_item_ids'] as $order_item_id) {
						
					//print_r($order_item_id);die();	
						
					$order_item = $this -> OrderItem -> findById($order_item_id);
					
					//pr($order_item);die();
					
					$order_item['OrderItem']['order_dispatch_id'] = $disp_id;
					$this -> OrderItem -> save($order_item);
				}

                //Check if all items in Order are dispatched
                $total_items = $this->OrderItem->find('count',array('conditions'=>array('OrderItem.order_id'=>$odispatch['OrderDispatch']['order_id'])));
                $dispatched_items = $this->OrderItem->find('count',array('conditions'=>array('OrderItem.order_id'=>$odispatch['OrderDispatch']['order_id'],array('NOT'=>array('OrderItem.order_dispatch_id'=>null)))));


                if($total_items==$dispatched_items){

                    $selectedOrder = $this->Order->findById($odispatch['OrderDispatch']['order_id']);
                    $selectedOrder['Order']['status']=2;

                    $this->Order->save($selectedOrder);
                }

                //Get EmailTemplate
/*                $order_delivered_template = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.alias'=>'order_delivered')));

                if($order_delivered_template != null)
                {
                    //Send Mail
                    $Email = new CakeEmail('default');

                    $subject = $order_delivered_template['EmailTemplate']['title'];
                    $message =  html_entity_decode($order_delivered_template['EmailTemplate']['content']);

                    $message = str_replace("{user_name}",$selectedOrder['User']['first_name'],$message);

                    $message = str_replace("{order_id}",$selectedOrder['Order']['code'],$message);


                    $Email->emailFormat('html');
                    $Email->template('default');
                    $Email->to($selectedOrder['User']['username']);
                    $Email->subject($subject);
                    $Email->send($message);

                }

                //Send SMS
                if($selectedOrder['ShippingAddress']['mobile'] != "")
                {
                    $this->SMS->sendOrderDispatched($selectedOrder['Order']['code'],$selectedOrder['ShippingAddress']['mobile']);
                }*/


                //This flash message has to be set in the view properly
				$this -> Session -> setFlash('New dispatch added.', 'default', array('class' => 'alert alert-success'), 'success');
				// $this -> redirect(array('controller' => 'orderdispatches', 'action' => 'index'));
                $this -> redirect($this->referer());
			} else {
				//This flash message has to be set in the view properly
                $this -> Session -> setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger'), 'error');
				// $this -> redirect(array('controller' => 'orderdispatches', 'action' => 'index'));
                $this -> redirect($this->referer());
			}

		} else {

			$order = $this -> Order -> findById($id);
			$this -> set('order', $order);
			
			$shipping = $this -> City -> findById($order['ShippingAddress']['city_id']);
			$this -> set('shipping', $shipping);
			
			$billing = $this -> City -> findById($order['ShippingAddress']['city_id']);
			$this -> set('billing', $billing);
			
			$coupon = $this -> Coupon -> findById($order['Order']['coupon_id']);
			$this -> set('coupon', $coupon);
			
			//pr($coupon);

			$order_items = $this -> OrderItem -> find('all', array('conditions' => array('OrderItem.order_id' => $id,'OrderItem.order_dispatch_id'=>null), 'fields' => array('OrderItem.id', 'Item.name','Item.id','OrderItem.quantity','OrderItem.item_params')));
			$order_items_list = array();
			
			//print_r($order_items);

			foreach ($order_items as $oitem) {
                $order_items_list[$oitem['OrderItem']['id']] = array();
                if (isset($oitem['Item']['variant_name'])) {
					$order_items_list[$oitem['OrderItem']['id']]['name'] = $oitem['Item']['name'] . " (" . $oitem['Item']['variant_name'] . ") x ". $oitem['OrderItem']['quantity'];
				} else {
					$order_items_list[$oitem['OrderItem']['id']]['name'] = $oitem['Item']['name']. " x ". $oitem['OrderItem']['quantity'];
				}

                if($oitem['OrderItem']['item_params'] != ""){
                    $order_items_list[$oitem['OrderItem']['id']]['item_params'] = json_decode($oitem['OrderItem']['item_params'],true);
                }
                else{
                    $order_items_list[$oitem['OrderItem']['id']]['item_params'] = "";
                }
				$order_items_list[$oitem['OrderItem']['id']]['item_id'] = $oitem['Item']['id'];

			}

			$this -> set('order_items', $order_items_list);

			$couriers = $this -> Courier -> find('list');
			$this -> set('couriers', $couriers);

			
			
			//pr($order_items_list);die();

			$this -> set('page_title', 'New Dispatch');
			$this -> layout = 'polka_shell';

		}
	}

    public function delete($id=null) {

        if($id == null){
            $this->Session->setFlash('Please choose an item.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'orderdispatches', 'action' => 'index'));
        }

        $selectedItem = $this->OrderDispatch->findById($id);

        if($this->OrderDispatch->delete($selectedItem['OrderDispatch']['id'])){


            $selectedOrder = $this->Order->findById($selectedItem['OrderDispatch']['order_id']);
            $selectedOrder['Order']['status']=1;
            if($this->Order->save($selectedOrder))
            {
            $this->Session->setFlash('Order Dispatch deleted.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect(array('controller' => 'orderdispatches', 'action' => 'index'));
            }
            else{
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'orderdispatches', 'action' => 'index'));
            }
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'orderdispatches', 'action' => 'index'));
        }
    }
    
    
    public function mark_delivered($id=null){

            if($id == null){
                $this->Session->setFlash('Sorry an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'orders', 'action' => 'index'));
            }

            $selectedOrder = $this->Order->findById($id);

            if($selectedOrder == null){
                $this->Session->setFlash('Sorry an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'orders', 'action' => 'index'));
            }

            $selectedOrder['Order']['status']=3;       
            $amount = $this->getOrderGrandTotal($selectedOrder);
            $_amount = floor($amount/50);
            if($selectedOrder['Order']['reward_points_given'] == 0){

            $selectedOrder['Order']['reward_points_given']=$_amount;       
             // ========================================================


                if (!empty($selectedOrder['Order']['transaction_id'])) {

                    $transaction_id = $selectedOrder['Order']['transaction_id'];
                }
                $userWallet = [
                    'user_id' => $selectedOrder['Order']['user_id'],
                    'points' => $_amount,
                    'reason' => "Order successfully delivered: ".$selectedOrder['Order']['code'],
                    'added_by' => $this->Auth->user('id'),
                    'expires' => date('Y-m-d h:m:s', strtotime('+10 years')),
                    'transaction_type' => 0,
                    'transaction_id' => $transaction_id,

                ];

                $data = $this->UserWallet->save($userWallet);

            // ===========================================================
            
            
            }
            

            if ($this -> Order -> save($selectedOrder)) {
               
                




                //Get EmailTemplate
                $order_delivered_template = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.alias'=>'order_delivered')));

                if($order_delivered_template != null)
                {
                    //Send Mail
                    $Email = new CakeEmail('default');

                    $subject = $order_delivered_template['EmailTemplate']['title'];
                    $message =  html_entity_decode($order_delivered_template['EmailTemplate']['content']);

                    $message = str_replace("{user_name}",$selectedOrder['User']['first_name'],$message);

                    $message = str_replace("{order_id}",$selectedOrder['Order']['code'],$message);


                    $Email->emailFormat('html');
                    $Email->template('default','main');
                    $Email->to($selectedOrder['User']['username']);
                    $Email->subject($subject);
                    $Email->viewVars($message);

                    $Email->send($message);

                }

                //Send SMS
                // $sms = new Sms();
                if($selectedOrder['ShippingAddress']['mobile'] != "")
                {
                    $this->SMS->sendOrderDelivered($selectedOrder);
                } 

                //This flash message has to be set in the view properly
                $this -> Session -> setFlash('Marked Delivered.', 'default', array('class' => 'alert alert-success'), 'success');
                $this -> redirect(array('controller' => 'orders', 'action' => 'index'));          

            } else {
                //This flash message has to be set in the view properly
                $this -> Session -> setFlash("Sorry. an error occurred.");
                $this -> redirect(array('controller' => 'orders', 'action' => 'index'));
            }


                

    }    


}