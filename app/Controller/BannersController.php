<?php
/**
 * Created by IntelliJ IDEA.
 * User: Shoaib Merchant
 * Date: 10/4/13
 * Time: 2:37 AM
 * To change this template use File | Settings | File Templates.
 */

class BannersController extends AppController {

    public $name = 'Banners';

    public $uses = array('Banner', 'State','City','BannerCity');

    public function index() {
        $banners = $this -> Banner -> find('all',array('order'=>array('Banner.caption')));
        $this -> set('banners', $banners);

        $this -> set('page_title', 'Banners');
        $this -> layout = 'polka_shell';
    }

    public function add() {

        if ($this -> request -> is('post')) {
            $banner = $this -> request -> data;
	
			//pr($banner);die();

            if ($this -> Banner -> save($banner)) {
			
				$last_id = $this->Banner->getInsertID();
				
				//Set Cities
                $bannerCities = array();
                foreach($banner['Banner']['city_id'] as $key=>$value){
                    $newBannerCity = array();
                    $newBannerCity['BannerCity']['city_id'] = $value;
                    $newBannerCity['BannerCity']['banner_id'] = $last_id;

                    array_push($bannerCities, $newBannerCity);
                }

                if(sizeof($bannerCities)>0)
                {
                    $this->BannerCity->saveAll($bannerCities);
                }				
			
                //This flash message has to be set in the view properly
                $this->Session->setFlash('New banner added.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'banners', 'action' => 'index'));
            } else {
                //This flash message has to be set in the view properly
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'banners', 'action' => 'index'));
            }
        }
        else{

			$position = array('left-bottom'=>'left-bottom', 'left-top'=>'left-top','right-bottom'=>'right-bottom','right-top'=>'right-top');
            $this -> set('position', $position);
		
            //Get All Cities
            $cities = $this->City->find('list',array('order'=>array('City.name')));
            $this->set('cities',$cities);
		
        }
        $this -> set('page_title', 'Add Banner');
        $this -> layout = 'polka_shell';
    }


    public function edit($id=null) {

        if ($this -> request -> is('post')) {
            $banner = $this -> request -> data;

            if ($this -> Banner -> save($banner)) {
			
				//Remove Existing Cities
                $this->BannerCity->deleteAll(array('BannerCity.banner_id'=>$banner['Banner']['id']));
				
				//Set Cities
                $bannerCities = array();
                foreach($banner['Banner']['city_id'] as $key=>$value){
                    $newBannerCity = array();
                    $newBannerCity['BannerCity']['city_id'] = $value;
                    $newBannerCity['BannerCity']['banner_id'] = $banner['Banner']['id'];

                    array_push($bannerCities, $newBannerCity);
                }

                if(sizeof($bannerCities)>0)
                {
                    $this->BannerCity->saveAll($bannerCities);
                }		
				
                //This flash message has to be set in the view properly
                $this->Session->setFlash('Selected banner edited.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'banners', 'action' => 'index'));
            } else {
                //This flash message has to be set in the view properly
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'banners', 'action' => 'index'));
            }
        }

        else{

            if($id == null){
                $this->Session->setFlash('Please choose a banner.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'banners', 'action' => 'index'));
            }

            $selectedBanner = $this->Banner->findById($id);

            if($selectedBanner == null){
                $this->Session->setFlash('Please choose a banner.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'banners', 'action' => 'index'));
            }
			
            //Get Banner Cities
            $bannerCities = $this->BannerCity->find('all',array('conditions'=>array('BannerCity.banner_id'=>$selectedBanner['Banner']['id'])));

            $bannerCitiesList = array();
            foreach($bannerCities as $bannerCity){
                array_push($bannerCitiesList, $bannerCity['BannerCity']['city_id']);
            }

            $this->set('selectedCitiesList',$bannerCitiesList);			
			
			$position = array('left-bottom'=>'left-bottom', 'left-top'=>'left-top','right-bottom'=>'right-bottom','right-top'=>'right-top');
            $this -> set('position', $position);			
			
			//Get All Cities
            $cities = $this->City->find('list',array('order'=>array('City.name')));
            $this->set('cities',$cities);

            $this->set('banner',$selectedBanner);
            $this -> set('page_title', 'Edit Banner');
            $this -> layout = 'polka_shell';

        }
    }


    public function delete($id=null) {

        if($id == null){
            $this->Session->setFlash('Please choose a banner.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'banners', 'action' => 'index'));
        }

        $selectedBanner = $this->Banner->findById($id);

        if($selectedBanner == null){
            $this->Session->setFlash('Please choose a banner.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'banners', 'action' => 'index'));
        }

        if($this->Banner->delete($selectedBanner['Banner']['id'])){
            $this->Session->setFlash('Banner deleted.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect(array('controller' => 'banners', 'action' => 'index'));
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'banners', 'action' => 'index'));
        }

        $this -> set('page_title', 'Edit Banner');
        $this -> layout = 'polka_shell';
    }


}
