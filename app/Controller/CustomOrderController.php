<?php
// require_once('../Plugin/sqslib/SqsLib.php');
// require_once('../../../app/Plugin/sqslib/SqsLib.php');  //New One
// require_once('../Plugin/sms_engine/sms.php');

App::uses('CakeEmail', 'Network/Email');

class CustomOrderController extends AppController {
    public $uses = ['Order','User','UserAddress','Item','BillnoTrack','OrderItem','EmailTemplate','City','State','Country','ItemCity','OrderSubscription','ItemCategory','SkuItemMapping','OrderSkuItem','IndiapostPincode', 'Courier'];

    public function beforeFilter()
    {
        AppController::beforeFilter();
        $this->Auth->authenticate = array('Form');
        $this->Auth->authenticate = array(
            'Form' => array('userModel' => 'Admin')
        );
        $this->Auth->allow('login','getUsersList','getUserAddresses','addNewAddress');
    }


    //**************************************************************************

    function add()
    {
        $this->layout='base_layout';
        $this -> set('page_title', 'Add Custom Order');
        $couriers = $this -> Courier -> find('all');
        $this -> set('couriers', $couriers);

    }
    function recreate_order()
    {
        $this->layout='base_layout';
        $this -> set('page_title', 'Add RE-create Order');

    }
//-----------------------------------------------------------------------------------
    private function _getFiscalYear()
    {
        $currentMonth = date('m');
      	$fiscalYear = "";
      	if ($currentMonth > 3) {
      		//if month is greater than March -> CurrentYear + NextYear
      		//1718
      		$currentYear = date('y');
      		$nextYear = date('y',strtotime('+1 year'));

      		$fiscalYear =  $currentYear."-".$nextYear;

      	} else {
      		//if month is less than March -> LastYear + CurrentYear
            //1617
      		$lastYear = date('y',strtotime('-1 year'));
      		$currentYear = date('y');

      		$fiscalYear =  $lastYear."-".$currentYear;
      	}
        return $fiscalYear;
    }

    private function _getNextChallanNo()
    {
        $fiscalYear = $this->_getFiscalYear();
        $this->Order->Behaviors->load('Containable');
        $tmp = $this->Order->find('first',
            [
                'fields' => [
                    'id',
                    'challan_no'
                ],
                'conditions' => [
                    'Order.order_type !=' => null,
                    'Order.status !=' => 0,
                    'Order.challan_no !=' => null

                ],
                'order' => 'Order.order_placed_at desc',
                'contain' => []
            ]
        );
        // pr($tmp);
        if ($tmp['Order']['challan_no'] != null) {
            //Let's Break it to check it is for same FiscalYear
            //Also we have to get Only Challan no. from it so that we can +1 and then remerged it.
            $exp = explode('/Ch',$tmp['Order']['challan_no']);
            if ($exp[0] == $fiscalYear) { //Matching Both FiscalYear
                $new_challan_no = ((int) $exp[1] + 1);

                $new_challan_no = str_pad($new_challan_no, 4, "0", STR_PAD_LEFT);
                $formatted_challan_no = $fiscalYear."/Ch".$new_challan_no;
            } else {
                //FiscalYear changed so we have to start from 1.
                $new_challan_no = 1;
                $new_challan_no = str_pad($new_challan_no, 4, "0", STR_PAD_LEFT);
                $formatted_challan_no = $fiscalYear."/Ch".$new_challan_no;
            }
        } else {
            //Last one is NUll so let's Put 1
            $new_challan_no = 1;
            $new_challan_no = str_pad($new_challan_no, 4, "0", STR_PAD_LEFT);
            $formatted_challan_no = $fiscalYear."/Ch".$new_challan_no;
        }

        return $formatted_challan_no;
    }

    public function test()
    {
        $challan_no = $this->_getNextChallanNo();

        echo "Challan No: ".$challan_no;
        die();
    }
//------------------------------------------------------------------------------
    function placeOnlinePaymentOrder()
    {
        if($this->request->is('post'))
        {
            $data = $this->request->input('json_decode',true);
            if (isset($data['order_type'])) {

                $order_type = $data['order_type'];
                $challan_no = $this->_getNextChallanNo();
            } else {
                $order_type = null;
                $challan_no = null;
            }
            if (isset($data['order_id'])) {

                $porder_id = $data['order_id'];
            } else {
                $porder_id = null;

            }
            $order['Order']['code'] = strtoupper("ORD".substr(hash('sha256', mt_rand() . microtime()), 0, 15));
            $order['Order']['user_id'] = $data['user_id'];
            $order['Order']['shipping_address_id'] = $data['address_id'];
            $order['Order']['billing_address_id'] = $data['address_id'];
            //$order['Order']['payment_mode']= $data['payment_mode'];

            $items = $data['items'];

            if($res = $this->Order->save($order))
            {
                $order_id = $this->Order->getInsertID();
                $order['Order']['id']=$order_id;
                $total_amount = 0; $i = 0;
                foreach($items as $itm)
                {
                    $orderItem[$i]['OrderItem']['item_id'] = $itm['itm_id'];
                    $orderItem[$i]['OrderItem']['order_id'] = $order_id;
                    $orderItem[$i]['OrderItem']['price'] = $itm['itm_price'];
                    $orderItem[$i]['OrderItem']['quantity'] = $itm['variant_name'];
                    $orderItem[$i]['OrderItem']['qty'] = $itm['itm_qty'];

                    $orderItem[$i]['OrderItem']['item_name'] = $itm['itm_name'];
                    $orderItem[$i]['OrderItem']['item_image'] = $itm['itm_photo'];
                    $orderItem[$i]['OrderItem']['one_line3'] = $itm['one_line3'];
                    $total_amount+=($orderItem[$i]['OrderItem']['price']*$orderItem[$i]['OrderItem']['qty']);
                    $i++;
                }
                // $total_amount += ($data['ship_charge'] + $data['add_charge']);
                if($this->OrderItem->saveMany($orderItem))
                {
                    /*$b_temp=$this->BillnoTrack->find('first',array('conditions' => array('BillnoTrack.start_date <=' => date('Y-m-d'),'BillnoTrack.end_date >='=>date('Y-m-d'))));
                    $b_temp['BillnoTrack']['order_count']=($b_temp['BillnoTrack']['order_count']+1);
                    $this->BillnoTrack->save($b_temp);
                    $this->Order->id = $order_id;
                    $order['Order']['bill_no']=$b_temp['BillnoTrack']['financial_year'].''.str_pad($b_temp['BillnoTrack']['order_count'], 6, '0', STR_PAD_LEFT);*/

                    $order['Order']['shipping_charges'] = $data['ship_charge'];
                    $order['Order']['additional_charges'] = $data['add_charge'];

                    $order['Order']['discount_value'] = $data['discount'];
                    $order['Order']['courier_id'] = $data['courier_id'];
                    $order['Order']['order_type'] = $order_type;
                    $order['Order']['challan_no'] = $challan_no;
                    $order['Order']['order_id'] = $porder_id;
                    $order['Order']['status']=0;
                    $order['Order']['is_custom'] = 1;
                    /*$order['Order']['order_placed_at']=date('Y-m-d H:i:s');*/
                    $order['Order']['total_amount'] = $total_amount;
                    if($order=$this->Order->save($order))
                    {
                        $res = _sendMessage('prod_ordersQueue','InsertWarehouseOrder','Order','IN_PROGRESS',array('id'=>$order_id));

                        $order_item = $this->OrderItem->find('all',array('conditions'=>array('OrderItem.order_id'=>$order_id)));
                        $ordSkuItems = [];
                        for($i = 0; $i < sizeof($order_item); $i++)
                        {
                            $tmp[$i]['task'] = 'InsertWarehouseSKUItem';
                            $tmp[$i]['action'] = "IN_PROGRESS";
                            $tmp[$i]['entity'] = "OrderItem";
                            $tmp[$i]['payload'] = array('id'=>$order_item[$i]['OrderItem']['id']);

                            $sku_items = $this->getSkuItems($order_item[$i]['OrderItem']['item_id']);

                            foreach ($sku_items as $sku_itm) {
                                $tmp2 = [];
                                $tmp2['OrderSkuItem']['order_id'] = $order_id;
                                $tmp2['OrderSkuItem']['order_item_id'] = $order_item[$i]['OrderItem']['id'];
                                $tmp2['OrderSkuItem']['item_id'] = $sku_itm['SkuItemMapping']['item_id'];
                                $tmp2['OrderSkuItem']['sku_item_id'] = $sku_itm['SkuItemMapping']['sku_item_id'];
                                $tmp2['OrderSkuItem']['sku_qty'] = $sku_itm['SkuItemMapping']['qty'];
                                array_push($ordSkuItems, $tmp2);
                            }
                        }


                        if($this->OrderSkuItem->saveMany($ordSkuItems))
                        {
                            $res = _sendBatchMessage('prod_skuitemsQueue',$tmp);

                            // $this->log($order);
                            // $this->process_inventory($order_id);
                            $this->process_inventory_online($order_id);
                            //Order Successfully Placed
                            $res = new ResponseObject ( ) ;
                            $res -> status = 'success' ;
                            $res -> data = $order ;
                            $res -> order_id = $order_id;
                            $res -> message = 'Order has been placed!' ;
                            $this -> response -> body ( json_encode ( $res ) ) ;
                            return $this -> response ;
                        } else {
                            //Failed to placed Order...
                            //Let's RollBack
                            $this->Order->delete($order_id);
                            $this->OrderItem->deleteAll(array('OrderItem.order_id'=>$order_id));
                            $res = new ResponseObject ( ) ;
                            $res -> status = 'error' ;
                            $res -> message = 'Failed to place order!' ;
                            $this -> response -> body ( json_encode ( $res ) ) ;
                            return $this -> response ;
                        }

                    }
                    else {
                        //Failed to placed Order...
                        //Let's RollBack
                        $this->Order->delete($order_id);
                        $this->OrderItem->deleteAll(array('OrderItem.order_id'=>$order_id));
                        $res = new ResponseObject ( ) ;
                        $res -> status = 'error' ;
                        $res -> message = 'Failed to place order!' ;
                        $this -> response -> body ( json_encode ( $res ) ) ;
                        return $this -> response ;
                    }

                }
                else {
                    //Failed to insert OrderItem
                    //Let's RollBack
                    $this->Order->delete($order_id);
                    $this->OrderItem->deleteAll(array('OrderItem.order_id'=>$order_id));
                    $res = new ResponseObject ( ) ;
                    $res -> status = 'error' ;
                    $res -> message = 'Failed to place order! Something goes wrong with OrderItem' ;
                    $this -> response -> body ( json_encode ( $res ) ) ;
                    return $this -> response ;
                }

            }
            else {
                # code...
                $res = new ResponseObject ( ) ;
                $res -> status = 'error' ;
                $res -> message = 'Oops! Something went wrong. Please try again later.' ;
                $this -> response -> body ( json_encode ( $res ) ) ;
                return $this -> response ;
            }
        }
    }
//------------------------------------------------------------------------------
    function placeCODOrder()
    {
        if($this->request->is('post'))
        {
            $data = $this->request->input('json_decode',true);
            if (isset($data['order_type']) && $data['order_type']!=null) {
                $challan_no = $this->_getNextChallanNo();

                $order_type = $data['order_type'];
            } else {
                $order_type = null;
                $challan_no = null;
            }
            if (isset($data['order_id'])) {

                $porder_id = $data['order_id'];
            } else {
                $porder_id = null;

            }
            $order['Order']['code'] = strtoupper("ORD".substr(hash('sha256', mt_rand() . microtime()), 0, 15));
            $order['Order']['user_id'] = $data['user_id'];
            $order['Order']['shipping_address_id'] = $data['address_id'];
            $order['Order']['billing_address_id'] = $data['address_id'];
            $order['Order']['payment_mode']= $data['payment_mode'];
            $order['Order']['payment_through']= $data['payment_through'];

            $items = $data['items'];
            if($res = $this->Order->save($order))
            {
                $order_id = $this->Order->getInsertID();
                $order['Order']['id']=$order_id;
                $total_amount = 0; $i = 0;
                foreach($items as $itm)
                {
                    $orderItem[$i]['OrderItem']['item_id'] = $itm['itm_id'];
                    $orderItem[$i]['OrderItem']['order_id'] = $order_id;
                    $orderItem[$i]['OrderItem']['price'] = $itm['itm_price'];
                    $orderItem[$i]['OrderItem']['quantity'] = $itm['variant_name'];
                    $orderItem[$i]['OrderItem']['qty'] = $itm['itm_qty'];

                    $orderItem[$i]['OrderItem']['item_name'] = $itm['itm_name'];
                    $orderItem[$i]['OrderItem']['item_image'] = $itm['itm_photo'];
                    $orderItem[$i]['OrderItem']['one_line3'] = $itm['one_line3'];

                    $total_amount+=($orderItem[$i]['OrderItem']['price']*$orderItem[$i]['OrderItem']['qty']);
                    $i++;
                }
                // $total_amount += ($data['ship_charge'] + $data['add_charge']);
                if($this->OrderItem->saveMany($orderItem))
                {
//------------- OLD Bill Number ------------------------------------------------
                    // $b_temp=$this->BillnoTrack->find('first',array('conditions' => array('BillnoTrack.start_date <=' => date('Y-m-d'),'BillnoTrack.end_date >='=>date('Y-m-d'))));
                    // $b_temp['BillnoTrack']['order_count']=($b_temp['BillnoTrack']['order_count']+1);
                    // $this->BillnoTrack->save($b_temp);
                    // $this->Order->id = $order_id;
                    // $order['Order']['bill_no']=$b_temp['BillnoTrack']['financial_year'].''.str_pad($b_temp['BillnoTrack']['order_count'], 6, '0', STR_PAD_LEFT);
//------------------------------------------------------------------------------
                    $order['Order']['shipping_charges'] = $data['ship_charge'];
                    $order['Order']['additional_charges'] = $data['add_charge'];
                    $order['Order']['discount_value'] = $data['discount'];
                    $order['Order']['courier_id'] = $data['courier_id'];  
                    $order['Order']['order_type'] = $order_type;
                    $order['Order']['challan_no'] = $challan_no;
                    $order['Order']['order_id'] = $porder_id;

                    $order['Order']['status']=1;
                    $order['Order']['is_custom'] = 1; //Custom Order Yes
                    $order['Order']['order_placed_at']=date('Y-m-d H:i:s');
                    $order['Order']['delivery_date']=date('Y-m-d');
                    $order['Order']['total_amount'] = $total_amount;
                    if($order=$this->Order->save($order))
                    {
                        // $this->log($order);
                        $res = _sendMessage('prod_ordersQueue','InsertWarehouseOrder','Order','IN_PROGRESS',array('id'=>$order_id));
                        $res = _sendMessage('prod_billNumbersQueue','BillNumberGenerator','BillNumber','COMPLETED',array('id'=>$order_id));

                        $order_item = $this->OrderItem->find('all',array('conditions'=>array('OrderItem.order_id'=>$order_id)));
                        $ordSkuItems = [];
                        for($i = 0; $i < sizeof($order_item); $i++)
                        {
                            $tmp[$i]['task'] = 'InsertWarehouseSKUItem';
    			            $tmp[$i]['action'] = "IN_PROGRESS";
    			            $tmp[$i]['entity'] = "OrderItem";
    			            $tmp[$i]['payload'] = array('id'=>$order_item[$i]['OrderItem']['id']);

                            $sku_items = $this->getSkuItems($order_item[$i]['OrderItem']['item_id']);

                            foreach ($sku_items as $sku_itm) {
                                $tmp2 = [];
                                $tmp2['OrderSkuItem']['order_id'] = $order_id;
                                $tmp2['OrderSkuItem']['order_item_id'] = $order_item[$i]['OrderItem']['id'];
                                $tmp2['OrderSkuItem']['item_id'] = $sku_itm['SkuItemMapping']['item_id'];
                                $tmp2['OrderSkuItem']['sku_item_id'] = $sku_itm['SkuItemMapping']['sku_item_id'];
                                $tmp2['OrderSkuItem']['sku_qty'] = $sku_itm['SkuItemMapping']['qty'];
                                array_push($ordSkuItems, $tmp2);
                            }

                        }

                        if($this->OrderSkuItem->saveMany($ordSkuItems))
                        {
                            $res = _sendBatchMessage('prod_skuitemsQueue',$tmp);

                            // pr($order);die();

                            $this->process_inventory($order_id);
                            //Order Successfully Placed
                            $res = new ResponseObject ( ) ;
                            $res -> status = 'success' ;
                            $res -> data = $order ;
                            $res -> order_id = $order_id;
                            $res -> message = 'Order has been placed!' ;
                            $this -> response -> body ( json_encode ( $res ) ) ;
                            return $this -> response ;
                        } else {
                            //Failed to placed Order...
                            //Let's RollBack
                            $this->Order->delete($order_id);
                            $this->OrderItem->deleteAll(array('OrderItem.order_id'=>$order_id));
                            $res = new ResponseObject ( ) ;
                            $res -> status = 'error' ;
                            $res -> message = 'Failed to place order!' ;
                            $this -> response -> body ( json_encode ( $res ) ) ;
                            return $this -> response ;
                        }

                    }
                    else {
                        //Failed to placed Order...
                        //Let's RollBack
                        $this->Order->delete($order_id);
                        $this->OrderItem->deleteAll(array('OrderItem.order_id'=>$order_id));
                        $res = new ResponseObject ( ) ;
                        $res -> status = 'error' ;
                        $res -> message = 'Failed to place order!' ;
                        $this -> response -> body ( json_encode ( $res ) ) ;
                        return $this -> response ;
                    }

                }
                else {
                    //Failed to insert OrderItem
                    //Let's RollBack
                    $this->Order->delete($order_id);
                    $this->OrderItem->deleteAll(array('OrderItem.order_id'=>$order_id));
                    $res = new ResponseObject ( ) ;
                    $res -> status = 'error' ;
                    $res -> message = 'Failed to place order! Something goes wrong with OrderItem' ;
                    $this -> response -> body ( json_encode ( $res ) ) ;
                    return $this -> response ;
                }

            }
            else {
                # code...
                $res = new ResponseObject ( ) ;
                $res -> status = 'error' ;
                $res -> message = 'Oops! Something went wrong. Please try again later.' ;
                $this -> response -> body ( json_encode ( $res ) ) ;
                return $this -> response ;
            }


        }
    }

//******************************************************************************
 // Success Page
//------------------------------------------------------------------------------
function cod_success($id = null,$order_code = null)
{
    //
    $this->layout = 'base_layout';
    $this->set('order_id',$id);
    $order=$this->Order->find('first',[
        'fields'=>'order_type','conditions'=>['Order.code'=>$order_code]
    ]);
    //pr($order);die();
    $this->set(compact('order_code','order'));
}

function online_success($id = null,$order_code = null)
{
    $this->layout = 'base_layout';
    $this->set('order_id',$id);
    $this->set('order_code',$order_code);
}
//------------------------------------------------------------------------------
//      *** Customer Registration ***
//------------------------------------------------------------------------------
    function addNewCustomer()
    {
        if($this->request->is('post'))
        {
            $data = $this->request->input('json_decode',true);
            $chk = $this->User->find('all',array('conditions'=>array('User.username'=>$data['user']['email'])));

            function randomPassword( $length = 6 )
            {
                $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
                $str = substr( str_shuffle( $chars ), 0, $length );
                return $str;
            }

            if(sizeof($chk) > 0)
            {
                $res = new ResponseObject ( ) ;
                $res -> status = 'error' ;
                $res -> message = 'User already exists!' ;
                $this -> response -> body ( json_encode ( $res ) ) ;
                return $this -> response ;
            }
            else {
                //Let's Register him/her
                $tmp['User']['first_name'] = $data['user']['first_name'];
                $tmp['User']['last_name'] = $data['user']['last_name'];
                $tmp['User']['username'] = $data['user']['email'];
                $tmp['User']['user_email'] = $data['user']['email'];
                $tmp['User']['password'] = randomPassword();
                if($resp = $this->User->save($tmp))
                {

                    //Get EmailTemplate
                    $registerEmailTemplate= file_get_contents("../static/mailer/welcome.html");
                    // $registerEmailContent = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.alias'=>'register_success')));

                    if($registerEmailTemplate != "")
                    {
                        //Send Mail
                        $Email = new CakeEmail('default');
                        $subject = "Thank you for registering";
                        $message =  $registerEmailTemplate;

                        // $body_content =  html_entity_decode($registerEmailContent['EmailTemplate']['content']);

                        $message = str_replace("{user_name}",$resp['User']['first_name'],$message);
                        $message = str_replace("{email}",$resp['User']['username'],$message);
                        $message = str_replace("{password}",$tmp['User']['password'],$message);
                        // $message = str_replace("{content}",$body_content,$message);

                        $Email->emailFormat('html');

                        $Email->to($resp['User']['username']);
                        $Email->subject($subject);
                        $Email->send($message);
                    }

                    $res = new ResponseObject ( ) ;
                    $res -> status = 'success' ;
                    $res -> data = $resp ;
                    $res -> id = $resp['User']['id'];
                    $res -> message = 'Successfully registered!' ;
                    $this -> response -> body ( json_encode ( $res ) ) ;
                    return $this -> response ;
                }
                else {
                    # code...
                    $res = new ResponseObject ( ) ;
                    $res -> status = 'error' ;
                    $res -> message = 'Oops! Something went wrong.' ;
                    $this -> response -> body ( json_encode ( $res ) ) ;
                    return $this -> response ;
                }

            }
        }
    }
//-------------------------------------------------------------------------------
    function getSkuItems($item_id = null)
    {
        $sku_items = $this->SkuItemMapping->find('all', array(
            'conditions' => array(
                'SkuItemMapping.item_id' => $item_id
            )
        ));
        return $sku_items;
    }
//------------------------------------------------------------------------------
    function addNewAddress()
    {
        if($this->request->is('post'))
        {
            $data = $this->request->input('json_decode',true);
            $this->UserAddress->create();

            $tmp['UserAddress']['user_id'] = $data['id'];

            $tmp['UserAddress']['name'] = $data['address']['name'];
            // $tmp['UserAddress']['line1'] = str_replace("'","",$data['address']['line1']);
            // $tmp['UserAddress']['line2'] = str_replace("'","",$data['address']['line2']);
            // $tmp['UserAddress']['landmark'] = str_replace("'","",$data['address']['landmark']);

            $tmp['UserAddress']['line1'] = $data['address']['line1'];
            $tmp['UserAddress']['line2'] = $data['address']['line2'];
            $tmp['UserAddress']['landmark'] = $data['address']['landmark'];
            $tmp['UserAddress']['mobile'] = $data['address']['mobile'];
            $tmp['UserAddress']['pin_code'] = $data['address']['pin_code'];
            $tmp['UserAddress']['city_name'] = $data['address']['city_name'];
            $tmp['UserAddress']['state_name'] = $data['address']['state_name'];
            $tmp['UserAddress']['city_id'] = null;

            if($resp = $this->UserAddress->save($tmp))
            {
                $res = new ResponseObject ( ) ;
                $res -> status = 'success' ;
                $res -> data = $resp ;
                $res -> id = $data['id'];
                $res -> message = 'New address added!' ;
                $this -> response -> body ( json_encode ( $res ) ) ;
                return $this -> response ;
            }
            else {
                # code...
                $res = new ResponseObject ( ) ;
                $res -> status = 'error' ;
                // $res -> data = $resp ;
                $res -> id = $data['id'];
                $res -> message = 'Failed to add address!' ;
                $this -> response -> body ( json_encode ( $res ) ) ;
                return $this -> response ;
            }
        }
    }

//------------------------------------------------------------------------------
    function editAddress()
    {
        if($this->request->is('post'))
        {
            $data = $this->request->input('json_decode',true);

            $tmp['UserAddress']['user_id'] = $data['address']['user_id'];
            $tmp['UserAddress']['name'] = $data['address']['name'];
            $tmp['UserAddress']['line1'] = $data['address']['line1'];
            $tmp['UserAddress']['line2'] = $data['address']['line2'];
            $tmp['UserAddress']['landmark'] = $data['address']['landmark'];
            $tmp['UserAddress']['mobile'] = $data['address']['mobile'];
            $tmp['UserAddress']['pin_code'] = $data['address']['pin_code'];
            $tmp['UserAddress']['city_name'] = $data['address']['city_name'];
            $tmp['UserAddress']['state_name'] = $data['address']['state_name'];
            $tmp['UserAddress']['city_id'] = null;

            $this->UserAddress->id = $data['address']['id'];
            if($resp = $this->UserAddress->save($tmp))
            {
                $res = new ResponseObject ( ) ;
                $res -> status = 'success' ;
                $res -> data = $resp ;
                $res -> id = $data['address']['id'];
                $res -> message = 'Address has been edited!' ;
                $this -> response -> body ( json_encode ( $res ) ) ;
                return $this -> response ;
            }
            else {
                # code...
                $res = new ResponseObject ( ) ;
                $res -> status = 'error' ;
                // $res -> data = $resp ;
                $res -> id = $data['address']['id'];
                $res -> message = 'Failed to edit address!' ;
                $this -> response -> body ( json_encode ( $res ) ) ;
                return $this -> response ;
            }
        }
    }

//------------------------------------------------------------------------------
    function getProductItems()
    {
        if($this->request->is('post'))
        {
            $data = $this->request->input('json_decode',true);
            $this->Item->Behaviors->load('Containable');

            $name = $data['item_name'];
            $tmp = $this->Item->find('all',
                array(
                    'fields' => array('Item.id','Item.name','Item.alias','Item.primary_photo_directory','Item.primary_photo','Item.item_type_id','Item.sku_code','Item.stock_type','Item.featured','Item.variant_name','Item.item_id','Item.price','Item.discount_price','Item.one_line3'),
                    'conditions'=> array(
                        'Item.name LIKE'=> "%$name%",
                        'Item.item_category_id !='=> "ee2db3d0-b471-11e5-abe4-023bbb370ca3",
                        'Item.is_parent' => 0
                    ),
                    'contain'=>array()
                )
            );
            if(sizeof($tmp) > 0)
            {
                $res = new ResponseObject ( ) ;
                $res -> status = 'success' ;
                $res -> data = $tmp ;
                $res -> message = 'Items found!' ;
                $this -> response -> body ( json_encode ( $res ) ) ;
                return $this -> response ;
            }
            else {
                # code...
                $res = new ResponseObject ( ) ;
                $res -> status = 'error' ;
                $res -> message = 'Failed to find items!' ;
                $this -> response -> body ( json_encode ( $res ) ) ;
                return $this -> response ;
            }
        }
    }
//------------------------------------------------------------------------------
    function getUsersList($name = null)
    {
        //
        if($name != null) {
            $tmp = $this->User->find('all',
                array(
                    'conditions' =>
                    array(
                        // 'User.first_name LIKE'=> '%'.$name.'%'
                        'OR' => array(
                            'User.first_name LIKE'=> "%$name%",
                            'User.username LIKE'=> "%$name%",
                            'User.mobile LIKE'=> "%$name%",
                        )
                    ),
                    'limit'=>20
                )
            );

            //
            $idx = 0; $users = [];
            foreach ($tmp as $t) {
                # code...
                $users[$idx]['user_id'] = $t['User']['id'];
                $users[$idx]['email'] = $t['User']['username'];
                $users[$idx]['first_name'] = $t['User']['first_name'];
                $users[$idx]['last_name'] = $t['User']['last_name'];
                $users[$idx]['name'] = $t['User']['first_name']." ".$t['User']['last_name'] . ' (' . $t['User']['username'] . ')';
                $users[$idx]['desc'] = $t['User']['username']." - ".$t['User']['mobile'];
                $idx ++;
            }
            $res = new ResponseObject ( ) ;
            $res -> status = 'success' ;
            $res -> data = $users ;
            $res -> name = $name;
            $res -> message = 'Users found!' ;
            $this -> response -> body ( json_encode ( $res ) ) ;
            return $this -> response ;
        }
    }



    function getUserAddresses()
    {
        if($this->request->is('post'))
        {
            $data = $this->request->input('json_decode',true);
            $tmp = $this->UserAddress->find('all',
            array('conditions'=>
                    array(
                        'user_id'=>$data['id']
                    )
                )
            );

            if(sizeof($tmp) > 0)
            {
                $res = new ResponseObject ( ) ;
                $res -> status = 'success' ;
                $res -> data = $tmp ;
                $res -> id = $data['id'];
                $res -> message = 'Address found!' ;
                $this -> response -> body ( json_encode ( $res ) ) ;
                return $this -> response ;
            }
            else {
                $res = new ResponseObject ( ) ;
                $res -> status = 'error' ;
                $res -> data = $tmp ;
                $res -> id = $data['id'];
                $res -> message = 'No address found!' ;
                $this -> response -> body ( json_encode ( $res ) ) ;
                return $this -> response ;
            }
        }
    }

    //check For pinCode
    function pincodeCheck()
    {
        if($this->request->is('post'))
        {
            $data = $this->request->input('json_decode',true);

            if($this->check_pincode_bluedart($data['pincode'])) {
                $res = new ResponseObject ( ) ;
                $res -> status = 'success' ;
                $res -> pincode = $data['pincode'] ;
                $res -> message = 'COD available.' ;
                $res -> courier = 'Bluedart' ;
                $this -> response -> body ( json_encode ( $res ) ) ;
                return $this -> response ;
            }
            else {
                //Not Available in Bluedart Then Call Delhivery
                if($this->check_service_delhivery($data['pincode'])) {
                    $res = new ResponseObject ( ) ;
                    $res -> status = 'success' ;
                    $res -> pincode = $data['pincode'] ;
                    $res -> message = 'COD available.' ;
                    $res -> courier = 'Delhivery' ;
                    $this -> response -> body ( json_encode ( $res ) ) ;
                    return $this -> response ;
                }
                else {
                    //Not Available in Delhivery Too.
                    //Let's check for IndiaPost
                    if ($this->check_service_indiapost($data['pincode'])) {
                        $res = new ResponseObject ( ) ;
                        $res -> status = 'success' ;
                        $res -> pincode = $data['pincode'] ;
                        $res -> message = 'COD available.' ;
                        $res -> courier = 'IndiaPost' ;
                        $this -> response -> body ( json_encode ( $res ) ) ;
                        return $this -> response ;
                    } else {
                        // It Means DISABLED COD Button.
                        $res = new ResponseObject ( ) ;
                        $res -> status = 'error' ;
                        $res -> pincode = $data['pincode'] ;
                        $res -> message = 'COD not available.' ;
                        $this -> response -> body ( json_encode ( $res ) ) ;
                        return $this -> response ;
                    }
                }
            }
        }
    }

//------------------------------------------------------------------------------
//  Pincode Check

function check_pincode_bluedart($pincode=null){
    if($pincode!=null)
    {
        $pincode=trim($pincode);

        try {
            //Live
            $soap_pincode_check = new PincodeCheck('http://netconnect.bluedart.com/Ver1.7/ShippingAPI/Finder/ServiceFinderQuery.svc?wsdl',
            array(
                'trace'               => 1,
                'style'               => SOAP_DOCUMENT,
                'use'                 => SOAP_LITERAL,
                'soap_version'        => SOAP_1_2
            ));
            //Live
            $soap_pincode_check->__setLocation("http://netconnect.bluedart.com/Ver1.7/ShippingAPI/Finder/ServiceFinderQuery.svc");

            $soap_pincode_check->sendRequest = true;
            $soap_pincode_check->printRequest = false;
            $soap_pincode_check->formatXML = true;
            $actionHeader = new SoapHeader('http://www.w3.org/2005/08/addressing','Action','http://tempuri.org/IServiceFinderQuery/GetServicesforPincode',true);
            $soap_pincode_check->__setSoapHeaders($actionHeader);
            $params = array(
              'pinCode' => $pincode,
              'profile' => array(
                'Api_type' => 'S',
                'Area'=>'ALL',
                'Customercode'=>'415925',
                //demo
                //'LicenceKey'=>'cdb032a6daf6cd10814d86da652e9473',
                //live
                'LicenceKey'=>'1618b5db041bbe4f4594340a044b4cfb',
                'LoginID'=>'AB415262',
                'Version'=>'1.7'
                )
              );

            $result = $soap_pincode_check->__soapCall('GetServicesforPincode',array($params));
            //  pr($result);die();
            if($result->GetServicesforPincodeResult->IsError === true ||
                $result->GetServicesforPincodeResult->Embargo == 'Yes' ||
                $result->GetServicesforPincodeResult->eTailCODAirInbound === 'No' ||
                $result->GetServicesforPincodeResult->AreaCode==null)
            {
                return false;
            }
            else {
                return true;
            }
        }
        catch(Exception $e) {
            return false;
        }
    }
    return false;


}


public function check_service_delhivery($pincode=null)
{
    if($pincode == null) {
        return false;
    }
    $pincode=trim($pincode);
    $token='ddc7ee3dbf4c9de89e5a2ee7c070c4ecdb537745';
    $url='https://track.delhivery.com/c/api/pin-codes/json/?token='.$token.'&filter_codes='.$pincode;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $result = curl_exec($ch);
    curl_close($ch);
    $msg=json_decode($result,true);
    if($msg['delivery_codes']!=null)
    {
        if($msg['delivery_codes'][0]['postal_code']['cod']=="Y")
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

public function check_service_indiapost($pincode = null)
{
    if ($pincode == null) {
        return false;
    }
    $pincode=trim($pincode);
    $code_exists = $this->IndiapostPincode->find('first',array('conditions'=>array('pincode'=>$pincode)));
    if($code_exists != null){
        return true;
    }
    else {
        return false;
    }
}
//=-----------------------------------------------------------------------------

public function process_inventory_online($id) {
      $check_subscription_order='false';
      //changes in inventory
      $this->Order->Behaviors->load('Containable');
       $order=$this->Order->find('first',array(
        'conditions'=>array('Order.id'=>$id),
        'contain'=>array('OrderItem','ShippingAddress','User.username')
        ));



          //Send Mail
        //Send Mail
        if($check_subscription_order=='true'){
          $orderTemplate= file_get_contents("../static/mailer/shaveplan_order.html");
        }else{
          $orderTemplate= file_get_contents("../static/mailer/payment.html");
        }


        if($orderTemplate != ""){
            //Send Mail
            $Email = new CakeEmail('default');

            $subject = "Thank you for your Order";
            $message =  $orderTemplate;

            $message = str_replace("{order_code}",$order['Order']['code'],$message);
            $message = str_replace("{order_id}",$order['Order']['id'],$message);

            $count = 0;
            $order_items = $this->OrderItem->find('all',array('conditions'=>array('OrderItem.order_id'=>$order['Order']['id'])));
            $order_items_text = "";
            $sub_total=0;
            $delivery_details='';
            foreach($order_items as $order_item)
            {
              //pr($order_item);
              $order_items_text.='<tr style="border: 1px solid #ccc;width: 800px;height: 40px;color:#000;background: #fff;">
                <td align="center" style="font-size: 15px; font-family: Verdana, sans-serif; color: #333333; padding: 5px 15px 5px 15px" class="padding"><center>
                    <img style="  width: 35%;" src="'.$this->CDNURL.'item/primary_photo/'.$order_item['Item']['primary_photo_directory'].'/'.$order_item['Item']['primary_photo'].'">
                </center></td>
                <td align="center" style="text-align: justify;font-size: 15px; font-family: Verdana, sans-serif; color: #333333; padding: 5px 15px 5px 15px" class="padding">

                        <b style="font-size: 15px;text-align:center;display: block;">'.$order_item['Item']['name'].'</b>
                        <span style="color:#00a8e0;font-weight:bold;">'.$order_item['Item']['one_line3'].'</span><br/>Quantity: '.$order_item['OrderItem']['qty'].
                        '<br>'.$order_item['Item']['short_desc'].'</td>
                <td style="font-family: Verdana, sans-serif;text-align: center;color:  #505050;width: 30%;">Rs.'.($order_item['OrderItem']['price']*$order_item['OrderItem']['qty']).'</td>
              </tr>';

              $sub_total+=($order_item['OrderItem']['price']*$order_item['OrderItem']['qty']);
              if($order_item['Item']['subscription_enabled']==1 || $order_item['Item']['item_category_id']=='566ac30a-a3dc-4768-9780-15e4bf642048'){
                //$delivery_details="Order will be delivered on 30 June.";
              }
            }
            //die();
            // $shipping = $this -> City -> findById($order['ShippingAddress']['city_id']);
            //$shipping=$this->UserAddress->findById($order['Order']['shipping_address_id']);
            // $address=$order['ShippingAddress']['line1']." ".$order['ShippingAddress']['line2']." ".$order['ShippingAddress']['landmark']." ".
            // $order['ShippingAddress']['city_name']." - ".$order['ShippingAddress']['pin_code']."<br />".$order['ShippingAddress']['state_name']."<br />";
            $message = str_replace("{shipping_name}", $order['ShippingAddress']['name'],$message);
            $message = str_replace("{shipping_line1}", $order['ShippingAddress']['line1'],$message);
            $message = str_replace("{shipping_line2}", $order['ShippingAddress']['line2'],$message);
            $message = str_replace("{shipping_landmark}", $order['ShippingAddress']['landmark'],$message);
            $message = str_replace("{shipping_city}", $order['ShippingAddress']['city_name'],$message);
            $message = str_replace("{shipping_pincode}", $order['ShippingAddress']['pin_code'],$message);


            // $message = str_replace("{shipping_address}", $address, $message);

            $message = str_replace("{order_items}", $order_items_text, $message);
            $message = str_replace("{sub_total}", $sub_total, $message);

            $message= str_replace("{delivery_details}", $delivery_details, $message);
             $message = str_replace("{grand_total}", $order['Order']['total_amount']+$order['Order']['additional_charges']+$order['Order']['shipping_charges']+$order['Order']['cod_charges']-$order['Order']['discount_value']-$order['Order']['extra_discount'], $message);
             $message = str_replace("{order_amount}", $order['Order']['total_amount'], $message);

            if($order['Order']['shipping_charges']>0)
            {
                $message = str_replace("{shipping_charges}", $order['Order']['shipping_charges'], $message);
            }
            else{
                  $message = str_replace("{shipping_charges}", '0', $message);
            }

            if($order['Order']['cod_charges']>0){
              $temptext='<tr style="border: 1px solid #ccc;width: 800px;height: 40px;color:#000;background: #fff;">
                          <td></td>

                          <td style="background: #fff;text-align: center;color:#000;font-size: 12px;text-transform: uppercase;font-family: Verdana, sans-serif;">cash on delivery charge</td>
                          <td style="text-align:center;background: #fff;font-family: Verdana, sans-serif;"><b>Rs.'.$order['Order']['cod_charges'].'</b></td>
                      </tr>';
              $message = str_replace("{cod_charges}", $temptext, $message);
            }else
            {
              $message = str_replace("{cod_charges}", '', $message);

            }
            if(($order['Order']['discount_value']+$order['Order']['extra_discount'])>0){
              $temptext='<tr style="width: 800px;height: 40px;">
                          <td></td>

                          <td style="width: 800px;height: 40px;color:#EBDFC3;text-transform: uppercase;font-family: Verdana, sans-serif;">Discount</td>
                          <td style="text-align:center;font-family: Verdana, sans-serif;"><b>Rs.'.($order['Order']['discount_value']+$order['Order']['extra_discount']).'</b></td>
                      </tr>';
              $message = str_replace("{discount_value}", $temptext, $message);
            }else
            {
              $message = str_replace("{discount_value}", '', $message);

            }
            if($order['Order']['additional_charges']>0){
              $temptext='<tr style="border:1px solid #ccc;width:800px;height:40px;color:#000;background:#fff">
                          <td></td>

                          <td style="background:#fff;text-align:center;color:#000;font-size:12px;text-transform:uppercase;font-family: Verdana, sans-serif;">additional charges</td>
                          <td style="background:#fff;text-align:center;font-family: Verdana, sans-serif;"><b>Rs.'.$order['Order']['additional_charges'].'</b></td>
                      </tr>';
              $message = str_replace("{additional_charges}", $temptext, $message);
            }else
            {
              $message = str_replace("{additional_charges}", '', $message);

            }
            //$message = str_replace("{discount_value}", $order['Order']['discount_value']+$order['Order']['extra_discount'], $message);

           // $message = str_replace("{order_items}",$order_details, $message);
            $message = str_replace("{order_code}",$order['Order']['code'], $message);
            $message = str_replace("{order_id}",$order['Order']['id'], $message);

            $myDateTime = strtotime($order['Order']['modified']);
            $newDateString = date('l, F d Y',$myDateTime);
            $message = str_replace("{order_time}",$newDateString, $message);

            $Email->emailFormat('html');
            $Email->to($order['User']['username']);
            $Email->subject($subject);
            $Email->from(array('noreply@letsshave.com' => 'LetsShave'));
            $Email->send($message);


           $Email_owner = new CakeEmail('default');
           $Email_owner->emailFormat('html');
           //$Email_owner->to('asawaripawar26@gmail.com');
           $Email_owner->to('mohammed.sufyan@actonate.com');
           $Email_owner->subject('LS orders');
           $Email->from(array('noreply@letsshave.com' => 'LetsShave'));
           $Email_owner->send($order['Order']['code']."<br>".$order['Order']['id']."<br>".$order['User']['username']);

        }
    }
//------------------------------------------------------------------------------
    public function process_inventory($id) {
          $check_subscription_order='false';

          //changes in inventory
          $this->Order->Behaviors->load('Containable');
           $order=$this->Order->find('first',array(
            'conditions'=>array('Order.id'=>$id),
            'contain'=>array('OrderItem','ShippingAddress','User.username')
            ));


            //send Order Confirmation sms
            //Send SMS
            /*$sms = new Sms();
            if($order['ShippingAddress']['mobile'] != "")
            {
             $sms->sendOrderConfirmation($order['Order']['code'],$order['Order']['total_amount']+$order['Order']['additional_charges']+$order['Order']['shipping_charges']+$order['Order']['cod_charges']-$order['Order']['discount_value']-$order['Order']['extra_discount'],$order['ShippingAddress']['mobile']);
            }*/
            try {
                //Send SMS
                $sms = new Sms();
                $sms->sendOrderConfirmation($order['Order']['code'], $order['Order']['total_amount'], $order['ShippingAddress']['mobile']);
            }
            catch(Exception $err) {}

              //Send Mail
            //Send Mail
            if($check_subscription_order=='true'){
              $orderTemplate= file_get_contents("../static/mailer/shaveplan_order.html");
            }else{
              $orderTemplate= file_get_contents("../static/mailer/track_order.html");
            }


            if($orderTemplate != ""){
                //Send Mail
                $Email = new CakeEmail('default');

                $subject = "Thank you for your Order";
                $message =  $orderTemplate;

                $message = str_replace("{order_code}",$order['Order']['code'],$message);
                $message = str_replace("{order_id}",$order['Order']['id'],$message);

                $count = 0;
                $order_items = $this->OrderItem->find('all',array('conditions'=>array('OrderItem.order_id'=>$order['Order']['id'])));
                $order_items_text = "";
                $sub_total=0;
                $delivery_details='';
                foreach($order_items as $order_item)
                {
                  //pr($order_item);
                  $order_items_text.='<tr style="border: 1px solid #ccc;width: 800px;height: 40px;color:#000;background: #fff;">
                    <td align="center" style="font-size: 15px; font-family: Verdana, sans-serif; color: #333333; padding: 5px 15px 5px 15px" class="padding"><center>
                        <img style="  width: 35%;" src="'.$this->CDNURL.'item/primary_photo/'.$order_item['Item']['primary_photo_directory'].'/'.$order_item['Item']['primary_photo'].'">
                    </center></td>
                    <td align="center" style="text-align: justify;font-size: 15px; font-family: Verdana, sans-serif; color: #333333; padding: 5px 15px 5px 15px" class="padding">

                            <b style="font-size: 15px;text-align:center;display: block;">'.$order_item['Item']['name'].'</b>
                            <span style="color:#00a8e0;font-weight:bold;">'.$order_item['Item']['one_line3'].'</span><br/>Quantity: '.$order_item['OrderItem']['qty'].
                            '<br>'.$order_item['Item']['short_desc'].'</td>
                    <td style="font-family: Verdana, sans-serif;text-align: center;color:  #505050;width: 30%;">Rs.'.($order_item['OrderItem']['price']*$order_item['OrderItem']['qty']).'</td>
                  </tr>';

                  $sub_total+=($order_item['OrderItem']['price']*$order_item['OrderItem']['qty']);
                  if($order_item['Item']['subscription_enabled']==1 || $order_item['Item']['item_category_id']=='566ac30a-a3dc-4768-9780-15e4bf642048'){
                    //$delivery_details="Order will be delivered on 30 June.";
                  }
                }
                //die();
                // $shipping = $this -> City -> findById($order['ShippingAddress']['city_id']);
                //$shipping=$this->UserAddress->findById($order['Order']['shipping_address_id']);
                // $address=$order['ShippingAddress']['line1']." ".$order['ShippingAddress']['line2']." ".$order['ShippingAddress']['landmark']." ".
                // $order['ShippingAddress']['city_name']." - ".$order['ShippingAddress']['pin_code']."<br />".$order['ShippingAddress']['state_name']."<br />";
                $message = str_replace("{shipping_name}", $order['ShippingAddress']['name'],$message);
                $message = str_replace("{shipping_line1}", $order['ShippingAddress']['line1'],$message);
                $message = str_replace("{shipping_line2}", $order['ShippingAddress']['line2'],$message);
                $message = str_replace("{shipping_landmark}", $order['ShippingAddress']['landmark'],$message);
                $message = str_replace("{shipping_city}", $order['ShippingAddress']['city_name'],$message);
                $message = str_replace("{shipping_pincode}", $order['ShippingAddress']['pin_code'],$message);


                // $message = str_replace("{shipping_address}", $address, $message);

                $message = str_replace("{order_items}", $order_items_text, $message);
                $message = str_replace("{sub_total}", $sub_total, $message);

                $message= str_replace("{delivery_details}", $delivery_details, $message);
                 $message = str_replace("{grand_total}", $order['Order']['total_amount']+$order['Order']['additional_charges']+$order['Order']['shipping_charges']+$order['Order']['cod_charges']-$order['Order']['discount_value']-$order['Order']['extra_discount'], $message);
                 $message = str_replace("{order_amount}", $order['Order']['total_amount'], $message);

                if($order['Order']['shipping_charges']>0)
                {
                    $message = str_replace("{shipping_charges}", $order['Order']['shipping_charges'], $message);
                }
                else{
                      $message = str_replace("{shipping_charges}", '0', $message);
                }

                if($order['Order']['cod_charges']>0){
                  $temptext='<tr style="border: 1px solid #ccc;width: 800px;height: 40px;color:#000;background: #fff;">
                              <td></td>

                              <td style="background: #fff;text-align: center;color:#000;font-size: 12px;text-transform: uppercase;font-family: Verdana, sans-serif;">cash on delivery charge</td>
                              <td style="text-align:center;background: #fff;font-family: Verdana, sans-serif;"><b>Rs.'.$order['Order']['cod_charges'].'</b></td>
                          </tr>';
                  $message = str_replace("{cod_charges}", $temptext, $message);
                }else
                {
                  $message = str_replace("{cod_charges}", '', $message);

                }
                if(($order['Order']['discount_value']+$order['Order']['extra_discount'])>0){
                  $temptext='<tr style="width: 800px;height: 40px;">
                              <td></td>

                              <td style="width: 800px;height: 40px;color:#EBDFC3;text-transform: uppercase;font-family: Verdana, sans-serif;">Discount</td>
                              <td style="text-align:center;font-family: Verdana, sans-serif;"><b>Rs.'.($order['Order']['discount_value']+$order['Order']['extra_discount']).'</b></td>
                          </tr>';
                  $message = str_replace("{discount_value}", $temptext, $message);
                }else
                {
                  $message = str_replace("{discount_value}", '', $message);
                }
                if($order['Order']['additional_charges']>0){
                  $temptext='<tr style="border:1px solid #ccc;width:800px;height:40px;color:#000;background:#fff">
                              <td></td>

                              <td style="background:#fff;text-align:center;color:#000;font-size:12px;text-transform:uppercase;font-family: Verdana, sans-serif;">additional charges</td>
                              <td style="background:#fff;text-align:center;font-family: Verdana, sans-serif;"><b>Rs.'.$order['Order']['additional_charges'].'</b></td>
                          </tr>';
                  $message = str_replace("{additional_charges}", $temptext, $message);
                }else
                {
                  $message = str_replace("{additional_charges}", '', $message);
                }
                //$message = str_replace("{discount_value}", $order['Order']['discount_value']+$order['Order']['extra_discount'], $message);

               // $message = str_replace("{order_items}",$order_details, $message);
                $message = str_replace("{order_code}",$order['Order']['code'], $message);
                $message = str_replace("{order_id}",$order['Order']['id'], $message);

                $myDateTime = strtotime($order['Order']['modified']);
                $newDateString = date('l, F d Y',$myDateTime);
                $message = str_replace("{order_time}",$newDateString, $message);

                $Email->emailFormat('html');
                $Email->to($order['User']['username']);
                $Email->subject($subject);
                $Email->from(array('noreply@letsshave.com' => 'LetsShave'));
                $Email->send($message);


               $Email_owner = new CakeEmail('default');
               $Email_owner->emailFormat('html');
               //$Email_owner->to('asawaripawar26@gmail.com');
               $Email_owner->to('mohammed.sufyan@actonate.com');
               $Email_owner->subject('LS orders');
               $Email->from(array('noreply@letsshave.com' => 'LetsShave'));
               $Email_owner->send($order['Order']['code']."<br>".$order['Order']['id']."<br>".$order['User']['username']);

            }
        }
        // recreate order functions
    public function getUserOrders()
    {
            if($this->request->is('post'))
            {

                $data = $this->request->input('json_decode',true);
                $this->Order->Behaviors->load('Containable');
                if (isset($data['order_id'])) {
                    $conditions=array(
                        'Order.user_id'=>$data['user_id'],
                        'Order.id'=>$data['order_id'],
                        'Order.status'=>[2,-2]
                    );
                } else {
                    $conditions=array(
                        'Order.user_id'=>$data['user_id'],
                        'Order.status'=>[2,-2]
                    );
                }
                $tmp = $this->Order->find('all',
                    array('conditions'=> $conditions,
                        'contain'=>['OrderItem','User.username','ShippingAddress']
                    )
                );

                if(sizeof($tmp) > 0)
                {
                    $res = new ResponseObject ( ) ;
                    $res -> status = 'success' ;
                    $res -> data = $tmp ;
                    $res -> id = $data['user_id'];
                    $res -> message = 'Address found!' ;
                    $this -> response -> body ( json_encode ( $res ) ) ;
                    return $this -> response ;
                }
                else {
                    $res = new ResponseObject ( ) ;
                    $res -> status = 'error' ;
                    $res -> data = $tmp ;
                    $res -> id = $data['user_id'];
                    $res -> message = 'No address found!' ;
                    $this -> response -> body ( json_encode ( $res ) ) ;
                    return $this -> response ;
                }
            }
    }
    function getOrdersList($code = null)
    {
        //
        if($code != null) {
            $this->Order->Behaviors->load('Containable');
            $tmp = $this->Order->find('all',
                array(
                    'conditions' =>
                    array(
                        // 'User.first_name LIKE'=> '%'.$name.'%'

                        'Order.code LIKE'=> "%$code%",
                        'Order.status'=>[2,-2]

                    ),
                    'fields'=>['Order.code','Order.id','Order.user_id'],
                    'limit'=>20,
                    'contain' =>[]
                )
            );

            //
            $idx = 0; $users = [];
            foreach ($tmp as $t) {
                # code...
                $users[$idx]['order_id'] = $t['Order']['id'];
                $users[$idx]['user_id'] = $t['Order']['user_id'];
                    $users[$idx]['code'] = $t['Order']['code'];
                $idx ++;
            }
            $res = new ResponseObject ( ) ;
            $res -> status = 'success' ;
            $res -> data = $users ;
            $res -> code = $code;
            $res -> message = 'Users found!' ;
            $this -> response -> body ( json_encode ( $res ) ) ;
            return $this -> response ;
        }
    }

    function getOrderItems()
    {
        if($this->request->is('post'))
        {
            $data = $this->request->input('json_decode',true);
            $this->Item->Behaviors->load('Containable');

            //$name = $data['item_name'];
            if (isset($data['order_type'])) {

                $order_type = $data['order_type'];
            } else {
                $order_type = null;

            }
            if (isset($data['order_id'])) {

                $order_id = $data['order_id'];
            } else {
                $order_id = null;

            }
            $order_id = $data['order_id'];
            $item_id_list = $data['item_id_list'];

            $sku_item_ids=$this->SkuItemMapping->find('list',

                [
                    'conditions'=>['SkuItemMapping.item_id'=>$item_id_list],
                    'fields'=>['SkuItemMapping.sku_item_id']
                ]
            );
            $item_ids = $this->SkuItemMapping->find('list',

                [
                    'conditions'=>['SkuItemMapping.sku_item_id'=>$sku_item_ids],
                    'fields'=>['SkuItemMapping.item_id']
                ]
            );
            //pr(array_merge($item_id_list,$item_ids));die();
            $tmp = $this->Item->find('all',
                array(
                    'fields' => array('Item.id','Item.item_id'),
                    'conditions'=> array(
                        'OR'=>['Item.one_line3 not like'=> "%+%",'Item.id'=>$item_id_list],
                        'Item.item_category_id !='=> "ee2db3d0-b471-11e5-abe4-023bbb370ca3",
                        'Item.is_parent' => 0,
                        'Item.id'=>$item_ids
                    ),
                    'contain'=>array()
                )
            );

            $parent_ids=[];
            foreach ($tmp as $item) {
                if ( $item['Item']['item_id'] != null ||  $item['Item']['item_id'] != '') {
                    # code...
                    $parent_ids[] = $item['Item']['item_id'];
                }
            }


            $tmp_2 = $this->Item->find('all',
                array(
                    'fields' => array('Item.id','Item.name','Item.alias','Item.primary_photo_directory','Item.primary_photo','Item.item_type_id','Item.sku_code','Item.stock_type','Item.featured','Item.variant_name','Item.item_id','Item.price','Item.discount_price','Item.one_line3'),
                    'conditions'=> array(
                        'OR'=>[
                            'AND'=>[
                                'OR'=>['Item.one_line3 not like'=> "%+%",'Item.id'=>$item_id_list],
                                'Item.item_category_id !='=> "ee2db3d0-b471-11e5-abe4-023bbb370ca3",
                                'Item.is_parent' => 0,
                                'Item.id'=>$item_ids
                            ],
                            'Item.item_id'=>$parent_ids

                        ]
                    ),
                    'contain'=>array()
                )
            );

            if(sizeof($tmp_2) > 0)
            {
                $res = new ResponseObject ( ) ;
                $res -> status = 'success' ;
                $res -> data = $tmp_2 ;
                $res -> message = 'Items found!' ;
                $this -> response -> body ( json_encode ( $res ) ) ;
                return $this -> response ;
            }
            else {
                # code...
                $res = new ResponseObject ( ) ;
                $res -> status = 'error' ;
                $res -> message = 'Failed to find items!' ;
                $this -> response -> body ( json_encode ( $res ) ) ;
                return $this -> response ;
            }
        }
    }
}













?>
