<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   SMS
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2014-2016 Copyright (c) LetsShave Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
// namespace App\Controller\Component;



// use Cake\Network\Http\Client;
// use Cake\Controller\Component;

// use Cake\Core\Configure;

App::uses('Component', 'Controller');
App::uses('HttpSocket', 'Network/Http');
// require_once "../../Config/config.json";




/**
 * SMS Component
 *
 * @category Component
 * @package  SMS
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.letsshave.com/
 */

class SMSComponent extends Component
{

    /**
     *  Initialization
     *    DATE: 24th April 2017
     *
     * @return array
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function __construct()
    {
        $sms_config = Configure::read('sms');

        //$this->username = env('SMS_USERNAME');
        $this->password = "151656AD2jeC4Rb5910178f"; //$sms_config['MSG91_SMS_PASSWORD'];
        $this->senderID = "CAKWEB"; //$sms_config['MSG91_SMS_SENDERID'];
        $this->route = "4"; //$sms_config['MSG91_SMS_ROUTE'];
        $this->url = "https://control.msg91.com/api/sendhttp.php"; //$sms_config['MSG91_SMS_URL'];

        //SMS LANE
        // $this->baseUrl = $this->url."/vendorsms/pushsms.aspx?";
        // $this->baseUrl .= "user=".$this->username;
        // $this->baseUrl .= "&password=".$this->password;
        // $this->baseUrl .= "&sid=".$this->senderID;
        // $this->baseUrl .= "&msisdn=91";

        //msg91
        $this->baseUrl = $this->url."?";
        $this->baseUrl .= "&authkey=".$this->password;
        $this->baseUrl .= "&sender=".$this->senderID;
        $this->baseUrl .= "&route=".$this->route;
        $this->baseUrl .= "&country=91";
        $this->baseUrl .= "&mobiles=";
    }

    /**
     *   Send SMS
     *
     * @param string $url URL
     *
     * @return json
     */
    private function _send($url = null)
    {
        if ($url != null) {
            $http = new HttpSocket();
            // $response = $http->get($url);
            // pr($url);die();
            $res = $http->get($url);
            // pr($res->body);die();
            return ($res->body);
        }
    }

    /**
     *   Send Order Confirmation SMS
     *
     * @param array $order Order Data
     *
     * @return json
     */
    public function sendOrderConfirmation($order = [])
    {
        $template = $this->baseUrl;
        $template .= $order['mobile'];
        $template .= "&message=We have received your order ";
        $template .= $order['code']." amounting to Rs. ";
        $template .= $order['grand_total'];
        $template .= " and it is being processed. ";
        $template .= "Check your email for more details. ";
        $template .= "Regards, CakeWeb Team";

        $response = $this->_send($template);
        return $response;
    }

    /**
     *   Send Order Confirmation SMS
     *
     * @param array $order Order Data
     *
     * @return json
     */
    public function sendOrderDispatched($order = [])
    {
        $template = $this->baseUrl;
        $template .= $order['mobile'];
        $template .= "&message=Your order ";
        $template .= $order['code']." ";
        $template .= " is successfully dispatched at the desired shipping address.";
        $template .= " Check your email for more details. ";
        $template .= "Regards, CakeWeb Team";
        $response = $this->_send($template);
        return $response;
    }
    
    /**
     *   Send Order Confirmation SMS
     *
     * @param array $order Order Data
     *
     * @return json
     */
    public function sendOrderDelivered($order = [])
    {
        $template = $this->baseUrl;
        $template .= $order['User']['mobile'];
        $template .= "&message=Your order ";
        $template .= $order['Order']['code']." ";
        $template .= " was successfully delivered at the desired shipping address.";
        $template .= "Regards, CakeWeb Team";
        $response = $this->_send($template);
        return $response;
    }

    /**
     *   Send Reset Password SMS
     *
     * @param array $user User Data
     *
     * @return json
     */
    public function sendResetPassword($user = [])
    {
        $template = $this->baseUrl;
        $template .= $user['mobile'];
        $template .= "&message=We heard that you forgot your password.";
        $template .= " Please reset password using this link ".$user['link'];
        $template .= " . ";
        $template .= "Regards, CakeWeb Team";
        $response = $this->_send($template);
        return $response;
    }

}
?>
