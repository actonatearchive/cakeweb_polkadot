<?php
App::uses('CakeTime', 'Utility');
App::uses('CakeEmail', 'Network/Email');

class UserWalletsController extends AppController {

    public $name = 'UserWallets';
    var $uses = array('User', 'UserAddress','EmailTemplate','UserWallet', 'Order');
    public function beforeFilter()
    {
        AppController::beforeFilter();

    }
    public function index($value='')
    {
        $this->layout = "polka_shell";
        $total_count = $this->UserWallet->find('count');
        $this->UserWallet->Behaviors->load('Containable');
        $total_user = $this->UserWallet->find('first',array(
            'fields' => array('count(distinct UserWallet.user_id) as users'),
            'contain' =>array()
        ));
        //pr($total_user);die();
        $this->set('total_user',$total_user[0]['users']);
        $this->set('total_count',$total_count);
        # code...
    }
    public function add()
    {
       
        $this->layout = "polka_shell";
    }
public function submitAddLSMoney()
{
    if($this->activeUser['User']['AdminType']['id'] != 'bd2009c9-2138-11e4-88db-88532e350b97'
        && $this->activeUser['User']['id'] !="5570455e-aaac-44d7-b0fb-4aa16aba72de"
        && $this->activeUser['User']['id'] !="5a13fa99-f334-4f1b-91fc-5d63ac1f02b7"
    ){
        $this->Session->setFlash('Access denied! Please contact Super Admin', 'default', array('class' => 'alert alert-danger') , 'error');

    $this->redirect($this->referer());
} 
if($this->request->is('post'))
{

    $transaction_id = '';
    $data = $this->request->input('json_decode', true);

    if (!empty($data['order_id'])) {

        $order = $this->Order->find('first',
            array(
                'conditions' =>
                array(
                    'code' => $data['order_id'],
                    'Order.user_id' => $data['user_id']
                ),
                'limit'=>20
            )
        );

        if (!empty($order['Order']['transaction_id'])) {

            $transaction_id = $order['Order']['transaction_id'];
        }
    }

    $userWallet = [
        'user_id' => $data['user_id'],
        'points' => $data['amount'],
        'reason' => $data['reason'],
        'added_by' => $this->Auth->user('id'),
        'expires' => date('Y-m-d H:i:s', strtotime($data['expires_at'])),
        'transaction_type' => 0,
        'transaction_id' => $transaction_id,

    ];

    $data = $this->UserWallet->save($userWallet);

    if(!empty($data))
    {
        $res = new ResponseObject ( ) ;
        $res -> status = 'success' ;
        $res -> message = 'LS Money added!' ;
        $res -> user_id = $data['UserWallet']['user_id'];
        $this -> response -> body ( json_encode ( $res ) ) ;
        return $this -> response ;
    }
    else {
                # code...
        $res = new ResponseObject ( ) ;
        $res -> status = 'error' ;
        $res -> message = 'Wasn\'t able to add LS Money!' ;
        $this -> response -> body ( json_encode ( $res ) ) ;
        return $this -> response ;
    }
}
}

function getOrdersList($order_code = null)
{
        //
    if($order_code != null) {
        $tmp = $this->Order->find('all',
            array(
                'conditions' =>
                array(
                    'code' => $order_code
                ),
                'limit'=>20
            )
        );

        if(count($tmp) > 0) {

            $orders = [];
            foreach ($tmp as $row) {

                $order = [
                    'order_id' => $row['Order']['id'],
                    'total_amount' => $row['Order']['total_amount'],
                    'order_code' => $row['Order']['code'],
                    'desc' => 'Order total: ' . $row['Order']['total_amount']
                ];
                $orders[] = $order;
            }
            $res = new ResponseObject ( ) ;
            $res -> status = 'success' ;
            $res -> data = $orders ;
            $res -> message = 'Orders found!' ;
            $this -> response -> body ( json_encode ( $res ) ) ;
            return $this -> response ;
        }
    }
}

public function searchUser($value='')
{
    $this->layout = "polka_shell";
    $this -> set('finalSearchItems',null);
        // Check whether it is a post request or not
    if ($this -> request -> is('post')) {

            // Get the data from post request
        $item = $this->request->data;
        $search = $item['UserWallet']['search_user'];

            // Check whether search term is null, if yes - redirect to index
        if(empty($search) || $search == "" || $search == null) {

                // Display failure message and redirect
            $this->Session->setFlash('Search name blank.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect($this->referre());
        }

        $user = $this->User->find('list',
            array(
                'fields' => array('User.id'),
                'conditions' =>
                array(
                        // 'User.first_name LIKE'=> '%'.$name.'%'
                    'OR' => array(
                        'User.first_name LIKE'=> "%$search%",
                        'User.username LIKE'=> "%$search%",
                        'User.mobile LIKE'=> "%$search%"
                            // 'UserAddress.mobile LIKE'=> "%$search%",
                            // 'UserAddress.name LIKE'=> "%$search%",

                    )
                ),
                'limit'=>20
            )
        );
            //pr($user);die();
                // Find the item
        $this->UserWallet->Behaviors->load('Containable');
        $finalSearchItems = $this->UserWallet->find('all',array(
            'fields' => array('sum(UserWallet.points) as points'),
            'conditions'=>array("UserWallet.user_id"=>$user),
            'group'=>array('UserWallet.user_id'),
            'contain' =>array('User'=>array('fields'=>array('first_name','username','mobile','last_name')))
        ));
            //pr($finalSearchItems);die();
        $this -> set('finalSearchItems', $finalSearchItems);

            // Set the view variables to controller variable values and layout for the view
        $this -> set('page_title', 'Search Items');
        $this -> layout = 'polka_shell';

    }
}

public function viewUserWallet($user_id='')
{

    $this->UserWallet->Behaviors->load('Containable');
    $this->User->Behaviors->load('Containable');


    $wallets = $this->UserWallet->find('all',array(
        'conditions'=>array("UserWallet.user_id"=>$user_id),
        'order'=>array('UserWallet.expires desc'),
        'contain' =>array()));
    $user = $this->User->find('first',
        array(
            'fields' => array('User.first_name','User.username','User.mobile','User.last_name'),
            'conditions' =>
            array(
                'User.id'=> $user_id,

            ),
            'contain' =>array()
        )
    );
    $this -> set('wallets', $wallets);
    $this -> set('user', $user);

    $this->layout = "polka_shell";
}

}
?>
