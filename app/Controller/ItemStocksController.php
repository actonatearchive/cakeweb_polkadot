<?php
/**
 * Created by IntelliJ IDEA.
 * User: Shoaib Merchant
 * Date: 10/4/13
 * Time: 2:37 AM
 * To change this template use File | Settings | File Templates.
 */

class ItemStocksController extends AppController {

	public $name = 'ItemStocks';

	public $uses = array('ItemStock','Item','ItemType','City','ItemStockCity');

	public function index() {

        if ($this -> request -> is('post')) {
            $selectedItem = $this->request->data;
            $this -> redirect(array('controller' => 'item_stocks', 'action' => 'add', $selectedItem['ItemStock']['selected_item']));
        }

		//$itemCats = $this -> ItemStock -> find('all',array());

        // we prepare our query, the cakephp way!
        $this->paginate = array(
            'limit' => 10,
            'order' => array('created' => 'asc'),
            'conditions' => array('total_quantity !='=>-1)
        );
     
        // we are using the 'ItemStock' model
        $itemCats = $this->paginate('ItemStock');        
		$this -> set('item_stocks', $itemCats);

        $itemSelect = $this -> Item -> find('all', array('conditions'=>array('Item.stock_type'=>0),'order' => 'Item.created'));
        $itemSelectList = array();
        foreach($itemSelect as $item){
            $itemSelectList[$item['Item']['id']] = $item['Item']['name']." (".$item['Item']['variant_name'].") ".$item['Item']['alias'];
        }
        $this -> set('item_lists', $itemSelectList);


		$this -> set('page_title', 'Item Stocks');
		$this -> layout = 'polka_shell';

	}

	public function add($id=null) {

		if ($this -> request -> is('post')) {
			$itemStock = $this -> request -> data;
            pr($itemStock);

            if(!empty($itemStock['ItemStock']['discount_price']))
            {
                $discount_percent = round(($itemStock['ItemStock']['price'] - $itemStock['ItemStock']['discount_price'])/$itemStock['ItemStock']['price'],2)*100;
            }
            else{
                $itemStock['ItemStock']['discount_price'] = $itemStock['ItemStock']['price'];
                $discount_percent = 0;
            }
            $itemStock['ItemStock']['discount_percentage'] = $discount_percent;

            if ($this -> ItemStock -> save($itemStock)) {
                $this->Session->setFlash('New stock added.', 'default', array('class' => 'alert alert-success') , 'success');
                //$this -> redirect(array('controller' => 'item_gallery_photos', 'action' => 'manage_gallery',$itemStock['ItemStock']['item_id']));
                $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
			} else {
				//This flash message has to be set in the view properly
                $this->Session->setFlash('Sorry an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
			}
		}
        else {

            if($id == null){
                $this->Session->setFlash('No item selected.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
            }

            $selectedItem = $this->Item->findById($id);
            if($selectedItem == null){
                $this->Session->setFlash('No item selected.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
            } elseif ($selectedItem['Item']['stock_type'] == 1) {
                $this->Session->setFlash('Stock for item is set to infinite.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
            }

            $this->set('selectedItem',$selectedItem);

            $this -> set('page_title', 'Add Item Stock');
            $this -> layout = 'polka_shell';
		}
	}



    public function delete($id=null) {

        if($id == null){
            $this->Session->setFlash('Please choose a stock.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
        }

        $selectedItemStock = $this->ItemStock->findById($id);

        if($selectedItemStock == null){
            $this->Session->setFlash('Please choose a stock.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
        }

        if($this->ItemStock->delete($selectedItemStock['ItemStock']['id'])){
            $this->Session->setFlash('Stock deleted.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
        }

    }

    public function edit($id=null) {

        if ($this -> request -> is('post')) {
            $itemStock = $this -> request -> data;

            if(!empty($itemStock['ItemStock']['discount_price']))
            {
                $discount_percent = round(($itemStock['ItemStock']['price'] - $itemStock['ItemStock']['discount_price'])/$itemStock['ItemStock']['price'],2)*100;
            }
            else{
                $itemStock['ItemStock']['discount_price'] = $itemStock['ItemStock']['price'];
                $discount_percent = 0;
            }
            $itemStock['ItemStock']['discount_percentage'] = $discount_percent;

            if ($this -> ItemStock -> save($itemStock)) {
                $this -> Session -> setFlash("New item stock  added.");
                $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
            } else {
                //This flash message has to be set in the view properly
                $this -> Session -> setFlash("Sorry. an error occurred.");
                $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
            }
        }
        else {

            if($id == null){
                $this->Session->setFlash('No stock selected.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
            }

            $selectedStock = $this->ItemStock->findById($id);
            if($selectedStock == null){
                $this->Session->setFlash('No stock selected.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
            }

            $this->set('selectedStock',$selectedStock);

            $this -> set('page_title', 'Edit Item Stock');
            $this -> layout = 'polka_shell';
        }
    }

    public function increment($id=null,$item_stock_id) {

        if ($this -> request -> is('post')) {
            $itemStock = $this -> request -> data;

            if(!empty($itemStock['ItemStock']['discount_price']))
            {
                $discount_percent = round(($itemStock['ItemStock']['price'] - $itemStock['ItemStock']['discount_price'])/$itemStock['ItemStock']['price'],2)*100;
            }
            else{
                $itemStock['ItemStock']['discount_price'] = $itemStock['ItemStock']['price'];
                $discount_percent = 0;
            }
            $itemStock['ItemStock']['discount_percentage'] = $discount_percent;

            if ($this -> ItemStock -> save($itemStock)) {
                $this->Session->setFlash('New stock added.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
            } else {
                //This flash message has to be set in the view properly
                $this->Session->setFlash('Sorry an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
            }
        }
        else {

            if($id == null){
                $this->Session->setFlash('No item selected.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
            }

            $selectedItem = $this->Item->findById($id);
            if($selectedItem == null){
                $this->Session->setFlash('No item selected.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
            } elseif ($selectedItem['Item']['stock_type'] == 1) {
                $this->Session->setFlash('Stock for item is set to infinite.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
            }

            $selectedItemStock = $this->ItemStock->findById($item_stock_id);
            if($selectedItemStock == null){
                $this->Session->setFlash('No item stock selected.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
            }     

            $this->set('selectedItem',$selectedItem);
            $this->set('selectedItemStock',$selectedItemStock);

            $this -> set('page_title', 'Add Item Stock');
            $this -> layout = 'polka_shell';
        }
    }    

    public function getItemDisplayPrice($item_id=null,$intended_quantity=null) {
        //$item_id = "534254ef-7708-4595-95f6-1b5cbf642048";
        //$intended_quantity = 5;

        $selectedItemStocks = $this->ItemStock->find('all',array('conditions' => array('ItemStock.item_id' => $item_id),'order' => array('ItemStock.created ASC')));

        $finalItemStock = array();
        foreach ($selectedItemStocks as $selectedItemStock) {
            $availableQuantity = 0;
            $availableQuantity = $selectedItemStock['ItemStock']['total_quantity'] - $selectedItemStock['ItemStock']['exhausted_quantity'];

            if($availableQuantity > 0){
                if($availableQuantity >= $intended_quantity) {
                    $finalItemStock['price'] = $selectedItemStock['ItemStock']['price'];
                    $finalItemStock['discount_price'] = $selectedItemStock['ItemStock']['discount_price'];
                    $finalItemStock['available_quantity'] = $availableQuantity;
                    $finalItemStock['stock_id'] = $selectedItemStock['ItemStock']['id'];
                    break;
                 } else {
                    continue;
                 }

            } else {
                continue;
            }
        }

        //pr($finalItemStock);die();
    }

    public function depleteStock($stock_id=null,$buy_quantity=null) {
        //$stock_id = "53428373-623c-410b-9119-1b5cbf642048";
        //$buy_quantity = 2;        
        
        $selectedItemStock = $this->ItemStock->findById($stock_id);
        $finalQuantity = $selectedItemStock['ItemStock']['total_quantity'] - $selectedItemStock['ItemStock']['exhausted_quantity'] - $buy_quantity;

        if($finalQuantity >= 0){
            $selectedItemStock['ItemStock']['exhausted_quantity'] = $selectedItemStock['ItemStock']['exhausted_quantity'] + $buy_quantity;

            if ($this -> ItemStock -> save($selectedItemStock)) {
                $this->Session->setFlash('Stock depleted.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
            } else {
                //This flash message has to be set in the view properly
                $this->Session->setFlash('Sorry, could not deplete stock.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'item_stocks', 'action' => 'index'));
            }            
        }

        //pr($selectedItemStock);die();
    }   



    public function save_price() {
        if ($this -> request -> is('post')) {
            $itemStock = $this->request->data;

            //pr($itemStock);die();

            if($itemStock['ItemStock']['id'] == "" || $itemStock['ItemStock']['price'] == "" ){
                $this->Session->setFlash('You must set a price.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect($this->referer());
            }


            if($itemStock['ItemStock']['price']!=""){
                if($itemStock['ItemStock']['discount_price']==""){
                    $itemStock['ItemStock']['discount_price']=$itemStock['ItemStock']['price'];
                }
                if($itemStock['ItemStock']['discount_price']>$itemStock['ItemStock']['price']){
                    $this->Session->setFlash('Discounted price is greater than price.', 'default', array('class' => 'alert alert-danger') , 'error');
                    $this -> redirect($this->referer());
                }

                //Calculate percentage
                $itemStock['ItemStock']['discount_percentage'] = round(($itemStock['ItemStock']['price'] - $itemStock['ItemStock']['discount_price'])/$itemStock['ItemStock']['price'],2)*100;
            }
            else{
                $this->Session->setFlash('You must set a price.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect($this->referer());
            }

            //pr($itemStock);die();

            if ($this->ItemStock ->save($itemStock)) {

                //This flash message has to be set in the view properly
                $this->Session->setFlash('Item price saved.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect($this->referer());
            } else {
                //This flash message has to be set in the view properly
                $this->Session->setFlash('Sorry an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect($this->referer());
            }
        } else {

            $this -> redirect($this->referer());
            

        }

    }


    public function save_price_ajax($ajax_id=null,$ajax_price=null,$ajax_discount_price=null) {

            $itemStock = $this->ItemStock->findById($ajax_id);
            $return_message = "";            

            if($itemStock['ItemStock']['id'] == "" || $itemStock['ItemStock']['price'] == "" ){
                $return_message = "You must set a price.";
            }

            $itemStock['ItemStock']['price'] = $ajax_price;
            $itemStock['ItemStock']['discount_price'] = $ajax_discount_price;

            if($itemStock['ItemStock']['price']!=""){
                if($itemStock['ItemStock']['discount_price']==""){
                    $itemStock['ItemStock']['discount_price']=$itemStock['ItemStock']['price'];
                }
                if($itemStock['ItemStock']['discount_price']>$itemStock['ItemStock']['price']){
                    $return_message =  "Discounted price is greater than price.";
                }

                //Calculate percentage
                $itemStock['ItemStock']['discount_percentage'] = round(($itemStock['ItemStock']['price'] - $itemStock['ItemStock']['discount_price'])/$itemStock['ItemStock']['price'],2)*100;
            }
            else{
                $return_message =  "You must set a price.";
            }

            if ($this->ItemStock ->save($itemStock)) {

                //This flash message has to be set in the view properly
                $return_message =  "Item price saved.";
            } else {
                //This flash message has to be set in the view properly
                $return_message =  "Sorry an error occurred.";
            }

        $this->set('return_message',$return_message);
        $this -> layout = 'ajax';

    }

}
