<?php
/**
 * Created by IntelliJ IDEA.
 * User: Shoaib Merchant
 * Date: 10/4/13
 * Time: 2:37 AM
 * To change this template use File | Settings | File Templates.
 */

class ArticlesController extends AppController {

    public $name = 'Articles';

    public $uses = array('Article');

    public function index() {
        $articles = $this -> Article-> find('all',array('order'=>array('Article.title')));
        $this -> set('articles', $articles);

        $this -> set('page_title', 'Articles');
        $this -> layout = 'polka_shell';
    }

    public function add() {

        if ($this -> request -> is('post')) {
            $article = $this -> request -> data;

            $article['Article']['content'] = htmlentities($article['Article']['content']);

            $article['Article']['alias'] = str_replace(" ","_",strtolower($article['Article']['title']));

            if ($this -> Article -> save($article)) {
                $this->Session->setFlash('New article added.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'articles', 'action' => 'index'));
            } else {
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'articles', 'action' => 'index'));
            }
        }
        else{

        }
        $this -> set('page_title', 'Add Article');
        $this -> layout = 'polka_shell';
    }

    public function edit($id) {

        if ($this -> request -> is('post')) {
            $article = $this -> request -> data;

            $article['Article']['content'] = htmlentities($article['Article']['content']);
            $article['Article']['alias'] = str_replace(" ","_",strtolower($article['Article']['title']));

            if ($this -> Article -> save($article)) {
                $this->Session->setFlash('Article has been edited.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'articles', 'action' => 'index'));
            } else {
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'articles', 'action' => 'index'));
            }
        }
        else{

            if($id == null)
            {
                $this->Session->setFlash('Article does not exist.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'articles', 'action' => 'index'));
            }

            $selectedArticle = $this->Article->findById($id);

            if($selectedArticle == null)
            {
                $this->Session->setFlash('Article does not exist.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'articles', 'action' => 'index'));
            }

            $this->set('selectedArticle',$selectedArticle);

        }
        $this -> set('page_title', 'Edit Article');
        $this -> layout = 'polka_shell';
    }


    public function delete($id=null) {

        if($id == null){
            $this->Session->setFlash('Please choose an article.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'articles', 'action' => 'index'));
        }

        $selectedArticle = $this->Article->findById($id);

        if($selectedArticle == null){
            $this->Session->setFlash('Please choose an article.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'articles', 'action' => 'index'));
        }

        if($this->Article->delete($selectedArticle['Article']['id'])){
            $this->Session->setFlash('Article deleted.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect(array('controller' => 'articles', 'action' => 'index'));
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'articles', 'action' => 'index'));
        }

        $this -> set('page_title', 'Delete Article');
        $this -> layout = 'polka_shell';
    }


}
