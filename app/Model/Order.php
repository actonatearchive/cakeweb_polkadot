<?php

class Order extends AppModel
{
    var $hasMany = array('OrderItem');   

    var $belongsTo = array('ShippingAddress'=> array('className' => 'UserAddress', 'foreignKey'=>'shipping_address_id'),'BillingAddress'=>
    array('className' => 'UserAddress', 'foreignKey'=>'billing_address_id'),'User','Referral');

}

?>