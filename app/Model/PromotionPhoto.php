<?php
	class PromotionPhoto extends AppModel{
		public $actsAs = array(
        'Upload.Upload' => array(
            'img' => array(
                'mimetypes'=> array('image/jpg','image/jpeg', 'image/png'),
                'thumbnailMethod'=>"php",
                'extensions'=> array('jpg','png','JPG','PNG','jpeg','JPEG'),
                'fields' => array(
                    'dir' => 'img_link'
                )
            )
        )
    );
	}
?>	