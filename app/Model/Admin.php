<?php
App::uses('AuthComponent', 'Controller/Component/Auth');
class Admin extends AppModel
{	 
	public function beforeSave($options = array()) {
	// hash our password
	if (isset($this->data[$this->alias]['password'])) {
	$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
	}

	// fallback to our parent
	return parent::beforeSave($options);
	}

	public $belongsTo=array('AdminType');

}

?>